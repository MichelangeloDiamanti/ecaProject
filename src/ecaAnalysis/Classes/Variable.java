package ecaAnalysis.Classes;

import java.util.Comparator;

import ecaAnalysis.Classes.Enumerations.Kind;
import ecaAnalysis.Classes.Enumerations.Type;


public class Variable extends Label implements Comparable<Variable>
{

	private String value;
	
	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, String type, String kind, int value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() != Type.INT)
			throw new AutomatonException("Cannot convert from INT to " + this.getType().toString().toUpperCase());
		// check if the value is contained in the domain
		else if(value < AutomatonUtils.INT_MIN_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the minimum possible (" + AutomatonUtils.INT_MIN_VAL + ")");
		else if(value > AutomatonUtils.INT_MAX_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the maximum possible (" + AutomatonUtils.INT_MAX_VAL + ")");
		
		this.value = "" + value;
	}

	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, Type type, Kind kind, int value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() != Type.INT)
			throw new AutomatonException("Cannot convert from INT to " + this.getType().toString().toUpperCase());
		else if(value < AutomatonUtils.INT_MIN_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the minimum possible (" + AutomatonUtils.INT_MIN_VAL + ")");
		else if(value > AutomatonUtils.INT_MAX_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the maximum possible (" + AutomatonUtils.INT_MAX_VAL + ")");

		
		this.value = "" + value;
	}
	
	/**
	 * construct a new Variable
	 * 
	 * @param label
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(Label label, int value) throws AutomatonException
	{
		super(label.getName(), label.getType(), label.getKind());
		
		if(this.getType() != Type.INT)
			throw new AutomatonException("Cannot convert from INT to " + this.getType().toString().toUpperCase());
		else if(value < AutomatonUtils.INT_MIN_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the minimum possible (" + AutomatonUtils.INT_MIN_VAL + ")");
		else if(value > AutomatonUtils.INT_MAX_VAL)
			throw new AutomatonException("The value specified (" + value + ") exceeds the maximum possible (" + AutomatonUtils.INT_MAX_VAL + ")");
		this.value = "" + value;
	}
	
	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, String type, String kind, boolean value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() != Type.BOOL)
			throw new AutomatonException("Cannot convert from BOOL to " + this.getType().toString().toUpperCase());
		
		this.value = "" + value;
	}

	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, Type type, Kind kind, boolean value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() != Type.BOOL)
			throw new AutomatonException("Cannot convert from BOOL to " + this.getType().toString().toUpperCase());
		
		this.value = "" + value;
	}
	
	/**
	 * construct a new Variable
	 * 
	 * @param label
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(Label label, boolean value) throws AutomatonException
	{
		super(label.getName(), label.getType(), label.getKind());
		
		if(this.getType() != Type.BOOL)
			throw new AutomatonException("Cannot convert from BOOL to " + this.getType().toString().toUpperCase());
			
		this.value = "" + value;
	}

	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, String type, String kind, String value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() == Type.BOOL)
		{
			if(value.equals("true") || value.equals("false"))
				this.value = value;
			else
				throw new AutomatonException("Cannot assign " + value + " to a BOOL Label");
		}
		
		if(this.getType() == Type.INT)
		{
			if(value.matches("^-?\\d+$"))
			{
				// check if the value is contained in the domain
				int parsedVal = Integer.parseInt(value);
				if(parsedVal < AutomatonUtils.INT_MIN_VAL)
					throw new AutomatonException("The value specified (" + parsedVal + ") exceeds the minimum possible (" + AutomatonUtils.INT_MIN_VAL + ")");
				else if(parsedVal > AutomatonUtils.INT_MAX_VAL)
					throw new AutomatonException("The value specified (" + parsedVal + ") exceeds the maximum possible (" + AutomatonUtils.INT_MAX_VAL + ")");
				this.value = value;
			}
			else
				throw new AutomatonException("Cannot assign " + value + " to a INT Label");
		}
	}

	/**
	 * construct a new Variable
	 * 
	 * @param name
	 * @param type
	 * @param kind
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(String name, Type type, Kind kind, String value) throws AutomatonException
	{
		super(name, type, kind);
		
		if(this.getType() == Type.BOOL)
		{
			if(value.equals("true") || value.equals("false"))
				this.value = value;
			else
				throw new AutomatonException("Cannot assign " + value + " to a BOOL Label");
		}
		
		if(this.getType() == Type.INT)
		{
			if(value.matches("^-?\\d+$"))
			{
				// check if the value is contained in the domain
				int parsedVal = Integer.parseInt(value);
				if(parsedVal < AutomatonUtils.INT_MIN_VAL)
					throw new AutomatonException("The value specified (" + parsedVal + ") exceeds the minimum possible (" + AutomatonUtils.INT_MIN_VAL + ")");
				else if(parsedVal > AutomatonUtils.INT_MAX_VAL)
					throw new AutomatonException("The value specified (" + parsedVal + ") exceeds the maximum possible (" + AutomatonUtils.INT_MAX_VAL + ")");
				this.value = value;
			}
			else
				throw new AutomatonException("Cannot assign " + value + " to a INT Label");
		}
	}
	
	/**
	 * construct a new Variable
	 * 
	 * @param label
	 * @param value
	 * @throws AutomatonException 
	 */
	public Variable(Label label, String value) throws AutomatonException
	{
		super(label.getName(), label.getType(), label.getKind());
					
		if(this.getType() == Type.BOOL)
		{
			if(value.equals("true") || value.equals("false"))
				this.value = value;
			else
				throw new AutomatonException("Cannot assign " + value + " to a BOOL Label");
		}
		
		if(this.getType() == Type.INT)
		{
			if(value.matches("^-?\\d+$"))
				this.value = value;
			else
				throw new AutomatonException("Cannot assign " + value + " to a INT Label");
		}
				
	}
	
	
	@Override
	public Variable clone()
	{
		Variable newV = null;
		try 
		{
			newV = new Variable(this.getLabel().clone(), this.getValue());
		}
		catch (AutomatonException e){ e.printStackTrace(); }
		
		return newV;
	}
	
	public Label getLabel() throws AutomatonException
	{
		Label l = null;
		
		l = new Label(this.getName(), this.getType(), this.getKind());
		
		return l;
	}
	
	
	public String getValue()
	{
		return this.value;
	}
	
	public void setValue(boolean value)
	{
		this.value = "" + value;
	}
	
	public void setValue(int value)
	{
		this.value = "" + value;
	}
	
	public void setValue(String value)
	{
		this.value = value;
	}
	
	public boolean isSensor()
	{
		boolean r = (this.getKind() == Kind.IN) ? true : false;
		return r;
	}

	public boolean isActuator()
	{
		boolean r = (this.getKind() == Kind.OUT) ? true : false;
		return r;
	}
	
	

	@Override
	public String toString() 
	{		
		return this.getName() + " " + this.getType() + " " + this.getKind() + " = " + this.value;
	}
	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variable other = (Variable) obj;
		if (value == null)
		{
			if (other.value != null)
				return false;
		} 
		else if (!value.equals(other.value))
			return false;
		return true;
	}

	@Override
	public int compareTo(Variable v)
	{
		return Comparators.NAME.compare(this, v);
	}
	
	
	
	public static class Comparators
	{
		public static final Comparator<Variable> NAME = new Comparator<Variable>()
		{
			@Override
			public int compare(Variable v1, Variable v2)
			{
				return v1.getName().compareTo(v2.getName());
			}
		};
	}
	
}
