package ecaAnalysis.Classes;

import java.util.HashSet;

import ecaAnalysis.Classes.Enumerations.Type;

public class Invariant 
{
	private Expression condition = null;
	
	public Invariant(Expression condition)
	{
		super();
		this.condition = condition;
	}
	
	@Override
	public Invariant clone()
	{
		Invariant newI = new Invariant(this.getCondition().clone());
		return newI;
	}
	
	public Expression getCondition()
	{
		return condition;
	}

	public void setCondition(Expression condition) 
	{
		this.condition = condition;
	}
	
	@Override
	public String toString() 
	{
		return "[ " + this.condition.toString() + " ]";
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((condition == null) ? 0 : condition.hashCode());
		return result;
	}
	
	public static Invariant merge(HashSet<Invariant> invs) throws AutomatonException
	{
		Invariant merged = new Invariant( new Expression(true, Type.BOOL));
		if(!invs.isEmpty())
		{
			HashSet<Expression> exprs = new HashSet<Expression>();
			for(Invariant i : invs)
				exprs.add(i.getCondition());
		
			merged = new Invariant(Expression.merge(exprs));
		}
		
		return merged;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Invariant other = (Invariant) obj;
		if (condition == null) 
		{
			if (other.condition != null)
				return false;
		}
		else if (!condition.equals(other.condition))
			return false;
		return true;
	}


	

	



}
