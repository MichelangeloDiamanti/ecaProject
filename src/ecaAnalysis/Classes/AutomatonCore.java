package ecaAnalysis.Classes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.microsoft.z3.Context;
import com.microsoft.z3.Model;

import GUI.Classes.GUIutils;
import ecaAnalysis.Classes.Enumerations.Operator;
import ecaAnalysis.Classes.Enumerations.Stability;
import ecaAnalysis.Classes.Enumerations.Type;
import ecaAnalysis.Classes.Enumerations.VisitColor;

public class AutomatonCore 
{

	private static ECAfile ecafile = null; 
	private static StatesTable statestable = null;
	private static Vertex root = null;

	private static ConcurrentHashMap<BigInteger, Vertex> vertices = new ConcurrentHashMap<BigInteger, Vertex>();
	private static ConcurrentHashMap<String, Edge> edges = new ConcurrentHashMap<String, Edge>();
	private static ConcurrentHashMap<HashSet<Variable>, Cluster> clusters = new ConcurrentHashMap<HashSet<Variable>, Cluster>();

	private static boolean stop = false;
	
	/**
	 * Constructs and initializes an Automaton according to the parameters
	 * specified in the Ecafile.
	 *  
	 * @param ecafile
	 * @throws AutomatonException 
	 */
	public AutomatonCore(ECAfile ecafile) throws AutomatonException 
	{
		super();
		
		AutomatonCore.clear();
		AutomatonCore.ecafile = ecafile.clone();
		AutomatonCore.ecafile.setInvariantsInCNF();
		AutomatonCore.ecafile.removeUnused();
		AutomatonCore.statestable = new StatesTable(AutomatonCore.ecafile);
		// restore the initial value of the ecafile
		AutomatonCore.ecafile = ecafile.clone();
	}
	
	
	public HashSet<Variable> getInitialConfig() throws StopSimulationException, AutomatonException
	{
		manageStopExecution();
		
		HashSet<Variable> r = new HashSet<Variable>();
		
		// init z3
		HashMap<String, String> cfg = new HashMap<String, String>();
        cfg.put("model", "true");
        Context ctx = new Context(cfg);
        Model sat = null;	// the model used to check the input w.r.t the invariants
        
		// we want to check if the configuration given by the user is consistent with the invariants
		// we merge the invariants
		Expression SATmerged = Invariant.merge(ecafile.getInvariants()).getCondition();
					
		GUIutils.setPercentageProportion(AutomatonCore.statestable.size());
        
		State s = null;	
			
		// iterate over the temporary states table
		for(BigInteger i = new BigInteger("0"); i.compareTo(AutomatonCore.statestable.size())==-1; i=i.add(new BigInteger("1")))
		{
			
			manageStopExecution();
			
			s = AutomatonCore.statestable.getStateFromID(i);
			
			/*	we want to check if the current configuration of sensors is 
			 *	consistent w.r.t the invariants
			 */	
			Expression merged = SATmerged.clone();
			
			// we assert that each variable must be equal to the value of the current configuration
			for (Variable v : s.getVariables()) 
			{
				Expression varExpr = new Expression(v.getLabel(), v.getType());
				Expression valExpr = null;
				if (v.getType() == Type.BOOL)
				{
					Boolean bval = Boolean.parseBoolean(v.getValue());
					valExpr = new Expression(bval, Type.BOOL);
				}
				if (v.getType() == Type.INT)
				{
					Integer ival = Integer.parseInt(v.getValue());
					valExpr = new Expression(ival, Type.INT);
				}
				Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
				merged = Expression.merge(merged, cmpExpr);
			}
	        // check if the given inputs are sat w.r.t the invariants
			sat = merged.checkSatisfiability(ctx);
			// if it is sat
			if(sat != null)
			{
				r = s.getVariables();
				break;
			}
		}	
		// free the memory (garbage collection)
		System.gc();
	
		if(r == null)
			throw new AutomatonException("\nThere is no valid state");

		manageStopExecution();
		return r;
	}
	
	/**
	 * Asks the user for an initial configuration of the actuators, repeat the
	 * input until it is SAT w.r.t the invariants.
	 * Find the lowest possible configuration of the sensors that satisfies 
	 * the invariants. Create a state with these variables and return the
	 * corresponding Vertex
	 * 
	 * @return the Vertex created with the lowest possible SAT configuration of vars
	 * @throws StopSimulationException 
	 * @throws AutomatonException 
	 */
	private HashSet<Variable> getInitialSensorConfig(HashSet<Variable> actuators) throws StopSimulationException, AutomatonException
	{
		manageStopExecution();
		
		HashSet<Variable> r = new HashSet<Variable>();
		
		// init z3
		HashMap<String, String> cfg = new HashMap<String, String>();
        cfg.put("model", "true");
        Context ctx = new Context(cfg);
        Model sat = null;	// the model used to check the input w.r.t the invariants
        
		// we want to check if the configuration given by the user is consistent with the invariants
		// we merge the invariants
		Expression SATmerged = Invariant.merge(ecafile.getInvariants()).getCondition();
		
		// for each variable we assert that it must be equal to the value given by the user
		for (Variable v : actuators) 
		{
			Expression varExpr = new Expression(v.getLabel(), v.getType());
			Expression valExpr = null;
			if (v.getType() == Type.BOOL)
			{
				Boolean bval = Boolean.parseBoolean(v.getValue());
				valExpr = new Expression(bval, Type.BOOL);
			}
			if (v.getType() == Type.INT)
			{
				Integer ival = Integer.parseInt(v.getValue());
				valExpr = new Expression(ival, Type.INT);
			}
			Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
			SATmerged = Expression.merge(SATmerged, cmpExpr);
		}
			
	    // check if the given inputs are sat w.r.t the invariants
		sat = SATmerged.checkSatisfiability(ctx);
			
		if(sat == null)
		{
			State tmpState = new State(actuators);
			throw new AutomatonException("\nThe values entered: \n" + tmpState + "\nare not SAT w.r.t the invariants\n");
		}
			
		// save all the variable instances that are useful (sensors)
		ArrayList<ArrayList<Variable>> vars = new ArrayList<ArrayList<Variable>>();
		for(ArrayList<Variable> v : statestable.getVars())
			if(v.get(0).isSensor())
				vars.add(v);
             
		// contains all the sensors combinations
        StatesTable tmpStatesTable = new StatesTable(vars);

        GUIutils.setPercentageProportion(tmpStatesTable.size());
        
		State s = null;	
			
		// iterate over the temporary states table
		for(BigInteger i = new BigInteger("0"); i.compareTo(tmpStatesTable.size())==-1; i=i.add(new BigInteger("1")))
		{
			
			manageStopExecution();
			
			s = tmpStatesTable.getStateFromID(i);
			
			
			/*	we want to check if the current configuration of sensors is 
			 *	consistent w.r.t the invariants
			 */	
			Expression merged = SATmerged.clone();
			
			// we assert that each variable must be equal to the value of the current configuration
			for (Variable v : s.getVariables()) 
			{
				Expression varExpr = new Expression(v.getLabel(), v.getType());
				Expression valExpr = null;
				if (v.getType() == Type.BOOL)
				{
					Boolean bval = Boolean.parseBoolean(v.getValue());
					valExpr = new Expression(bval, Type.BOOL);
				}
				if (v.getType() == Type.INT)
				{
					Integer ival = Integer.parseInt(v.getValue());
					valExpr = new Expression(ival, Type.INT);
				}
				Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
				merged = Expression.merge(merged, cmpExpr);
			}
	        // check if the given inputs are sat w.r.t the invariants
			sat = merged.checkSatisfiability(ctx);
			// if it is sat
			if(sat != null)
			{
				// contains both sensors and actuators
				r.addAll(actuators);	// add all the actuators
				r.addAll(s.getVariables());	// add all the sensors
				break;
			}
		}	
		// free the memory (garbage collection)
		System.gc();
		
		manageStopExecution();
		return r;
	}
	
	
	private Vertex initialState(HashSet<Variable> devices)throws StopSimulationException, AutomatonException
	{
		manageStopExecution();
		
		Vertex r = null;
		
		// init z3
		HashMap<String, String> cfg = new HashMap<String, String>();
        cfg.put("model", "true");
        Context ctx = new Context(cfg);
        Model sat = null;	// the model used to check the input w.r.t the invariants
        
		// we want to check if the configuration given by the user is consistent with the invariants
		// we merge the invariants
		Expression merged = Invariant.merge(ecafile.getInvariants()).getCondition();
		
		// we assert that each variable must be equal to the value of the current configuration
		for (Variable v : devices) 
		{
			Expression varExpr = new Expression(v.getLabel(), v.getType());
			Expression valExpr = null;
			if (v.getType() == Type.BOOL)
			{
				Boolean bval = Boolean.parseBoolean(v.getValue());
				valExpr = new Expression(bval, Type.BOOL);
			}
			if (v.getType() == Type.INT)
			{
				Integer ival = Integer.parseInt(v.getValue());
				valExpr = new Expression(ival, Type.INT);
			}
			Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
			merged = Expression.merge(merged, cmpExpr);
		}
        // check if the given inputs are sat w.r.t the invariants
		sat = merged.checkSatisfiability(ctx);
		
		// if it is sat
		if(sat != null)
		{
			State s = new State(devices);
			r = new Vertex(s, AutomatonCore.statestable);
		}
		else
			throw new AutomatonException("\nThe values entered are not SAT w.r.t the invariants\n");
		
		return r;
		
	}
	
	/**
	 * Find the next natural state updating only the sensors.
	 * The new state will have a configuration of sensors that activate at least one
	 * rule and is the closest configuration from the current one, the actuators
	 * wont be changed
	 * 
	 * @param the parent Vertex 
	 * @return the Vertex created with the lowest possible SAT configuration of sensors
	 * @throws StopSimulationException 
	 * @throws AutomatonException 
	 */
	private Edge nextNaturalState(Vertex parent) throws StopSimulationException, AutomatonException
	{
		manageStopExecution();
		
		Edge arch = null;
		Model sat = null;	// the model used to check the input w.r.t the invariants
		
		// init z3
		HashMap<String, String> cfg = new HashMap<String, String>();
	    cfg.put("model", "true");
	    Context ctx = new Context(cfg);
		
		HashSet<Label> lbls = new HashSet<Label>();
		HashSet<Variable> chdVars = new HashSet<Variable>(); //sensors that must be updated
	    HashSet<Variable> unchdVars = new HashSet<Variable>(); //variables that must not be updated
	    ArrayList<ArrayList<Variable>> vars = new ArrayList<ArrayList<Variable>>();
	         
	    // get all the useful labels 
	    //(only the sensors which appear in the rules)
	    for(ECArule r : ecafile.getRules())
	    {
	    	for(Label l : r.getGuards())
	      		if(l.isSensor())
	    			lbls.add(l);

	    	for(Label l : r.getCondition().getLabels())
	      		if(l.isSensor())
	    			lbls.add(l);
	    }   
	          
	    // save all the variable instances that are useful
	    for(ArrayList<Variable> v : statestable.getVars())
	      	if(lbls.contains(v.get(0).getLabel()))
	    		vars.add(v);
	    	
	    // get the useful variables of the old state
	    for(Variable v : parent.getState(AutomatonCore.statestable).getVariables())
	    {
	      	if(lbls.contains(v.getLabel()))
	    		chdVars.add(v);
	      	else
	      		unchdVars.add(v);
	    }
	    
		if(isNextNaturalStateExisting(chdVars, unchdVars, vars))
		{
		    // create a StatesTable only for the useful sensors
			StatesTable chdStatesTable = new StatesTable(vars);
			// a temporary state
			State s = null; 
			
			// create a state from the changing variables (for getting the id)
			State chdState = new State(chdVars);
						
			for
			(
				// start with the next configuration of the changing sensors (id) 
				BigInteger i = chdStatesTable.getIDFromState(chdState).add(new BigInteger("1")); 
				i.compareTo(chdStatesTable.size())==-1; 
				i=i.add(new BigInteger("1"))
			)
			{		
				
				manageStopExecution();
				
				s = chdStatesTable.getStateFromID(i);
		        
		    	// we want to check if the configuration of sensors is consistent with the invariants
				// we merge the invariants (we don't check if it is empty because there are always the dummy inv)
				Expression merged = Invariant.merge(ecafile.getInvariants()).getCondition();
				
				// for each variable we assert that it must be equal to the value given by the user
				for (Variable v : s.getVariables()) 
				{
					Expression varExpr = new Expression(v.getLabel(), v.getType());
					Expression valExpr = null;
					if (v.getType() == Type.BOOL)
					{
						Boolean bval = Boolean.parseBoolean(v.getValue());
						valExpr = new Expression(bval, Type.BOOL);
					}
					if (v.getType() == Type.INT)
					{
						Integer ival = Integer.parseInt(v.getValue());
						valExpr = new Expression(ival, Type.INT);
					}
					Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
					merged = Expression.merge(merged, cmpExpr);
				}
				// merge the value of the unchanged variables
				for (Variable v : unchdVars) 
				{
					Expression varExpr = new Expression(v.getLabel(), v.getType());
					Expression valExpr = null;
					if (v.getType() == Type.BOOL)
					{
						Boolean bval = Boolean.parseBoolean(v.getValue());
						valExpr = new Expression(bval, Type.BOOL);
					}
					if (v.getType() == Type.INT)
					{
						Integer ival = Integer.parseInt(v.getValue());
						valExpr = new Expression(ival, Type.INT);
					}
					Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
					merged = Expression.merge(merged, cmpExpr);
				}
				
		        // check if the given inputs are sat w.r.t the invariants
				sat = merged.checkSatisfiability(ctx);
				
				
				// if the new configuration of sensors is consistent, we proceed to merge one rule at time
				if(sat != null)
				{		
					manageStopExecution();
					
					// get the previous config of sensors
					State prevS = chdStatesTable.getStateFromID(i.subtract(new BigInteger("1")));
					// get all the updating labels
					HashSet<Label> chdLbls = s.getChangedLabels(prevS);
					
					sat = null;
					for(ECArule r : ecafile.getRules())
					{
		
						boolean guardFlag = false;
		
				        // check if the guard is triggered
				       	for(Label l : r.getGuards())
				       	{
				       		if(chdLbls.contains(l))
				       		{
				       			guardFlag = true;
				       			break;
				       		}
				       	}
						
				       	if(guardFlag)
				       	{						       		
							// merge the current rule
							Expression condition = Expression.merge(merged,r.getCondition());
			           
					    	// evaluate the expression using the JavaScript engine
							sat = condition.checkSatisfiability(ctx);
			    		
							// if the rule can be activated with the current configuration
			    			if(sat != null )
			    			{
			    				// create a new State and Vertex with the current config of variables
			    				HashSet<Variable> allVars = new HashSet<Variable>();
			    				allVars.addAll(s.getVariables());
			    				allVars.addAll(unchdVars);
			    				s.setVariables(allVars);
			    				Vertex child = new Vertex(AutomatonCore.statestable.getIDFromState(s));
			    				
			    				// create the edge between parent and child
			    				arch = new Edge(parent, child);
		    				
			    				break;
			    			}    			
				       	}
		            }
		            if(arch != null)
						break;
				}
			}
		}

		// free the memory (garbage collection)
		System.gc();
		
		manageStopExecution();
		
		return arch;
	}	


	private HashSet<Edge> nextArtificialState(Edge prevCur) throws StopSimulationException, AutomatonException
	{		
		manageStopExecution();
		
		Vertex current = prevCur.getTo();
		HashSet<Edge> children = new HashSet<Edge>();
				
		// contains all the rules that will be activated
		HashSet<ECArule> rules = new HashSet<ECArule>();
				
		// iterate over all the rules to find the ones that are triggered
		for(ECArule r : ecafile.getRules())
		{
			if(prevCur.triggers(r))
				rules.add(r);			
		}
		
		// if at least one rule is activated, the state is unstable
		if(!rules.isEmpty())
			current.setStability(Stability.UNSTABLE);
		
		// execute the actions on the rules activated
		for(ECArule currRule : rules)
		{
			manageStopExecution();
			
			String actions = "";
			String result = "";
	
			State s = current.getState(AutomatonCore.statestable).clone();
			
			// adds the declaration of the variable to the input string
	    	//eg: "A>0" ---> "A=1;A>10"
	        for(Variable var : current.getState(AutomatonCore.statestable).getVariables())
	        	actions += var.getName() + "=" + var.getValue() + ";";
	        
			for(Action a : currRule.getActions())
			{
				String action = actions + a.toString() + ";";
				
				// evaluate the expression using the JavaScript engine
				synchronized (AutomatonUtils.engine) { result = AutomatonUtils.evaluateExpression(action); }
				
				// remove the decimal part in te input: 2.0 --> 2
				if(result.indexOf(".") != -1)
					result = result.substring(0,result.indexOf("."));
				
				s.setVariable(new Variable(a.getLabel(),result));
				
			}
	
			Vertex nextState = null;
			
			boolean c = s.checkConsistency(AutomatonCore.ecafile.getInvariants());
			
			// create a new vertex from the obtained state
			
			if(c)			
				nextState = new Vertex(s, AutomatonCore.statestable);
			else
			{
				BigInteger id = AutomatonUtils.getIncorrectStateID();
				nextState = new Vertex(id);
				nextState.setStability(Stability.INCORRECT);	
			}
				
	
			// create a new edge
			Edge nextRule = new Edge(current, currRule, nextState);
				
			children.add(nextRule);
						
		}
		
		// free the memory (garbage collection)
		System.gc();
		
		manageStopExecution();
		
		return children;
	}
	
	
	/**
	 * Uses z3 to check if the next natural state is containing in the remaining
	 * part of the state table
	 * 
	 * @param chdVars
	 * @param unchdVars
	 * @param vars
	 * @return true if there is a next natural state, false otherwise
	 * @throws AutomatonException 
	 */
	private boolean isNextNaturalStateExisting(HashSet<Variable> chdVars, HashSet<Variable> unchdVars, ArrayList<ArrayList<Variable>> vars ) throws AutomatonException
	{
		boolean innseFlag = false;
		
		// normally the vars contained in the state don't follow any particular order
		// we must organize them to be in the same order as in the statestable
		ArrayList<Variable> orderedChdVars = new ArrayList<Variable>();
		for(ArrayList<Variable> alv : vars)
	    {
	    	for(Variable v : chdVars)
		    {
	    		if(v.getLabel().equals(alv.get(0).getLabel()))
	    		{
	    			orderedChdVars.add(v);
	    			break;
	    		}
		    }
	    }		
		
		/*
		 * the condition we must check is: 
		 * invariants & ( r1_condition | r2_condition | ... | rn_condition ) & remaining_part_of_statestable
		 */
		
		// we merge the invariants
		Expression CNNSE = Invariant.merge(ecafile.getInvariants()).getCondition();;

		// merge all the rules conditions using OR operators
		Iterator<ECArule> rIt = ecafile.getRules().iterator();
		Expression CNNSEcond = rIt.next().getCondition();
		while(rIt.hasNext())
			CNNSEcond = new Expression(CNNSEcond, Operator.OR, rIt.next().getCondition(), Type.BOOL);
					
		// merge them to the previous condition using AND
		CNNSE = Expression.merge(CNNSE, CNNSEcond);
		
		// we put in the expression the current values of the variables that did not change
		HashSet<Expression> unchdVarsExpr = new HashSet<Expression>();
		for (Variable v : unchdVars) 
		{
			Expression varExpr = new Expression(v.getLabel(), v.getType());
			Expression valExpr = null;
			if (v.getType() == Type.BOOL)
			{
				Boolean bval = Boolean.parseBoolean(v.getValue());
				valExpr = new Expression(bval, Type.BOOL);
			}
			if (v.getType() == Type.INT)
			{
				Integer ival = Integer.parseInt(v.getValue());
				valExpr = new Expression(ival, Type.INT);
			}
			Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
			unchdVarsExpr.add(cmpExpr);
		}
		Expression CNNSEact = Expression.merge(unchdVarsExpr);
		CNNSE = Expression.merge(CNNSE, CNNSEact);
		
		/*
		 * compute the expression which is going to represent the remaining part of the states table:
		 * [ (s1 == current & s2 == current & ... & sn > current) |
		 *   (s1 == current & s2 == current & ... & sn-1 > current) |
		 *   ... |
		 *   (s1 > current) ]
		 * where s1 ... sn are the sensors ordered in the same manner as in the states table 
		 * (s1 is the left-most sensor and sn is the right-most)
		 */		
		
		int chdVarSize = orderedChdVars.size();
		
		Expression CNNSEor = null;

		while(chdVarSize > 0)
		{
			chdVarSize--;
					
			int chdVarIndex = 0;

			HashSet<Expression> chdVarsExpr = new HashSet<Expression>();
			
			
			
			/* 
			 * we compute one piece of the expression: (s1 == current & s2 == current & ... & sn > current)
			 * 
			 * iterate over the state's variables in order, and instruct that the n-th variable's
			 * value must be different from it's previous one while the others must be the same
			 */
			for (Variable v : orderedChdVars) 
			{
				Expression varExpr = new Expression(v.getLabel(), v.getType());
				Expression valExpr = null;
				if (v.getType() == Type.BOOL)
				{
					Boolean bval = Boolean.parseBoolean(v.getValue());
					valExpr = new Expression(bval, Type.BOOL);
				}
				if (v.getType() == Type.INT)
				{
					Integer ival = Integer.parseInt(v.getValue());
					valExpr = new Expression(ival, Type.INT);
				}
				
				Expression cmpExpr = null;
				
				if(chdVarIndex < chdVarSize)
					cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
				else
				{
					// different means 'greater' for integer values
					if(v.getType() == Type.INT)
						cmpExpr = new Expression(varExpr, Operator.GT, valExpr, Type.BOOL);
					if(v.getType() == Type.BOOL)
						cmpExpr = new Expression(varExpr, Operator.NEQ, valExpr, Type.BOOL);
					
					chdVarsExpr.add(cmpExpr);
					break;
				}
				chdVarsExpr.add(cmpExpr);
				chdVarIndex++;
			}
			// merge all the pieces with ANDS (s1 == current & s2 == current & ... & sn > current)
			Expression CNNSEsens = Expression.merge(chdVarsExpr);
			// add the expression to the previous one with OR
			CNNSEor = (CNNSEor == null) ? CNNSEsens : new Expression(CNNSEor, Operator.OR, CNNSEsens, Type.BOOL);
		}
				
		CNNSE = Expression.merge(CNNSE, CNNSEor);
		
		// if the expression is satisfiable returns true, false otherwise
		if(CNNSE.checkSatisfiability() != null)
			innseFlag = true;
		
		return innseFlag;		
	}
	
	public static void stopExecution()
	{
		AutomatonCore.stop = true;
	}
	
	private void manageStopExecution() throws StopSimulationException
	{
//		if(Runtime.getRuntime().freeMemory() < 5242880) //5MB//TODO
//		{
//			String info = "The execution was interupted to because the memory was very poca"; //risolvere
//		//	JOptionPane.showMessageDialog(GUI.frame, info);
//			throw new StopSimulationException("The execution has been stopped");
//		}
		
		if(AutomatonCore.stop)
			throw new StopSimulationException("The execution has been stopped");
	}
	
	private void manageStopParallelExecution(ArrayList<Thread> threads, ArrayList<Thread> waitingThreads) throws StopSimulationException
	{
		if(AutomatonCore.stop)
		{
			threads.addAll(waitingThreads);
			waitingThreads.clear();
			
			for(Iterator<Thread> ti = threads.iterator(); ti.hasNext();)
	    	{
				Thread t = ti.next();

	   			// we remove it from the threads list
 	    		try 
   	    		{
   	    			ti.remove();
					t.join();
				} 
   	    		catch (InterruptedException e){}
    		}
			throw new StopSimulationException("The execution has been stopped");
    	}	
		
	}
	
	private int checkDuplicate(Edge newEdge, ConcurrentHashMap<BigInteger, Vertex> localVertices, ConcurrentHashMap<String, Edge> localEdges)
	{
		// 2 = nothing is duplicated
		// 1 = only the vertex is duplicated
		// 0 = all duplicated
	
		int duplicateCode = 2;
		
		// if the edge was already explored
		if(localEdges.get(newEdge.identifier()) != null)
			duplicateCode = 0;
		else
			// check if the vertex was already created
			if(localVertices.get(newEdge.to.getState()) != null)
				duplicateCode = 1;
		
		// if the edge was already explored but it leads to an incorrect state
		if(duplicateCode==2 && !newEdge.getTo().isConsistent())
		{
			BigInteger i = new BigInteger("-1");
			while(localVertices.get(i) != null)
			{
				Vertex tmpV = localVertices.get(i);
				Edge tmpE = tmpV.getIncomingEdges().iterator().next(); //we know that there is only ONE incoming edge
				
				if(newEdge.getFrom().equals(tmpE.getFrom()))
				{
					if(newEdge.getRule().equals(tmpE.getRule()))
					{
						duplicateCode = 0;
						break;
					}
				}
				i = i.subtract(new BigInteger("1"));
			}
		}
		
		return duplicateCode;
	}
		
	private HashSet<Edge> naturalIteration(HashSet<Vertex> parentVertices) throws StopSimulationException, AutomatonException
	{
		
		HashSet<Edge> nextNatEdges = new HashSet<Edge>();
		
		for(Vertex v : parentVertices)
		{
			Edge tmpEdge = nextNaturalState(v);
			
			if( tmpEdge != null)
			{
				HashSet<Variable> clusterID = Cluster.getClusterID(tmpEdge, AutomatonCore.statestable);
						
				Cluster curCluster = null;
				if(AutomatonCore.clusters.get(clusterID) == null)
					curCluster = new Cluster(clusterID);
				else
					curCluster = AutomatonCore.clusters.get(clusterID);
						   					
				curCluster.addEdge(tmpEdge, AutomatonCore.statestable);
				curCluster.addVertex(tmpEdge.getTo(), AutomatonCore.statestable);
				AutomatonCore.clusters.put(clusterID, curCluster);	
				GUIutils.updatePercentage(AutomatonCore.clusters.size());
				
				tmpEdge.getTo().addEdge(tmpEdge);
				tmpEdge.getFrom().addEdge(tmpEdge);    				
				AutomatonCore.vertices.put(tmpEdge.getTo().getState(), tmpEdge.getTo());
				AutomatonCore.edges.put(tmpEdge.identifier(), tmpEdge);
				
				nextNatEdges.add(tmpEdge);
			}
				
		}
		
		return nextNatEdges;
	}
	
	private HashSet<Vertex> artificialRecursion(Edge parentEdge) throws StopSimulationException, AutomatonException
	{		
		HashSet<Vertex> leaves = new HashSet<Vertex>();
		HashSet<Edge> nextArtEdges = nextArtificialState(parentEdge);
			
		Iterator<Edge> naei = nextArtEdges.iterator();	
		while(naei.hasNext())
		{
			Edge e = naei.next();
			HashSet<Variable> clusterID = Cluster.getClusterID(e, AutomatonCore.statestable);
			Cluster curCluster = null;
			if(AutomatonCore.clusters.get(clusterID) == null)
				curCluster = new Cluster(clusterID);
			else
				curCluster = AutomatonCore.clusters.get(clusterID);	
			
			int dc = checkDuplicate(e, AutomatonCore.vertices, AutomatonCore.edges);
			if(dc == 2)
			{
				e.to.addEdge(e);
				e.from.addEdge(e);
				AutomatonCore.edges.put(e.identifier(), e);	
				AutomatonCore.vertices.put(e.to.getState(), e.to);
				if(e.getTo().isConsistent())
				{
					curCluster.addEdge(e, AutomatonCore.statestable);
					curCluster.addVertex(e.getTo(), AutomatonCore.statestable);
				}
				else
					curCluster.addIncorrectEdgeAndVertex(e, statestable);
				AutomatonCore.clusters.put(clusterID, curCluster);
				GUIutils.updatePercentage(AutomatonCore.clusters.size());
			}
			else if(dc == 1)
			{
				Vertex oldState = AutomatonCore.vertices.get(e.to.getState());
		
				// recreate a the edge with the correct nextState
				e = new Edge(e.from, e.rule, oldState);
				oldState.addEdge(e);
				e.from.addEdge(e);
				AutomatonCore.edges.put(e.identifier(), e);
				curCluster.addEdge(e, AutomatonCore.statestable);
				AutomatonCore.clusters.put(clusterID, curCluster);
				GUIutils.updatePercentage(AutomatonCore.clusters.size());
			}
			else if(dc == 0)
				naei.remove();
		}
		if(!nextArtEdges.isEmpty())
		{
			for(Edge nae : nextArtEdges)
			{
				if(nae.getTo().isConsistent())
					leaves.addAll(artificialRecursion(nae));
			}
		}
		else
			leaves.add(parentEdge.getTo());

		return leaves;
	}
	
	
	public void startSequentialSimulation(HashSet<Variable> initialConfig) throws StopSimulationException, AutomatonException
	{	
		ECAfile tmpECAfile = AutomatonCore.ecafile.clone();
		AutomatonCore.ecafile.setInvariantsInCNF();
		AutomatonCore.ecafile.removeUnused();
		
		boolean sensorPresence = false;
		for(Variable v: initialConfig)
		{
			if(v.isSensor())
			{
				sensorPresence = true;
				break;
			}
		}
		if(sensorPresence == false)
			initialConfig.addAll(this.getInitialSensorConfig(initialConfig));
		
		AutomatonCore.root = initialState(initialConfig);
		AutomatonCore.vertices.put(AutomatonCore.root.getState(), AutomatonCore.root);
		
		if(!tmpECAfile.getSensors().isEmpty())
		{	
			
			// we assume that clusters is empty
			HashSet<Variable> clusterID = Cluster.getClusterID(AutomatonCore.root, AutomatonCore.statestable);
			Cluster rootCluster = new Cluster(clusterID);
			rootCluster.addVertex(AutomatonCore.root, AutomatonCore.statestable);
			AutomatonCore.clusters.put(clusterID , rootCluster);
			GUIutils.updatePercentage(AutomatonCore.clusters.size());
			
			HashSet<Edge> e = new HashSet<Edge>();
			HashSet<Vertex> v = new HashSet<Vertex>();
			v.add(AutomatonCore.getRoot());
					  
			while(true)
			{
				e = naturalIteration(v);
				v.clear();
				
				if(!e.isEmpty())
				{
					for(Edge ed : e)
					{
						if(ed != null)
							v.addAll(artificialRecursion(ed));
					}
				}
				else
					break;
			}
		}
		
		ecafile = tmpECAfile.clone();
	}
	
	public void startParallelSimulation(HashSet<Variable> initialConfig) throws StopSimulationException, AutomatonException
	{
		
		ECAfile tmpECAfile = AutomatonCore.ecafile.clone();
		AutomatonCore.ecafile.setInvariantsInCNF();
		AutomatonCore.ecafile.removeUnused();
		
		boolean sensorPresence = false;
		for(Variable v: initialConfig)
		{
			if(v.isSensor())
			{
				sensorPresence = true;
				break;
			}
		}
		if(sensorPresence == false)
			initialConfig.addAll(this.getInitialSensorConfig(initialConfig));
		
		AutomatonCore.root = initialState(initialConfig);
		AutomatonCore.vertices.put(AutomatonCore.root.getState(), AutomatonCore.root);
		
		// we assume that clusters is empty
		HashSet<Variable> clusterID = Cluster.getClusterID(AutomatonCore.root, AutomatonCore.statestable);
		Cluster rootCluster = new Cluster(clusterID);
		rootCluster.addVertex(AutomatonCore.root, AutomatonCore.statestable);
		AutomatonCore.clusters.put(clusterID , rootCluster);
		GUIutils.updatePercentage(AutomatonCore.clusters.size());
		
    	int cores = Runtime.getRuntime().availableProcessors();	// number of virtual cores*3 
    	
    	ArrayList<Thread> threads = new ArrayList<Thread>();	// list containing all active threads
    	// list containing threads that are waiting to be executed
    	ArrayList<Thread> waitingThreads = new ArrayList<Thread>();	
    	
    	// AutomatonUtils.parallelEdges = edges that can be processed in parallel by threads
    	AutomatonUtils.parallelVertices.add(root);

    	Logger log = AutomatonUtils.getLogger();
    	
    	while(true)
    	{
    			
    		manageStopParallelExecution(threads, waitingThreads);
    		
    		// if there are any threads in a waiting state and we have some cores*3
    		// available we start it and remove it from the waiting threads
			Iterator<Thread> wti = waitingThreads.iterator();
    		while (threads.size() <= cores*3 && wti.hasNext()) 
    		{
    			Thread wt = wti.next();
    			threads.add(wt);
	    		wt.start();
	    		wti.remove();
	        	log.log(Level.INFO, "ResumedThread: " + wt.getName());	
			}
    		
    		int parallels = 0;
    		synchronized (AutomatonUtils.parallelVertices){ parallels = AutomatonUtils.parallelVertices.size(); }
    		    			
       		// we must assign each available thread to work on a parallel island
	    	while(parallels > 0)
	    	{
	    		Vertex pv = AutomatonUtils.parallelVertices.peek();
	    		AutomatonUtils.deleteParallelVertex(pv);
	    			
	    		Thread t = new Thread(new AutomaThread(pv)); // we create the new thread
		    			
		    	// if we did not reach the MAX number of threads yet we start it
		    	if(threads.size() <= cores*3)
		    	{    				
		    		threads.add(t);
		    		t.start();
		    	   	log.log(Level.INFO, "StartThread: " + t.getName());
		    	}
		    	// if we cant start it we enqueue it
		    	else
		    	{
		    		waitingThreads.add(t);
		    	   	log.log(Level.INFO, "WaitingThread: " + t.getName());
		    	}
	    	   				
		    	AutomatonUtils.parallelVertices.poll();
	            parallels--;
	    	}
    		

    		// we check the status of each thread
    		for(Iterator<Thread> ti = threads.iterator(); ti.hasNext();)
	    	{
    			Thread t = ti.next();

    			// if the current thread is dead
	    		if(!t.isAlive())
	    		{
	    			// we remove it from the threads list
	   	    		try 
	   	    		{
	   	    			ti.remove();
						t.join();
						log.log(Level.INFO, "TerminatedThread: " + t.getName());
					} 
	   	    		catch (InterruptedException e){ e.printStackTrace(); }
	    		}
	    	}
    		
    		// if there are no threads left it means that the work is done
	    	if(threads.size() <= 0)
	    		break;
	    	
	    	// free the memory
	    	System.gc();
    	}
    	
		ecafile = tmpECAfile.clone();
		
	}

	
	public void cloneResult(Automaton a)
	{
		for(BigInteger v : AutomatonCore.vertices.keySet())
		{
			Vertex newV = new Vertex(v);
			newV.visitState = AutomatonCore.vertices.get(v).visitState;
			newV.stability = AutomatonCore.vertices.get(v).stability;
			a.addVertex(newV);
			if(v.equals(AutomatonCore.root.state))
				a.setRoot(newV);
		}

		for(String e : AutomatonCore.edges.keySet())
		{
			BigInteger from = AutomatonCore.edges.get(e).getFrom().getState();
			BigInteger to = AutomatonCore.edges.get(e).getTo().getState();
			ECArule rule = AutomatonCore.edges.get(e).getRule();
			
			Edge newE = (rule == null) ? new Edge(a.getVertices().get(from), a.getVertices().get(to)) : new Edge(a.getVertices().get(from), rule.clone(), a.getVertices().get(to));
						
			newE.getFrom().addEdge(newE);
			newE.getTo().addEdge(newE);
							
			a.addEdge(newE);
		}
		

		for(HashSet<Variable> c : AutomatonCore.clusters.keySet())
			a.addCluster(AutomatonCore.clusters.get(c).clone());
	}

	public static void clear()
	{
		AutomatonCore.vertices.clear();
		AutomatonCore.edges.clear();
		AutomatonCore.clusters.clear();
		AutomatonCore.root = null;
		AutomatonCore.ecafile = null;
		AutomatonCore.statestable = null;
		AutomatonCore.stop = false;
		
		AutomatonUtils.clear();
		Cluster.clear();
	}
	
	public static Vertex getVertex(BigInteger id)
	{
		return AutomatonCore.vertices.get(id);
	}

	public static ECAfile getEcafile() 
	{
		return ecafile;
	}

	public static StatesTable getStatestable() 
	{
		return statestable;
	}

	public static Vertex getRoot() 
	{
		return root;
	}

	public static ConcurrentHashMap<BigInteger, Vertex> getVertices() 
	{
		return vertices;
	}

	public static ConcurrentHashMap<String, Edge> getEdges() 
	{
		return edges;
	}

	public static ConcurrentHashMap<HashSet<Variable>, Cluster> getClusters() 
	{
		return clusters;
	}	
	
	/*   __   __              _               
	 \ \ / /  ___   _ _  | |_   ___  __ __
	  \ V /  / -_) | '_| |  _| / -_) \ \ /    ##########################################################################################################
	   \_/   \___| |_|    \__| \___| /_\_\    ##########################################################################################################
*/
	public class Vertex
	{
	   private BigInteger state;
	   private HashSet<Edge> incomingEdges = new HashSet<Edge>();
	   private HashSet<Edge> outgoingEdges = new HashSet<Edge>();
	   private VisitColor visitState = VisitColor.WHITE;
	   private Stability stability = Stability.STABLE;
		
		public Vertex(BigInteger state) 
	   {
			super();
			this.state = state;
		}
	   
	   public Vertex(State state, StatesTable statestable) throws AutomatonException 
	   {
			super();
			this.state = statestable.getIDFromState(state);
		}
	
		public BigInteger getState() 
		{
			return state;
		}
	
		public State getState(StatesTable statestable) throws AutomatonException 
		{
			return statestable.getStateFromID(state);
		}
		
		public void setState(BigInteger state) 
		{
			this.state = state;
		}
		
		public void setState(State state, StatesTable statestable) throws AutomatonException 
		{
			this.state = statestable.getIDFromState(state);
		}
		
		public HashSet<Edge> getIncomingEdges() 
		{
			return incomingEdges;
		}
	
		public HashSet<Edge> getOutgoingEdges() 
		{
			return outgoingEdges;
		}
		
	   public VisitColor getVisitState() 
	   {
			return visitState;
		}
	
		public void setVisitState(VisitColor visitState) 
		{
			this.visitState = visitState;
		}
		
		
		public Stability getStability() 
		{
			return stability;
		}
	
		public void setStability(Stability stability) 
		{
			this.stability = stability;
		}
	
		public boolean addEdge(Edge edge) 
		{
			if(edge.getFrom() == this)
				outgoingEdges.add(edge);
			
			else if(edge.getTo() == this)
				incomingEdges.add(edge);
		
			else return false;
			return true;
		}   
								
		public boolean isConsistent()
		{
			return (this.stability != Stability.INCORRECT) ? true : false;			
		}
		
		/**
		 * computes the difference between the labels of 2 vertex
		 * 
		 * @param previous
		 * @return HashSet<Label> changed labels
		 * @throws AutomatonException 
		 */
		public HashSet<Label> getChangedLabels(Vertex previous) throws AutomatonException
		{
			State curState = statestable.getStateFromID(this.getState());
			State prevState = statestable.getStateFromID(previous.getState());
			return curState.getChangedLabels(prevState);
		}
		
		/**
		 * computes the difference between the variables of 2 vertex
		 * 
		 * @param previous
		 * @return HashSet<Variable> changed variables
		 * @throws AutomatonException 
		 */
		public HashSet<Variable> getChangedVariables(Vertex previous) throws AutomatonException
		{
			State curState = statestable.getStateFromID(this.getState());
			State prevState = statestable.getStateFromID(previous.getState());	
			return curState.getChangedVariables(prevState);
		}
		
		/**
		 * @return the statestable of the parent automaton
		 */
		public StatesTable getStatesTable() 
		{
			return statestable;
		}
		
		/**
		 * performs a DFS and returns the first encountered stable vertices.
		 * Note that it works only if there are not incorrect states nor cycles
		 * 
		 * @param HashSet of leaves already encountered in other branches
		 * @return HashSet of leaves encountered
		 */
		public HashSet<Vertex> getNearestLeaves(HashSet<Vertex> leaves)
		{
			HashSet<Vertex> newLeaves = new HashSet<Vertex>();
			newLeaves.addAll(leaves);
			
			if(this.stability == Stability.STABLE)
				newLeaves.add(this);
			else
			{
				for(Edge e : this.outgoingEdges)
					newLeaves.addAll(e.getTo().getNearestLeaves(newLeaves));
			}
			
			return newLeaves;
		}
		
		
		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((state == null) ? 0 : state.hashCode());
			return result;
		}
	
		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj) // same object
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass()) // object of different classes
				return false;
			Vertex other = (Vertex) obj; // cast to vertex
			if (state == null)
			{
				if (other.state != null)
					return false;
			} 
			// compare the vertex by state
			else
			{
					
				if(state.compareTo(new BigInteger("0")) != -1 && other.state.compareTo(new BigInteger("0")) != -1)
				{
					try 
					{
						// get the statestable ( one can compare 2 vertex of 2 different automaton )
						StatesTable st1 = this.getStatesTable();
						StatesTable st2 = other.getStatesTable();
						// get the concrete value of the states and compare them 
						State s1 = st1.getStateFromID(state);
						State s2 = st2.getStateFromID(other.state);
						if (!s1.equals(s2))
							return false;
					}
					catch (AutomatonException e){ e.printStackTrace(); }
				}
				else if(state.compareTo(other.state) == 0)
					return true;
				else
					return false;
						
			}
				
			return true;
		}
	
		@Override
		public String toString() 
		{
			return state.toString();
		} 
	   
		public String toString(StatesTable statestable) throws AutomatonException 
		{
			return statestable.getStateFromID(state).toString();
		} 
		  		  	
	}

/*	 ___      _              
	| __|  __| |  __ _   ___ 
	| _|  / _` | / _` | / -_)   ##########################################################################################################
	|___| \__,_| \__, | \___|   ##########################################################################################################
         		 |___/       
*/
	public class Edge
	{
		private Vertex from;
		private Vertex to;
		private ECArule rule;
	
		
		public Edge(Vertex from, ECArule rule, Vertex to) 
		{
			super();
			this.from = from;
			this.rule = rule;
			this.to = to;
		}
		
		public Edge(Vertex from, Vertex to) 
		{
			this(from, null, to);
		}
	
		public boolean triggers(ECArule rule) throws AutomatonException 
		{
			boolean triggered = false;
			
			if(rule != null)
			{
				// init z3
				HashMap<String, String> cfg = new HashMap<String, String>();
				cfg.put("model", "true");
				Context ctx = new Context(cfg);
				
				// get all the updating labels
				HashSet<Label> chdLbls = to.getChangedLabels(from);
				
				HashSet<Label> rls = rule.getGuards();
				
				// check if at least one updating label appears in the guard of the current rule
				boolean guardFlag = false; 
				for(Label l : chdLbls)
				{
					if(rls.contains(l))
					{
						guardFlag = true;
						break;
					}
				}
				if(guardFlag)
				{
					// we want to check if the configuration of sensors is consistent with the invariants
					// we merge the invariants
					Expression merged = Invariant.merge(ecafile.getInvariants()).getCondition();
					
					for (Variable v : to.getState(statestable).getVariables()) 
					{
						Expression varExpr = new Expression(v.getLabel(), v.getType());
						Expression valExpr = null;
						if (v.getType() == Type.BOOL)
						{
							Boolean bval = Boolean.parseBoolean(v.getValue());
							valExpr = new Expression(bval, Type.BOOL);
						}
						if (v.getType() == Type.INT)
						{
							Integer ival = Integer.parseInt(v.getValue());
							valExpr = new Expression(ival, Type.INT);
						}
						Expression cmpExpr = new Expression(varExpr, Operator.EQ, valExpr, Type.BOOL);
						merged = Expression.merge(merged, cmpExpr);
					}
					merged = Expression.merge(merged, rule.getCondition());
		
					// check if the given inputs are sat w.r.t the invariants
					if(merged.checkSatisfiability(ctx) != null)
						triggered = true;
				}
			}
			return triggered;
		}
				
		public Vertex getFrom() 
		{
			return from;
		}
	
		public void setFrom(Vertex from) 
		{
			this.from = from;
		}
	
		public Vertex getTo() 
		{
			return to;
		}
	
		public void setTo(Vertex to) 
		{
			this.to = to;
		}
	
		public ECArule getRule() 
		{
			return rule;
		}
	
		public void setRule(ECArule rule) 
		{
			this.rule = rule;
		}
	
		public String identifier() 
		{
			String id = rule != null ? rule.getID() : "nat";
			return this.getFrom() + id + this.getTo(); 
		}
		
		@Override
		public String toString() 
		{
			String id = rule != null ? rule.getID() : " ";
			return this.getFrom() + "   " + id + "   " + this.getTo(); 
		}
	
		public String toString(StatesTable statestable) throws AutomatonException 
		{
			String r = rule != null ? rule.toString() : "Natural";
			return this.getFrom().toString(statestable) + "   " + r + "   " + this.getTo().toString(statestable); 
		}
	
		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((from == null) ? 0 : from.hashCode());
			result = prime * result + ((rule == null) ? 0 : rule.hashCode());
			result = prime * result + ((to == null) ? 0 : to.hashCode());
			return result;
		}
	
		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Edge other = (Edge) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (from == null) {
				if (other.from != null)
					return false;
			} else if (!from.equals(other.from))
				return false;
			if (rule == null) {
				if (other.rule != null)
					return false;
			} else if (!rule.equals(other.rule))
				return false;
			if (to == null) {
				if (other.to != null)
					return false;
			} else if (!to.equals(other.to))
				return false;
			return true;
		}
	
		private AutomatonCore getOuterType() 
		{
			return AutomatonCore.this;
		}
		
		
		
	}

/*    _____   _                            _ 
	 |_   _| | |_    _ _   ___   __ _   __| |
	   | |   | ' \  | '_| / -_) / _` | / _` |   ##########################################################################################################
	   |_|   |_||_| |_|   \___| \__,_| \__,_|   ##########################################################################################################
*/
	public class AutomaThread implements Runnable
	{	
		private Vertex clusterRoot;
		private HashSet<Variable> clusterID = new HashSet<Variable>();
		private ConcurrentHashMap<BigInteger, Vertex> localVertices = new ConcurrentHashMap<BigInteger, Vertex>();
		private ConcurrentHashMap<String, Edge> localEdges = new ConcurrentHashMap<String, Edge>();
		
		/**
		 * spawn a thread that's meant to work on a parallel edge
		 * @param edge to work on
		 */
		public AutomaThread(Vertex cr)
		{
			super();
			this.clusterRoot = cr;
		}
		
		public void run()
		{
			try 
			{
				Edge nns = nextNaturalState(clusterRoot);
	     			
				if(nns != null)
				{
		   			clusterID = Cluster.getClusterID(nns, AutomatonCore.statestable);
		   			
		   			synchronized(clusterID)
					{
		   				Cluster curCluster = null;
		   				if(AutomatonCore.clusters.get(clusterID) == null)
		   					curCluster = new Cluster(clusterID);
		   				else
		   					curCluster = AutomatonCore.clusters.get(clusterID);
		   					
		   				curCluster.addEdge(nns, AutomatonCore.statestable);
		   				curCluster.addVertex(nns.getTo(), AutomatonCore.statestable);
		   				AutomatonCore.clusters.put(clusterID, curCluster);
		   				GUIutils.updatePercentage(AutomatonCore.clusters.size());
					}
	
					localEdges.put(nns.identifier(), nns);
					localVertices.put(nns.to.state, nns.to);
					   			
					if(nns.to.isConsistent())
						parallelArtificialRecursion(nns);
	
					synchronized(AutomatonCore.edges){ AutomatonCore.edges.putAll(localEdges); }
					synchronized(AutomatonCore.vertices){ AutomatonCore.vertices.putAll(localVertices); }
				}
	
				// free the memory
				System.gc();
			} 
			catch (StopSimulationException e)
			{
				return;
			} 
			catch (AutomatonException e) 
			{
				return;
			}

	   }
	   
	   private void parallelArtificialRecursion(Edge parentEdge) throws StopSimulationException, AutomatonException
		{
			HashSet<Edge> nextArtEdges = nextArtificialState(parentEdge);
				
			if(!nextArtEdges.isEmpty())
			{
				synchronized(clusterID)
				{
	
					Cluster curCluster = AutomatonCore.clusters.get(clusterID);
					
					Iterator<Edge> naei = nextArtEdges.iterator();	
					while(naei.hasNext())
					{
						Edge e = naei.next();
						
						int dc = checkDuplicate(e, curCluster.getClusterVertices(), curCluster.getClusterEdges());
						if(dc == 2)
						{
							e.to.addEdge(e);
							e.from.addEdge(e);
							localEdges.put(e.identifier(), e);	
							localVertices.put(e.to.getState(), e.to);
							if(e.to.isConsistent())
							{
								curCluster.addEdge(e, statestable);
								curCluster.addVertex(e.to, statestable);	
							}
							else
								curCluster.addIncorrectEdgeAndVertex(e, statestable);
													
						}
						else if(dc == 1)
						{
						
							Vertex oldState = curCluster.getClusterVertices().get(e.to.getState());
					
							// recreate a the edge with the correct nextState
							e = new Edge(e.from, e.rule, oldState);
							oldState.addEdge(e);
							e.from.addEdge(e);
							localEdges.put(e.identifier(), e);
							curCluster.addEdge(e, statestable);
						}
						else if(dc == 0)
							naei.remove();
					}
					
					AutomatonCore.clusters.put(clusterID, curCluster);
					GUIutils.updatePercentage(AutomatonCore.clusters.size());
				}
				
				for(Edge nae : nextArtEdges)
				{
					if(nae.getTo().isConsistent())
						parallelArtificialRecursion(nae);
				}
									
			}
			else
			{
				// put the start of another island in the parallelVertices
				if(parentEdge.to.isConsistent())
					AutomatonUtils.offerParallelVertex(parentEdge.to);
				
			}
	
		}
	   
	   
	}

	public class StopSimulationException extends Exception
	{
		private static final long serialVersionUID = 1L;

		public StopSimulationException(String message)
		{
			super(message);
		}
		
	}
	

	
}
