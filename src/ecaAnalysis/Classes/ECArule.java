package ecaAnalysis.Classes;

import java.util.HashSet;

public class ECArule 
{

	private String id = null;
	private HashSet<Label> guards = new HashSet<Label>();
	private Expression condition = null;
	private HashSet<Action> actions = new HashSet<Action>();
	
	/** 
	 *  constructor for many guards and many actions
	 *  
	 *  @param String id
	 *	@param ArrayList<Label> guards 
	 *  @param Expression condition
	 *  @param ArrayList<Action> actions
	 */
	public ECArule(String id, HashSet<Label> guards, Expression condition, HashSet<Action> actions)
	{
		super();
		
		this.id = id;
		HashSet<Label> gs = new HashSet<Label>();
		for(Label l : guards)
			gs.add(l);
		this.guards = gs;
		
		this.condition = condition;
		
		HashSet<Action> as = new HashSet<Action>();
		for(Action a : actions)
			as.add(a);
		this.actions = as;
	}
	
	/** 
	 *  constructor for many guards and a single action
	 *  	 
	 *  @param String id
	 *	@param ArrayList<Label> guards 
	 *  @param Expression condition
	 *  @param Action action
	 */
	public ECArule(String id, HashSet<Label> guards, Expression condition, Action action)
	{
		super();
		
		this.id = id;
		HashSet<Label> gs = new HashSet<Label>();
		for(Label l : guards)
			gs.add(l);
		this.guards = gs;
		this.condition = condition;
		actions.add(action);
	}
	
	/** 
	 *  constructor for a many actions and single guard
	 *  
	 *  @param String id
	 *	@param Label label 
	 *  @param Expression condition
	 *  @param ArrayList<Action> actions
	 */
	public ECArule(String id, Label label, Expression condition, HashSet<Action> actions)
	{
		super();
		
		this.id = id;
		guards.add(label);
		this.condition = condition;
		HashSet<Action> as = new HashSet<Action>();
		for(Action a : actions)
			as.add(a);
		this.actions = as;
	}
	
	/** 
	 *  constructor for a single label and a single action
	 *  
	 *  @param String id
	 *	@param Label label 
	 *  @param Expression condition
	 *  @param Action action
	 */
	public ECArule(String id, Label label, Expression condition, Action action)
	{
		super();
		
		this.id = id;
		guards.add(label);
		this.condition = condition;
		actions.add(action);
	}

	@Override
	public ECArule clone()
	{
		HashSet<Label> tmpL = new HashSet<Label>();
		for(Label l : this.getGuards())
			tmpL.add(l.clone());
		
		HashSet<Action> tmpA = new HashSet<Action>();
		for(Action a : this.getActions())
			tmpA.add(a.clone());
		
		ECArule newE = new ECArule(this.id, tmpL, this.getCondition().clone(), tmpA);
		return newE;
	}
	
	public String getID() 
	{
		return id;
	}
	
	public HashSet<Label> getGuards() 
	{
		return guards;
	}

	public void setGuards(HashSet<Label> guards) 
	{
		this.guards = guards;
	}

	public Expression getCondition() 
	{
		return condition;
	}

	public void setCondition(Expression condition) 
	{
		this.condition = condition;
	}

	public HashSet<Action> getActions() 
	{
		return actions;
	}

	public void setActions(HashSet<Action> actions) 
	{
		this.actions = actions;
	}

	public HashSet<Label> getLabels()
	{
		HashSet<Label> lbls = new HashSet<Label>();
		lbls.addAll(this.getGuards());
		lbls.addAll(this.getCondition().getLabels());
		for(Action a : this.getActions())
		{
			lbls.add(a.getLabel());
			lbls.addAll(a.getExpression().getLabels());
		}
		return lbls;
	}
	
	/**
	 * get the action which has the specified label as the assignment label
	 * 
	 * @param Label l
	 * @return Action
	 */
	public Action getAction(Label l)
	{
		Action returnAct = null;
		for(Action a : this.actions)
		{
			if(a.getLabel().equals(l))
			{
				returnAct = a;
				break;
			}
		}
		return returnAct;
	}
	
	@Override
	public String toString() 
	{
		String eca = this.id + ": ";
		int count = 0;
		
		// list all the guards adding an OR between them
        for (Label l : guards)
        {
        	if(count>0)
        		eca = eca + ", ";
        	
        	eca = eca + l.getName();
        	count++;
        }
        
        // add the condition
		eca = eca + " [ " + this.condition.toString() + " ] ";
		
		count = 0;
		
		// list all the actions adding a comma between them
        for (Action a : actions)
        {
        	if(count>0)
        		eca = eca + ", ";
        	
        	eca = eca + a.toString();
        	count++;
        }
        
		return eca;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ECArule other = (ECArule) obj;
		if (id == null) 
		{
			if (other.id != null)
				return false;
		} 
		else if (!id.equals(other.id))
			return false;
		return true;
	}




	
	
}
