package ecaAnalysis.Classes;

public class Action 
{

	private Label label = null;
	private Expression expression = null;
	
	public Action(Label label, Expression expression) 
	{
		super();
		this.label = label;
		this.expression = expression;
	}

	@Override
	public Action clone()
	{
		Action newA = new Action(this.getLabel().clone(),this.getExpression().clone());
		return newA;
	}
	
	public Label getLabel() 
	{
		return label;
	}

	public void setLabel(Label label) 
	{
		this.label = label;
	}

	public Expression getExpression() 
	{
		return expression;
	}

	public void setExpression(Expression expression) 
	{
		this.expression = expression;
	}

	@Override
	public String toString() 
	{
		return label.getName() + " := " + expression;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((expression == null) ? 0 : expression.hashCode());
		result = prime * result + ((label == null) ? 0 : label.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (expression == null) 
		{
			if (other.expression != null)
				return false;
		}
		else if (!expression.equals(other.expression))
			return false;
		if (label == null) 
		{
			if (other.label != null)
				return false;
		}
		else if (!label.equals(other.label))
			return false;
		return true;
	}



	
}
