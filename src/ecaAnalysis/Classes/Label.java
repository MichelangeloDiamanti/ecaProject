package ecaAnalysis.Classes;

import ecaAnalysis.Classes.Enumerations.Kind;
import ecaAnalysis.Classes.Enumerations.Type;

public class Label
{

	private String name = "";
	private Type type = null;
	private Kind kind = null;
	
	public Label(String name, String type, String kind) throws AutomatonException
	{
		super();
		
		if (name == null) throw new AutomatonException("Parameter NAME cannot be NULL");
		
		if ( type != null )
			if( !type.equals("int") && !type.equals("bool") )
				throw new AutomatonException("Label type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		if ( kind != null )
			if( !kind.equals("in") && !kind.equals("out") )
				throw new AutomatonException("Label kind " + kind.toUpperCase() + " unkwown: \n	it must be IN or OUT");
		
		this.name = name;
		
		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;

		if(kind.equals("in")) this.kind = Kind.IN;
		else if(kind.equals("out")) this.kind = Kind.OUT;
	}
	
	public Label(String name, Type type, Kind kind) throws AutomatonException
	{
		super();
		if (name == null) throw new AutomatonException("Parameter NAME cannot be NULL");
		
		this.name = name;
		this.type = type;
		this.kind = kind;
	}
	
	public Label(String name) throws AutomatonException
	{
		super();
		if (name == null) throw new AutomatonException("Parameter NAME cannot be NULL");
		
		this.name = name;
		this.type = null;
		this.kind = null;
	}
	
	public boolean isSensor()
	{
		boolean r = (kind == Kind.IN) ? true : false;
		return r;
	}

	public boolean isActuator()
	{
		boolean r = (kind == Kind.OUT) ? true : false;
		return r;
	}

	@Override
	public Label clone()
	{
		Label newL = null;
		try 
		{
			newL = new Label(this.getName(),this.getType(),this.getKind());
		}
		catch (AutomatonException e){ e.printStackTrace(); }
		
		return newL;
	}
	
	public String getName() 
	{
		return name;
	}
	
	public void setName(String name) 
	{
		this.name = name;
	}
	
	public Type getType() 
	{
		return this.type;
	}
	
	public void setType(String type) throws AutomatonException 
	{		
		if ( type != null )
			if( !type.equals("int") && !type.equals("bool") )
				throw new AutomatonException("Label type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;
	}
	
	public void setType(Type type) throws AutomatonException 
	{		
		if ( type == null )
			throw new AutomatonException("Label type unkwown: \n	it must be INT or BOOL");
		
		this.type = type;
	}
	
	public Kind getKind() 
	{
		return kind;
	}
	
	public void setKind(String kind) throws AutomatonException 
	{		
		if ( kind != null )
			if( !kind.equals("in") && !kind.equals("out") )
				throw new AutomatonException("Label kind " + kind.toUpperCase() + " unkwown: \n	it must be IN or OUT");

		if(kind.equals("in")) this.kind = Kind.IN;
		else if(kind.equals("out")) this.kind = Kind.OUT;
	}

	public void setKind(Kind kind) throws AutomatonException 
	{		
		if ( kind == null )
			throw new AutomatonException("Label kind unkwown: \n	it must be IN or OUT");
		
		this.kind = kind;
	}

	
	@Override
	public String toString() 
	{
		return name + " " + type.toString().toLowerCase() + " " + kind.toString().toLowerCase();
	}

	
	@Override
	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Label other = (Label) obj;
		if (name == null)
		{
			if (other.name != null)
				return false;
		} 
		else if (!name.equals(other.name))
			return false;
		return true;
	}

	
}
