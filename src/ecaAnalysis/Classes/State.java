package ecaAnalysis.Classes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;

import ecaAnalysis.Classes.Enumerations.Type;

public class State 
{
	private HashSet<Variable> vars = new HashSet<Variable>();
	
	public State(HashSet<Variable> vars) 
	{
		super();
		
		HashSet<Variable> vs = new HashSet<Variable>();
		for(Variable v : vars)
			vs.add(v);
		
		this.vars = vs;
	}

	@Override
	public State clone()
	{ 
		HashSet<Variable> tmpV = new HashSet<Variable>();
		for(Variable v : this.getVariables())
			tmpV.add(v.clone());
		State newS = new State(tmpV);
		return newS;
	}
	
	public HashSet<Variable> getVariables() 
	{
		return vars;
	}

	public void setVariables(HashSet<Variable> vars) 
	{
		this.vars = vars;
	}
	
	public void setVariable(Variable var) throws AutomatonException 
	{
		if(this.getLabels().contains(var.getLabel()))
		{
			for(Variable v : this.vars)
				// check if the name, type and kind are equal
				if(v.getLabel().equals(var.getLabel()))
				{
					if(v.getKind().equals(var.getKind()) &&	v.getType().equals(var.getType()))
					{
						v.setValue(var.getValue());
						break;
					}
					else throw new AutomatonException("Cannot make the assignment because the variable < " + var.getLabel() + " > is not compatible with < " + v.getLabel() + " >");
				}
		}
		else throw new AutomatonException("The variable < " + var.getLabel() + " > is not contained in the State");
			
	}
	
	public HashSet<Label> getLabels() throws AutomatonException 
	{
		HashSet<Label> l = new HashSet<Label>();
		for(Variable v : vars)
			l.add( v.getLabel() );

		return l;
	}
	
	public HashSet<Label> getSensors() throws AutomatonException 
	{
		HashSet<Label> l = new HashSet<Label>();
		for(Variable v : vars)
			if(v.isSensor())
				l.add( v.getLabel() );

		return l;
	}

	public HashSet<Label> getActuators() throws AutomatonException 
	{
		HashSet<Label> l = new HashSet<Label>();
		for(Variable v : vars)
			if(v.isActuator())
				l.add( v.getLabel() );

		return l;
	}

	public HashSet<String> getNames() throws AutomatonException 
	{
		HashSet<String> n = new HashSet<String>();
		for(Variable v : vars)
			n.add( v.getLabel().getName() );
		return n;
	}

	
	public boolean checkConsistency(HashSet<Invariant> invs) throws AutomatonException
	{
		boolean c = true;
		
		Expression merged = Invariant.merge(invs).getCondition();
		
		// iterate over all the variables to replace them in the invs
		for(Variable v : this.vars)
		{
			Expression val = null;
			if(v.getType() == Type.BOOL)
				val = new Expression(Boolean.parseBoolean(v.getValue()), v.getType());
			if(v.getType() == Type.INT)
				val = new Expression(Integer.parseInt(v.getValue()), v.getType());
			
			merged = merged.updateVar(v.getLabel(), val);
		}
		
		if(merged.checkSatisfiability() == null)
		{
			this.vars = null;
			c = false;
		}
		
		return c;
	}
		
	/**
	 * compare this state with the one passed by State
	 * 
	 * @param previous
	 * @return	HashSet<Label> changed labels
	 * @throws AutomatonException 
	 */
	public HashSet<Label> getChangedLabels(State previous) throws AutomatonException
	{
		HashSet<Label> lbls = new HashSet<Label>();
		// iterates over all the vars of the current state
		for(Variable v : this.getVariables())
			// if the var is NOT contained in the prev state it means it changed
			if(!previous.getVariables().contains(v))
				lbls.add(v.getLabel());
		
		return lbls;
	}
	
	/**
	 * compare this state with the one passed by State
	 * 
	 * @param previous
	 * @return	HashSet<Variable> changed variables
	 */
	public HashSet<Variable> getChangedVariables(State previous)
	{
		HashSet<Variable> vars = new HashSet<Variable>();
		// iterates over all the vars of the current state
		for(Variable v : this.getVariables())
			// if the var is NOT contained in the prev state it means it changed
			if(!previous.getVariables().contains(v))
				vars.add(v);
		
		return vars;
	}
	
	
	/**
	 * compare this state with the one passed by ID
	 * 
	 * @param previous
	 * @param st
	 * @return	HashSet<Label> changed labels
	 * @throws AutomatonException 
	 */
	public HashSet<Label> getChangedLabels(BigInteger previous, StatesTable st) throws AutomatonException
	{
		// get the state from the id passed
		State prevState = st.getStateFromID(previous);		
		return this.getChangedLabels(prevState);
	}
	
	/**
	 * Static compare this state with the one passed by ID
	 * 
	 * @param previous
	 * @param st
	 * @return	HashSet<Label> changed labels
	 * @throws AutomatonException 
	 */
	public static HashSet<Label> getChangedLabels(BigInteger previous, BigInteger current, StatesTable st) throws AutomatonException
	{
		// get the state from the id passed
		State curState = st.getStateFromID(current);
		State prevState = st.getStateFromID(previous);		
		return curState.getChangedLabels(prevState);
	}
	
	@Override
	public String toString() 
	{
		String result = "";
	
		ArrayList<Variable> names = new ArrayList<Variable>(vars);
		Collections.sort(names);
		
		result = result + "〈 ";
		for( Variable v : names )
			result = result + v.getName() + ":" + v.getValue() + ", ";
	
		result = result.substring(0, result.length()-2);
		result = result + " 〉";
		return result;
	}

	public String toGraphString() 
	{
		String result = "";
	
		ArrayList<Variable> names = new ArrayList<Variable>(vars);
		Collections.sort(names);
		
		for( Variable v : names )
			result = result + v.getName() + ":" + v.getValue() + " \n";

		return result;
	}
	
	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vars == null) ? 0 : vars.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		State other = (State) obj;
		
		if (vars == null) 
		{
			if (other.vars != null)
				return false;
		}
		else if (!vars.equals(other.vars))
			return false;
		
		return true;
	}

	
}
