package ecaAnalysis.Classes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import com.microsoft.z3.ArithExpr;
import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.IntExpr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import ecaAnalysis.Classes.Enumerations.Operator;
import ecaAnalysis.Classes.Enumerations.Type;

public class Expression 
{
	
	private Expression leftExpr = null;
	private NodeExpression node = null;
	private Expression rightExpr = null;
	private Type type = null;
	
	/**
	 * Default constructor
	 * 
	 * @param leftExpr
	 * @param op
	 * @param rightExpr
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Expression leftExpr, Operator op, Expression rightExpr, Type type) throws AutomatonException 
	{
		super();
		if(leftExpr == null || op == null || rightExpr == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		this.leftExpr = leftExpr;
		this.node = new NodeExpression(op);
		this.rightExpr = rightExpr;
		this.type = type;
	}
	
	/**
	 * Constructor for unary expressions
	 * 
	 * @param op
	 * @param rightExpr
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Operator op, Expression rightExpr, Type type) throws AutomatonException 
	{
		super();
		if(op == null || rightExpr == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(op);
		this.rightExpr = rightExpr;
		this.type = type;
	}
	
	/**
	 * Constructor for label leafs
	 * 
	 * @param l
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Label l, Type type) throws AutomatonException 
	{
		super();
		if(l == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(l);
		this.rightExpr = null;
		this.type = type;
	}
	
	/**
	 * Constructor for constant leaf of type int
	 * 
	 * @param intVal
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(int intVal, Type type) throws AutomatonException 
	{
		super();
		if(type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(intVal);
		this.rightExpr = null;
		this.type = type;
	}
	
	/**
	 * Constructor for constant leaf of type boolean
	 * 
	 * @param boolVal
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(boolean boolVal, Type type) throws AutomatonException 
	{
		super();
		if(type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(boolVal);
		this.rightExpr = null;
		this.type = type;
	}
	
	/**
	 * Default constructor
	 * 
	 * @param leftExpr
	 * @param op
	 * @param rightExpr
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Expression leftExpr, Operator op, Expression rightExpr, String type) throws AutomatonException 
	{
		super();
		if(leftExpr == null || op == null || rightExpr == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		if(!type.equals("bool") && !type.equals("int"))	
			throw new AutomatonException("Expression type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		this.leftExpr = leftExpr;
		this.node = new NodeExpression(op);
		this.rightExpr = rightExpr;
		
		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;
		
	}
	
	/**
	 * Constructor for unary expressions
	 * 
	 * @param op
	 * @param rightExpr
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Operator op, Expression rightExpr, String type) throws AutomatonException 
	{
		super();
		if(op == null || rightExpr == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		if(!type.equals("bool") && !type.equals("int"))	
			throw new AutomatonException("Expression type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(op);
		this.rightExpr = rightExpr;
		
		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;
	}
	
	/**
	 * Constructor for label leafs
	 * 
	 * @param l
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(Label l, String type) throws AutomatonException 
	{
		super();
		if(l == null || type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		if(!type.equals("bool") && !type.equals("int"))	
			throw new AutomatonException("Expression type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(l);
		this.rightExpr = null;
		
		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;	}
	
	/**
	 * Constructor for constant leaf of type int
	 * 
	 * @param intVal
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(int intVal, String type) throws AutomatonException 
	{
		super();
		if(type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		if(!type.equals("bool") && !type.equals("int"))	
			throw new AutomatonException("Expression type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(intVal);
		this.rightExpr = null;

		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;
		
	}
	
	/**
	 * Constructor for constant leaf of type boolean
	 * 
	 * @param boolVal
	 * @param type
	 * @throws AutomatonException 
	 */
	public Expression(boolean boolVal, String type) throws AutomatonException 
	{
		super();
		if(type == null)
			throw new AutomatonException("Expression fields cannot be NULL");
		
		if(!type.equals("bool") && !type.equals("int"))	
			throw new AutomatonException("Expression type " + type.toUpperCase() + " unkwown: \n	it must be INT or BOOL");
		
		this.leftExpr = null;
		this.node = new NodeExpression(boolVal);
		this.rightExpr = null;

		if(type.equals("bool")) this.type = Type.BOOL;
		else if(type.equals("int")) this.type = Type.INT;
		
	}
	
	// forse vengono duplicate le variabili nel contesto --> male!
	/**
	 * Perform a visit in the Expression and fills a HashSet of 
	 * z3 objects needed to evaluate the model
	 * 
	 * @param ctx the context
	 * @return HashSet<Expr> the Labels converted in z3 objects
	 */
	public HashSet<Expr> getZ3Consts(Context ctx)
	{ 
		HashSet<Expr> z3exprs = new HashSet<Expr>();
		
        if(this.node.isLeaf())
        {
            if(this.node.isLabel())
            {
                if(this.node.getLabel().getType() == Type.INT)
                    z3exprs.add(ctx.mkIntConst(this.node.getLabel().getName()));
                else if(this.node.getLabel().getType() == Type.BOOL)
                    z3exprs.add(ctx.mkBoolConst(this.node.getLabel().getName()));
            }   
        }
        else
        {
            if(this.leftExpr != null)
            	z3exprs.addAll(this.leftExpr.getZ3Consts(ctx));
            z3exprs.addAll(this.rightExpr.getZ3Consts(ctx));
        }
		
		return z3exprs;
	}
	
	public Expr z3Expression(Context ctx)
	{
		Expr z3Expr = null;
		
		// caso base
		if(this.node.isLeaf())
		{
			if(this.node.isBoolean())
				z3Expr = ctx.mkBool(this.node.getBoolVal());
			else if(this.node.isInteger())
				z3Expr = ctx.mkInt(this.node.getIntVal());
			else if(this.node.isLabel())
			{
				if(this.node.getLabel().getType() == Type.INT)
					z3Expr = ctx.mkIntConst(this.node.getLabel().getName());
				else if(this.node.getLabel().getType() == Type.BOOL)
					z3Expr = ctx.mkBoolConst(this.node.getLabel().getName());
			}				
		}
		else
		{
			switch (this.node.getOp()) {
				case ADD:
					z3Expr = ctx.mkAdd((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case SUB:
					if(this.leftExpr != null)
						z3Expr = ctx.mkSub((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					else
						z3Expr = ctx.mkUnaryMinus((ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case MUL:
					z3Expr = ctx.mkMul((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case DIV:
					z3Expr = ctx.mkDiv((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case MOD:
					z3Expr = ctx.mkMod((IntExpr) this.leftExpr.z3Expression(ctx), (IntExpr) this.rightExpr.z3Expression(ctx));
					break;
				case LT:
					z3Expr = ctx.mkLt((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case LE:
					z3Expr = ctx.mkLe((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case GT:
					z3Expr = ctx.mkGt((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case GE:
					z3Expr = ctx.mkGe((ArithExpr) this.leftExpr.z3Expression(ctx), (ArithExpr) this.rightExpr.z3Expression(ctx));
					break;
				case AND:
					z3Expr = ctx.mkAnd((BoolExpr) this.leftExpr.z3Expression(ctx), (BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				case OR:
					z3Expr = ctx.mkOr((BoolExpr) this.leftExpr.z3Expression(ctx), (BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				case NOT:
					z3Expr = ctx.mkNot((BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				case EQ:
					z3Expr = ctx.mkEq( this.leftExpr.z3Expression(ctx), this.rightExpr.z3Expression(ctx));
					break;
				case NEQ:
					z3Expr = ctx.mkEq( this.leftExpr.z3Expression(ctx), this.rightExpr.z3Expression(ctx));
					z3Expr = ctx.mkNot((BoolExpr)z3Expr);
					break;
				case IMP:
					z3Expr = ctx.mkImplies((BoolExpr) this.leftExpr.z3Expression(ctx), (BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				case XOR:
					z3Expr = ctx.mkXor((BoolExpr) this.leftExpr.z3Expression(ctx), (BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				case IFF:
					z3Expr = ctx.mkIff((BoolExpr) this.leftExpr.z3Expression(ctx), (BoolExpr) this.rightExpr.z3Expression(ctx));
					break;
				default:
					break;
			}
		}	
		return z3Expr;
	}
	
	public Expression T2ExpressionWithoutBooleans() throws AutomatonException
	{
		Expression T2 = null;

		// caso base
		if(this.node.isLeaf())
		{	
			if(this.node.isBoolean())
			{
				if(this.node.getBoolVal()==true)
					T2 = new Expression(1, Type.INT);				
				else if(this.node.getBoolVal()==false)
					T2 = new Expression(0, Type.INT);				
			}
			else if(this.node.isInteger())
				T2 = new Expression(this.node.getIntVal(), Type.INT);
			else if(this.node.isLabel())
				T2 = new Expression(this.node.getLabel(), Type.INT);
		}
		else
		{
			
			Expression newLeftExpr = null;
			Expression newRightExpr = null;
			
			if(this.leftExpr != null)
				newLeftExpr = this.leftExpr.T2ExpressionWithoutBooleans();
			newRightExpr = this.rightExpr.T2ExpressionWithoutBooleans();
						
			switch (this.node.getOp()) 
			{
				case ADD:
					T2 = new Expression(newLeftExpr, Operator.ADD, newRightExpr, Type.INT);
					break;
				case SUB:
					if(this.leftExpr != null)
						T2 = new Expression(newLeftExpr, Operator.SUB, newRightExpr, Type.INT);
					else
						T2 = new Expression(Operator.SUB, newRightExpr, Type.INT);
					break;
				case MUL:
					T2 = new Expression(newLeftExpr, Operator.MUL, newRightExpr, Type.INT);
					break;
				case DIV:
					T2 = new Expression(newLeftExpr, Operator.DIV, newRightExpr, Type.INT);
					break;
				case MOD:
					T2 = new Expression(newLeftExpr, Operator.MOD, newRightExpr, Type.INT);
					break;
				case LT:
					T2 = new Expression(newLeftExpr, Operator.LT, newRightExpr, Type.BOOL);
					break;
				case LE:
					T2 = new Expression(newLeftExpr, Operator.LE, newRightExpr, Type.BOOL);
					break;
				case GT:
					T2 = new Expression(newLeftExpr, Operator.GT, newRightExpr, Type.BOOL);
					break;
				case GE:
					T2 = new Expression(newLeftExpr, Operator.GE, newRightExpr, Type.BOOL);
					break;
				case AND:
					T2 = new Expression(new Expression(newLeftExpr, Operator.MUL, newRightExpr, Type.INT), Operator.MOD, new Expression(2, Type.INT), Type.INT);
					break;
				case OR:
					T2 = new Expression(new Expression(newLeftExpr, Operator.ADD, newRightExpr, Type.INT), Operator.MOD, new Expression(2, Type.INT), Type.INT);
					break;
				case NOT:
					T2 = new Expression(new Expression(newRightExpr, Operator.SUB, new Expression(1,Type.INT), Type.BOOL), Operator.MOD, new Expression(2, Type.INT), Type.INT);
					break;
				case EQ:
					T2 = new Expression(newLeftExpr, Operator.EQ, newRightExpr, Type.BOOL);
					break;
				case NEQ:
					T2 = new Expression(newLeftExpr, Operator.EQ, newRightExpr, Type.BOOL);
					T2 = new Expression(Operator.NOT, T2, Type.BOOL);
					break;					
				case IMP:
					T2 = new Expression(newLeftExpr, Operator.IMP, newRightExpr, Type.BOOL);
					break;
				case XOR:
					T2 = new Expression(newLeftExpr, Operator.XOR, newRightExpr, Type.BOOL);
					break;
				case IFF:
					T2 = new Expression(newLeftExpr, Operator.IFF, newRightExpr, Type.BOOL);
					break;
				default:
					break;
			}
		}	
	
		return T2;
	}
	
	public Expression T2Expression() throws AutomatonException
	{
		Expression t2 = null;
		
		if(this.node.isLeaf() && this.node.isBoolean())
		{
			if(this.node.getBoolVal() == true)
				t2 = new Expression(new Expression(1, Type.INT), Operator.EQ, new Expression(1, Type.INT), Type.BOOL);
			else
				t2 = new Expression(new Expression(0, Type.INT), Operator.EQ, new Expression(1, Type.INT), Type.BOOL);
		}
			
			
		else
			t2 = this.recursiveT2Expression();
		
		
		return t2;
	}
	
	
	private Expression recursiveT2Expression() throws AutomatonException
	{
		Expression T2 = null;

		// caso base
		if(this.node.isLeaf())
		{	
			if(this.node.isBoolean())
			{
				if(this.node.getBoolVal()==true)
					T2 = new Expression(1, Type.INT);				
				else if(this.node.getBoolVal()==false)
					T2 = new Expression(0, Type.INT);				
			}
			else if(this.node.isInteger())
				T2 = new Expression(this.node.getIntVal(), Type.INT);
			else if(this.node.isLabel())
				T2 = new Expression(this.node.getLabel(), Type.INT);		
		}
		else
		{
			
			Expression newLeftExpr = null;
			Expression newRightExpr = null;
			
			if(this.node.getOp() != Operator.EQ && this.node.getOp() != Operator.NEQ)
			{
				if(this.leftExpr != null)
				{
					if(this.leftExpr.node.isLeaf() && this.leftExpr.getType() == Type.BOOL)
						newLeftExpr = new Expression(this.leftExpr, Operator.EQ, new Expression(true, Type.BOOL), Type.BOOL);
					else 
						newLeftExpr = this.leftExpr.recursiveT2Expression();
				}

				if(this.rightExpr.node.isLeaf() && this.rightExpr.getType() == Type.BOOL)
					newRightExpr = new Expression(this.rightExpr, Operator.EQ, new Expression(true, Type.BOOL), Type.BOOL);	
				else
					newRightExpr = this.rightExpr.recursiveT2Expression();
			}
			else
			{
				if(this.leftExpr != null)
					newLeftExpr = this.leftExpr.recursiveT2Expression();
				newRightExpr = this.rightExpr.recursiveT2Expression();
			}
			
			switch (this.node.getOp()) 
			{
				case ADD:
					T2 = new Expression(newLeftExpr, Operator.ADD, newRightExpr, Type.INT);
					break;
				case SUB:
					if(this.leftExpr != null)
						T2 = new Expression(newLeftExpr, Operator.SUB, newRightExpr, Type.INT);
					else
						T2 = new Expression(Operator.SUB, newRightExpr, Type.INT);
					break;
				case MUL:
					T2 = new Expression(newLeftExpr, Operator.MUL, newRightExpr, Type.INT);
					break;
				case DIV:
					T2 = new Expression(newLeftExpr, Operator.DIV, newRightExpr, Type.INT);
					break;
				case MOD:
					T2 = new Expression(newLeftExpr, Operator.MOD, newRightExpr, Type.INT);
					break;
				case LT:
					T2 = new Expression(newLeftExpr, Operator.LT, newRightExpr, Type.BOOL);
					break;
				case LE:
					T2 = new Expression(newLeftExpr, Operator.LE, newRightExpr, Type.BOOL);
					break;
				case GT:
					T2 = new Expression(newLeftExpr, Operator.GT, newRightExpr, Type.BOOL);
					break;
				case GE:
					T2 = new Expression(newLeftExpr, Operator.GE, newRightExpr, Type.BOOL);
					break;
				case AND:
					T2 = new Expression(newLeftExpr, Operator.AND, newRightExpr, Type.BOOL);
					break;
				case OR:
					T2 = new Expression(newLeftExpr, Operator.OR, newRightExpr, Type.BOOL);
					break;
				case NOT:
					T2 = new Expression(Operator.NOT, newRightExpr, Type.BOOL);
					break;
				case EQ:
					T2 = new Expression(newLeftExpr, Operator.EQ, newRightExpr, Type.BOOL);
					break;
				case NEQ:
					T2 = new Expression(newLeftExpr, Operator.EQ, newRightExpr, Type.BOOL);
					T2 = new Expression(Operator.NOT, T2, Type.BOOL);
					break;					
				case IMP:
					T2 = new Expression(newLeftExpr, Operator.IMP, newRightExpr, Type.BOOL);
					break;
				case XOR:
					T2 = new Expression(newLeftExpr, Operator.XOR, newRightExpr, Type.BOOL);
					break;
				case IFF:
					T2 = new Expression(newLeftExpr, Operator.IFF, newRightExpr, Type.BOOL);
					break;
				default:
					break;
			}
		}	
	
		return T2;
	}
	
	
	@Override
	public Expression clone()
	{
		Expression newE = null;

		try
		{
			// if the epression is like "a + 10"
			if(this.leftExpr != null && this.rightExpr != null)
				newE = new Expression(this.leftExpr.clone(), this.node.getOp(), this.rightExpr.clone(), this.type);
			// if the expression is like "!a"
			else if(this.leftExpr == null && this.rightExpr != null)
				newE = new Expression(this.node.getOp(), this.rightExpr.clone(), this.type);
			// if the expression is like "a"
			else if(this.node.isLeaf() && this.node.isLabel())
				newE = new Expression(this.node.getLabel(), this.type);
			// if the expression is like "10"
			else if(this.node.isLeaf() && this.node.isInteger())
				newE = new Expression(this.node.getIntVal(), this.type);
			// if the expression is like "true"
			else if(this.node.isLeaf() && this.node.isBoolean())
				newE = new Expression(this.node.getBoolVal(), this.type);
		}
		catch (AutomatonException e){ e.printStackTrace();	}
		
		return newE;
	}
	
	
	/**
	 * replace all the occurrences of the specified label with the given expression
	 * 
	 * @param updating Label to be updated
	 * @param update new Expression
	 * @return updated Expression
	 * @throws AutomatonException 
	 */
	public Expression updateVar(Label updating, Expression update) throws AutomatonException
	{

		Expression updated = null;
		
        if(this.node.isLeaf())
        {
            if(this.node.isLabel())
            {
            	// if the current node matches the variable to update, we replace it
                if(this.node.getLabel().equals(updating))  
                	updated = update;
            	// if the current node is not the updating label, we return the label itself
                else
                	updated = new Expression(this.node.getLabel(), this.getType());
    
            }
   
            // if the current node is an integer or a boolean, we return the leaf itself
            else if(this.node.isInteger())
            	updated = new Expression(this.node.getIntVal(), this.getType());
   
            else if(this.node.isBoolean())
            	updated = new Expression(this.node.getBoolVal(), this.getType());

        }
        // if we didn't reach a leaf, we recursively call this method to update the right 
        // and left expressions and then we merge the result
        else
        {
        	
        	Expression r = this.rightExpr.updateVar(updating, update);
        	
        	if(this.leftExpr != null)
        	{
        		Expression l = this.leftExpr.updateVar(updating, update);
        		updated = new Expression(l, this.node.getOp(), r, this.getType());
        	}
        	else
        	{
        		updated = new Expression(this.node.getOp(), r, this.getType());
        	}        
        }
		
        return updated;
	}
	
	public Expression replaceLabels(HashMap<Label, Label> updates) throws AutomatonException
	{
		Expression updated = this;
		
		for(Label l : updates.keySet())
			updated = updated.updateVar(l, new Expression(updates.get(l), l.getType()));
			
		return updated;
	}
	
	
	public Expression getLeftExpr() 
	{
		return leftExpr;
	}

	public NodeExpression getNode() 
	{
		return node;
	}

	public Expression getRightExpr() 
	{
		return rightExpr;
	}

	public Type getType() 
	{
		return type;
	}

	// get all the labels that appear in the expression
	public HashSet<Label> getLabels() 
	{
		HashSet<Label> lbls = new HashSet<Label>();
		
		if(this.node.isLeaf())
		{
			if(this.node.isLabel())
				lbls.add(this.node.getLabel());
		}
		else
		{
			if(this.leftExpr != null)
				lbls.addAll(this.leftExpr.getLabels());
			lbls.addAll(this.rightExpr.getLabels());
		}
		
		return lbls;
	}
	
	/**
	 * Merge all the expressions into a single one using ANDs
	 * 
	 * @param HashSet<Invariant>
	 * @return Expression
	 * @throws AutomatonException 
	 */
	public static Expression merge(HashSet<Expression> expressions) throws AutomatonException
	{
		Expression mergedExpr = null;
		
		// check if the set is not empty
		if(!expressions.isEmpty())
		{					
			Iterator<Expression> iter = expressions.iterator();
			mergedExpr = iter.next();
			if(mergedExpr.getType() != Type.BOOL)
				throw new AutomatonException("Only boolean expressions can be merged");
			
			while(iter.hasNext()) 
			{
				Expression nextExpr = iter.next();
				if(nextExpr.getType() != Type.BOOL)
					throw new AutomatonException("Only boolean expressions can be merged");
				
			    mergedExpr = new Expression(mergedExpr, Operator.AND, nextExpr, Type.BOOL);
			}    
		}
		
		return mergedExpr;
	}
	
	/**
	 * Merge two expressions using ANDs
	 * 
	 * @param e1 the first expression to merge
	 * @param e2 the second expression to merge
	 * @return the merged expression
	 * @throws AutomatonException 
	 */
	public static Expression merge(Expression e1, Expression e2) throws AutomatonException
	{
		// create an hashset containing the two expressions
		HashSet<Expression> expressions = new HashSet<Expression>();
		expressions.add(e1);
		expressions.add(e2);
		// merge them and return using the general method
		return merge(expressions);
	}
	
	/**
	 * take all the invariants, convert them into CNF form and split them by the AND operator
	 * @throws AutomatonException 
	 */
	public HashSet<Expression> getExpressionsInCNF() throws AutomatonException
	{
		// set of operators that must be processed in a certant way
		HashSet<Operator> boolOps = new HashSet<Operator>(Arrays.asList(Operator.OR, Operator.AND, Operator.NOT));
		
		// list of clauses to return
		HashSet<Expression> clauses = new HashSet<Expression>();
		
		// if there is an operator and it is an AND, an OR or a NOT
		if(this.node.isOperator() && boolOps.contains(this.node.getOp()))
		{
			if( this.node.getOp() == Operator.OR )
			{
				HashSet<Expression> Lclauses = this.leftExpr.getExpressionsInCNF();
				HashSet<Expression> Rclauses = this.rightExpr.getExpressionsInCNF();
				// the for loops generate all the combination between the expressions to obtain the CNF form
                for(Expression le : Lclauses)
                	for(Expression re : Rclauses)
                		clauses.add(new Expression(le, Operator.OR, re, Type.BOOL));
			}
	
			if( this.node.getOp() == Operator.AND )
			{
				clauses.addAll(this.leftExpr.getExpressionsInCNF());
				clauses.addAll(this.rightExpr.getExpressionsInCNF());
			}
	
			if( this.node.getOp() == Operator.NOT )
			{				
				if(this.rightExpr.getNode().isLeaf())
					clauses.add(this);
				else if(this.rightExpr.getNode().getOp() == Operator.NOT)
				{
					for(Expression ne : this.rightExpr.getExpressionsInCNF())
						clauses.add(new Expression(Operator.NOT, ne, Type.BOOL));
				}
				else if(this.rightExpr.getNode().getOp() == Operator.AND)
				{
					Expression leftNot = new Expression(Operator.NOT, this.rightExpr.leftExpr, Type.BOOL);
					Expression rightNot = new Expression(Operator.NOT, this.rightExpr.rightExpr, Type.BOOL);		
					Expression deMorgan = new Expression(leftNot, Operator.OR, rightNot, Type.BOOL);
					clauses.addAll(deMorgan.getExpressionsInCNF());
				}
				else if(this.rightExpr.getNode().getOp() == Operator.OR)
				{
					Expression leftNot = new Expression(Operator.NOT, this.rightExpr.leftExpr, Type.BOOL);
					Expression rightNot = new Expression(Operator.NOT, this.rightExpr.rightExpr, Type.BOOL);		
					Expression deMorgan = new Expression(leftNot, Operator.AND, rightNot, Type.BOOL);
					clauses.addAll(deMorgan.getExpressionsInCNF());
				}		
			}
		}
		else
		{
			//caso base
			clauses.add(this);
		}
				
		return clauses;
	}
	
	
	/**
	 * check the Satisfiability of the expression
	 * 
	 * @param context
	 * @return a Model object if the expression is satisfiable, null otherwise
	 */
	public Model checkSatisfiability(Context ctx)
	{
        Expr z3expr = this.z3Expression(ctx);
        Model model = null;
        
		try 
		{ 
	       
	        Solver s = ctx.mkSolver();
	        s.add((BoolExpr) z3expr);
	        if (s.check() == Status.SATISFIABLE)
	            model = s.getModel();
	        else
	            model = null;
		}
        catch (Exception e) { e.printStackTrace(); }

		return model;
	}
	
	/**
	 * check the Satisfiability of the expression without passing a context
	 * 
	 * @return a Model object if the expression is satisfiable, null otherwise
	 */
	public Model checkSatisfiability()
	{
		// Settin up Z3
		HashMap<String, String> cfg = new HashMap<String, String>();
		cfg.put("model", "true");
		Context ctx = new Context(cfg);
		
		return this.checkSatisfiability(ctx);
	}
	
	@Override
	public String toString() 
	{
		String expr = "";
		
		if(this.node.isLeaf())
			expr = expr + this.node.toString();
		else if(this.leftExpr == null)
			expr = expr + "(" + this.node.toString() + this.rightExpr.toString() + ")";
		else
			expr = expr + "(" + this.leftExpr.toString() + " " + this.node.toString() + " " + this.rightExpr.toString() + ")";
		
		return expr;
	}
	
	public String toT2String() 
	{
		String t2expr = this.toString();
		t2expr = t2expr.replace("|", "||");
		t2expr = t2expr.replace("&", "&&");
		return t2expr;
	}
	


	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((leftExpr == null) ? 0 : leftExpr.hashCode());
		result = prime * result + ((node == null) ? 0 : node.hashCode());
		result = prime * result + ((rightExpr == null) ? 0 : rightExpr.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Expression other = (Expression) obj;
		if (leftExpr == null) 
		{
			if (other.leftExpr != null)
				return false;
		}
		else if (!leftExpr.equals(other.leftExpr))
			return false;
		if (node == null) 
		{
			if (other.node != null)
				return false;
		} 
		else if (!node.equals(other.node))
			return false;
		if (rightExpr == null) 
		{
			if (other.rightExpr != null)
				return false;
		}
		else if (!rightExpr.equals(other.rightExpr))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

















	/*
	 ***************************************************************************************************** 
	 ***************************************************************************************************** 
	*/
	public class NodeExpression
	{
		private Operator op = null;
		private Label l = null;
		private Integer intVal = null;
		private Boolean boolVal = null;
		
		public NodeExpression(Operator op) throws AutomatonException 
		{
			super();
			if(op == null)
				throw new AutomatonException("Operator field cannot be NULL");
			this.op = op;
		}
		
		public NodeExpression(Label l) throws AutomatonException 
		{
			super();
			if(l == null)
				throw new AutomatonException("Label field cannot be NULL");
			this.l = l;
		}
		
		public NodeExpression(Integer intVal) throws AutomatonException 
		{
			super();
			if(intVal == null)
				throw new AutomatonException("intVal field cannot be NULL");
			this.intVal = intVal;
		}
		
		public NodeExpression(Boolean boolVal) throws AutomatonException 
		{
			super();
			if(boolVal == null)
				throw new AutomatonException("boolVal field cannot be NULL");
			this.boolVal = boolVal;
		}
		
		
		public Operator getOp() 
		{
			return op;
		}
		
		public Label getLabel() 
		{
			return l;
		}
		
		public Integer getIntVal() 
		{
			return intVal;
		}
		
		public Boolean getBoolVal() 
		{
			return boolVal;
		}
		
		public boolean isLeaf()
		{
			boolean leaf = false;
			
			if(this.op != null)
				leaf = false;
			else if(this.l != null || this.intVal != null || this.boolVal != null)
				leaf = true;
			
			return leaf;
		}
		
		public boolean isOperator()
		{
			boolean opF = false;
			
			if(this.op != null && this.l == null && this.intVal == null && this.boolVal == null)
				opF = true;
			
			return opF;
		}

		public boolean isLabel()
		{
			boolean lF = false;
			
			if(this.op == null && this.l != null && this.intVal == null && this.boolVal == null)
				lF = true;
			
			return lF;
		}

		public boolean isInteger()
		{
			boolean intF = false;
			
			if(this.op == null && this.l == null && this.intVal != null && this.boolVal == null)
				intF = true;
			
			return intF;
		}

		public boolean isBoolean()
		{
			boolean boolF = false;
			
			if(this.op == null && this.l == null && this.intVal == null && this.boolVal != null)
				boolF = true;
			
			return boolF;
		}

		
		@Override
		public NodeExpression clone()
		{
			NodeExpression newN = null;
			
			try
			{
				if(this.op != null)
					newN = new NodeExpression(this.op);
				
				else if(this.l != null)
					newN = new NodeExpression(this.l);
				
				else if(this.intVal != null)
					newN = new NodeExpression(this.intVal.intValue());
				
				else if(this.boolVal != null)
					newN = new NodeExpression(this.boolVal.booleanValue());				
			}
			catch (AutomatonException e){ e.printStackTrace(); }
			
			return newN;
		
		}
		
		@Override
		public String toString() 
		{
			String node = "";
			
			if(this.isLabel())
				node += this.getLabel().getName();
			
			else if(this.isInteger())
				node += this.getIntVal().toString();
				
			else if(this.isBoolean())
				node += this.getBoolVal().toString();
			
			// sicuramente va cambiato
			else if(this.isOperator())
				node += this.getOp().toString();
			
			return node;
		}
	
		
		
		@Override
		public int hashCode() 
		{
			final int prime = 31;
			int result = 1;
			result = prime * result + ((boolVal == null) ? 0 : boolVal.hashCode());
			result = prime * result + ((intVal == null) ? 0 : intVal.hashCode());
			result = prime * result + ((l == null) ? 0 : l.hashCode());
			result = prime * result + ((op == null) ? 0 : op.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) 
		{
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			NodeExpression other = (NodeExpression) obj;
			if (boolVal == null) {
				if (other.boolVal != null)
					return false;
			} 
			else if (!boolVal.equals(other.boolVal))
				return false;
			if (intVal == null) 
			{
				if (other.intVal != null)
					return false;
			}
			else if (!intVal.equals(other.intVal))
				return false;
			if (l == null) 
			{
				if (other.l != null)
					return false;
			}
			else if (!l.equals(other.l))
				return false;
			if (op != other.op)
				return false;
			return true;
		}
	
		
		
	
		
		

		
		
		
		
	}

}
