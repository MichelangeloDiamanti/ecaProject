package ecaAnalysis.Classes;

import java.math.BigInteger;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

import ecaAnalysis.Classes.AutomatonCore.Edge;
import ecaAnalysis.Classes.AutomatonCore.Vertex;

public class Cluster 
{

	// sensors used to index EVERY cluster 
	private static HashSet<Label> clusterIndexing = new HashSet<Label>();
	
	// values of the sensors used to identify this particular cluster
	private HashSet<Variable> clusterID = new HashSet<Variable>(); 
	
	// set of vertices and edges contained in this cluster
	private ConcurrentHashMap<BigInteger, Vertex> clusterVertices = new ConcurrentHashMap<BigInteger, Vertex>();
	private ConcurrentHashMap<String, Edge> clusterEdges = new ConcurrentHashMap<String, Edge>();
	
	
	public Cluster(HashSet<Variable> clusterID) throws AutomatonException 
	{
		super();
	
		// check if the set passed is not empty
		if(clusterID.isEmpty())
			throw new AutomatonException("Cannot pass an empty ID");
			
		// check if the ID contains no actuators
		for(Variable v : clusterID)
			if(v.isActuator())
				throw new AutomatonException("The Cluster can only be indexed with sensors");
		
		// if the set of sensors is not defined, then the first cluster created must fill it
		if(Cluster.clusterIndexing.isEmpty())
		{
			for(Variable v : clusterID)
				Cluster.clusterIndexing.add(v.getLabel());
		}
		// if every cluster uses the same sensors as ID
		else
		{
			HashSet<Label> tmpIndexing = new HashSet<Label>();
			for(Variable v : clusterID)
				tmpIndexing.add(v.getLabel());
			
			if(!tmpIndexing.equals(Cluster.clusterIndexing))
				throw new AutomatonException("All the Clusters must have the same indexing");
		}
							
		this.clusterID = clusterID;
	}


	private static HashSet<Variable> getClusterID(State s)
	{
		HashSet<Variable> ID = new HashSet<Variable>();
		
		for(Variable v : s.getVariables())
			if(v.isSensor())
				ID.add(v);
	
		return ID;
	}
	
	public static HashSet<Variable> getClusterID(Vertex vertex, StatesTable statestable) throws AutomatonException
	{
		return Cluster.getClusterID(vertex.getState(statestable));
	}

	public static HashSet<Variable> getClusterID(Edge edge, StatesTable statestable) throws AutomatonException
	{
		HashSet<Variable> id = new HashSet<Variable>();
		
		if(edge.getTo().isConsistent())
			id = Cluster.getClusterID(edge.getTo().getState(statestable));
		else
			id = Cluster.getClusterID(edge.getFrom().getState(statestable));
		
		return id;
	}
	
	private boolean isPartOfCluster(Vertex vertex, StatesTable statestable) throws AutomatonException
	{
		boolean ipoc = true;
		
		// get the hashs
		HashSet<Variable> sensors = vertex.getState(statestable).getVariables();
		Iterator<Variable> sensorIt = sensors.iterator();
		while(sensorIt.hasNext())
		{
			Variable sit = sensorIt.next();
			if(sit.isActuator())
				sensorIt.remove();
		}
				
		if(!sensors.equals(clusterID))
			ipoc = false;
		
		return ipoc;
	}
	
	public void addVertex(Vertex vertex, StatesTable statestable) throws AutomatonException
	{
		if(isPartOfCluster(vertex, statestable))
		{
			this.clusterVertices.put(vertex.getState(), vertex);
		}
		else
			throw new AutomatonException("Cannot add the vertex " + vertex + " to a cluster with a different index");
	}
	
	public void addEdge(Edge edge, StatesTable statestable) throws AutomatonException
	{
		if(isPartOfCluster(edge.getTo(), statestable))
		{
			this.clusterEdges.put(edge.identifier(), edge);
		}
		else
			throw new AutomatonException("Cannot add the edge " + edge + " to a cluster with a different index");
	}
	
	public void addIncorrectEdgeAndVertex(Edge edge, StatesTable statestable) throws AutomatonException
	{
		if(isPartOfCluster(edge.getFrom(), statestable))
		{
			this.clusterEdges.put(edge.identifier(), edge);
			this.clusterVertices.put(edge.getTo().getState(), edge.getTo());
		}
		else
			throw new AutomatonException("Cannot add the edge " + edge + " to a cluster with a different index");
	}

	private void forceVertex(Vertex vertex)
	{
		this.clusterVertices.put(vertex.getState(), vertex);
	}
	
	private void forceEdge(Edge edge)
	{
		this.clusterEdges.put(edge.identifier(), edge);
	}
	
	public ConcurrentHashMap<BigInteger, Vertex> getClusterVertices() 
	{
		return clusterVertices;
	}

	public ConcurrentHashMap<String, Edge> getClusterEdges() 
	{
		return clusterEdges;
	}


	public HashSet<Variable> getClusterID() 
	{
		return clusterID;
	}
	
	public static void clear()
	{
		Cluster.clusterIndexing.clear();
	}

	@Override
	public Cluster clone() 
	{
		HashSet<Variable> id = new HashSet<Variable>();
		
		for(Variable v : this.clusterID)
			id.add(v.clone());
				
		Cluster newC = null;
		try 
		{
			newC = new Cluster(id);
		
			for(BigInteger v : this.clusterVertices.keySet())
				newC.forceVertex(this.clusterVertices.get(v));
	
			for(String e : this.clusterEdges.keySet())
				newC.forceEdge(this.clusterEdges.get(e));
		
		} catch (AutomatonException e1){ e1.printStackTrace(); } 
		
		return newC;
	}

	
}
