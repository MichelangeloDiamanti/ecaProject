package ecaAnalysis.Classes.Enumerations;

public enum Stability
{ 
	STABLE,
	UNSTABLE,
	INCORRECT
}
