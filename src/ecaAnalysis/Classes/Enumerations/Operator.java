package ecaAnalysis.Classes.Enumerations;

public enum Operator
{
    ADD, // +
    SUB, // -
    MUL, // *
    DIV, // /
    MOD, // %
    LT,  // <
    LE,  // <=
    GT,  // >
    GE,  // >=
    AND, // &
    OR,  // |
    NOT, // !
    EQ,  // ==
    NEQ, // !=
	ASS, // :=
	IMP, // =>
	XOR, // x|
	IFF; // <->
	
	static public Operator getOperatorFromString(String op)
	{
		Operator r = null;
		op = op.replace(" ", "");
		
		if(op.equals("+")) r = Operator.ADD;
		else if(op.equals("-")) r = Operator.SUB;
		else if(op.equals("*")) r = Operator.MUL;
		else if(op.equals("/")) r = Operator.DIV;
		else if(op.equals("%")) r = Operator.MOD;
		else if(op.equals("<")) r = Operator.LT;
		else if(op.equals("<=")) r = Operator.LE;
		else if(op.equals(">")) r = Operator.GT;
		else if(op.equals(">=")) r = Operator.GE;
		else if(op.equals("&")) r = Operator.AND;
		else if(op.equals("|")) r = Operator.OR;
		else if(op.equals("!")) r = Operator.NOT;
		else if(op.equals("==")) r = Operator.EQ;
		else if(op.equals("!=")) r = Operator.NEQ;
		else if(op.equals(":=")) r = Operator.ASS;
		else if(op.equals("=>")) r = Operator.IMP;
		else if(op.equals("x|")) r = Operator.XOR;
		else if(op.equals("<->")) r = Operator.IFF;
		
		return r;
	}
	
	@Override
	public String toString()
	{
		String r = "";
		
		if(this == Operator.ADD) r = "+";
        else if(this == Operator.SUB) r = "-";
        else if(this == Operator.MUL) r = "*";
        else if(this == Operator.DIV) r = "/";
        else if(this == Operator.MOD) r = "%";
        else if(this == Operator.LT) r = "<";
        else if(this == Operator.LE) r = "<=";
        else if(this == Operator.GT) r = ">";
        else if(this == Operator.GE) r = ">=";
        else if(this == Operator.AND) r = "&";
        else if(this == Operator.OR) r = "|";
        else if(this == Operator.NOT) r = "!";
        else if(this == Operator.EQ) r = "==";
        else if(this == Operator.NEQ) r = "!=";
        else if(this == Operator.ASS) r = ":=";
        else if(this == Operator.IMP) r = "=>";
        else if(this == Operator.XOR) r = "x|";
        else if(this == Operator.IFF) r = "<->";
		
		return r;
	}
}
