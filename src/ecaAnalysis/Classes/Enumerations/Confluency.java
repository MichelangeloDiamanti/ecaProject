package ecaAnalysis.Classes.Enumerations;

public enum Confluency 
{
	CYCLIC,
	INCORRECT,
	NON_CONFLUENT,
	CONFLUENT
}
