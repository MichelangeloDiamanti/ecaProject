package ecaAnalysis.Classes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;

import ecaAnalysis.Classes.AutomatonUtils;
import ecaAnalysis.Classes.Enumerations.Type;

public class StatesTable 
{

	// number of combinations of the variable values
	private BigInteger size = new BigInteger("0");
	
	// all the variables and their possible values
	private ArrayList<ArrayList<Variable>> vars = new ArrayList<ArrayList<Variable>>();

	// a unique invariant to check every time a new State is generated
	private Invariant invariant = null; 
	    
    /**
     * Constructor for an ArrayList of ArrayLists of Variables.
     * This method does not instantiate the variables, so the Lists need
     * to be populated beforehand
     *  
     * @param vars ArrayList< ArrayList< Variable > >
     * @throws AutomatonException 
     */
	public StatesTable(ArrayList<ArrayList<Variable>> vars) throws AutomatonException 
	{
		super();
		
		this.invariant = new Invariant( new Expression(true, Type.BOOL) );
		
		ArrayList<ArrayList<Variable>> aav = new ArrayList<ArrayList<Variable>>();
		for(ArrayList<Variable> av : vars)
		{
			ArrayList<Variable> avt = new ArrayList<Variable>();
			for(Variable v : av)
				avt.add(v);
			aav.add(avt);
		}
			
		
		this.vars = aav;
		
		// get the length of the table
		this.size = new BigInteger("1");
		for(ArrayList<Variable> al : this.vars)
			this.size = this.size.multiply(new BigInteger( new Integer(al.size()).toString()) );
	}

    /**
     * Constructor for an ECAfile.
     * This method instantiate the variables using the set of labels 
     * and the set of invariants
     *  
     * @param ECAfile
     * @throws AutomatonException 
     */
	public StatesTable(ECAfile ecafile) throws AutomatonException 
	{
		super();
		
		this.invariant = new Invariant( new Expression(true, Type.BOOL) );
		
		// make a deep copy of the ECAfile	
		ECAfile tmpECAfile = ecafile.clone();
		tmpECAfile.setInvariantsInCNF();
		
		// the variables are instantiated from the labels and the invariants
		this.vars = createVariables(tmpECAfile.getLabels(), tmpECAfile.getInvariants());
	
		// get the length of the table
		this.size = new BigInteger("1");
		for(ArrayList<Variable> al : this.vars)
			this.size = this.size.multiply(new BigInteger( new Integer(al.size()).toString()) );
	}
	
	/**
	 * the variables are instantiated from the labels and the invariants
	 * 
	 * @param HashSet<Label>
	 * @param HashSet<Invariant>
	 * @return ArrayList<ArrayList<Variable>>
	 * @throws AutomatonException 
	 */
	private ArrayList<ArrayList<Variable>> createVariables(HashSet<Label> labels, HashSet<Invariant> invariants) throws AutomatonException
	{
		// check and delete the invariants with no labels in it (eg: 3 < 4)
	    checkConstInvariants(invariants);    		        	
	        
		// instantiate the list that will be returned
		ArrayList<ArrayList<Variable>> vars = new ArrayList<ArrayList<Variable>>();

		// loop over every label to instantiate the corresponding variables
		for(Label l : labels)
		{
			// the list of the possible variables for the current label
			ArrayList<Variable> var = new ArrayList<Variable>();
			
			// for int labels the range is [ -128 ; 127 ]
			if(l.getType() == Type.INT)
				for(int i=AutomatonUtils.INT_MIN_VAL; i<=AutomatonUtils.INT_MAX_VAL; i++)
				{
					// check all the invariants with only one label to avoid getting inconsistent variables
					// eg: [ A > 10 ]
					if(checkSingleVarInvariants(invariants, new Variable(l,i)))
						// the variable is added to the list only if its value is consistent
						var.add(new Variable(l,i));
				}
	
			// for bool labels the options are [ false ; true ]
			else if(l.getType() == Type.BOOL)
			{
				// check all the invariants with only one label to avoid getting inconsistent variables
				if(checkSingleVarInvariants(invariants, new Variable(l,false)))
					// the variable is added to the list only if its value is consistent
					var.add(new Variable(l,false));
				
				// check all the invariants with only one label to avoid getting inconsistent variables
				if(checkSingleVarInvariants(invariants, new Variable(l,true)))
					// the variable is added to the list only if its value is consistent
					var.add(new Variable(l,true));
			}
			
			// add the list of variables related to the current label to the main list
			vars.add(var);
			
			// delete all the invariants that are only composed by the current label 
			deleteSingleVarInvariant(invariants, l);

		}
	
		// all the remaining invariants are merged in a single one using AND
		Invariant mrgd = Invariant.merge(invariants);
		this.invariant.setCondition(mrgd.getCondition());

		return vars;
	}
	
	/**
	 * Removes from the HashSet all the invariants that are only  
	 * composed by the input label
	 * 
	 * @param HashSet<Invariant>
	 * @param Label
	 */
	private void deleteSingleVarInvariant(HashSet<Invariant> invariants, Label l)
	{
		// iterate over all the invariants
		for (Iterator<Invariant> iter = invariants.iterator(); iter.hasNext(); )
		{
			Invariant i = iter.next();

			// if the invariant has only one label 
			if(i.getCondition().getLabels().size() == 1)
				// if the label corresponds to the input one
	    		if(i.getCondition().getLabels().contains(l))
	    			// the invariant is removed
	    			iter.remove();
		}
	}

	/**
	 * Check all the invariants with only one label to avoid getting inconsistent variables
	 * 
	 * @param HashSet<Invariant>
	 * @param Variable
	 * @return
	 * @throws AutomatonException 
	 */
	private boolean checkSingleVarInvariants(HashSet<Invariant> invariants, Variable v) throws AutomatonException
	{

		Expression expr = null;
		String result = "";		
		
		// iterates all the invariants
		for (Iterator<Invariant> iter = invariants.iterator(); iter.hasNext(); )
		{
			Invariant i = iter.next();
			expr = i.getCondition();
			
			// if the invariant has only one label     	
	    	if(i.getCondition().getLabels().size() == 1)
	    		// if the label correspond to the passed label
	    		if(i.getCondition().getLabels().contains(v.getLabel()))
	    		{
	    			// adds the declaration of the variable to the input string
	    			String condition = v.getName() + "=" + v.getValue() + ";";
	    			//eg: "A>0" ---> "A=1;A>10"
	    		 
	    			condition += expr.toString();
	    	    	
	    	    	// evaluate the expression using the JavaScript engine
	        		result = AutomatonUtils.evaluateExpression(condition);
	    		        			
	        		// if the result is false, the variable must not be considered
	        		if(result.equals("false"))
	        			return false;    	    
	    		}
		}
		
		// if the results are true, the variable can be added to the list 
		boolean r = (result.equals("false")) ? false : true; 
		return r;
		
	}

	/**
	 * Check all the invariants with no labels and delete them
	 * 
	 * @param HashSet<Invariant>
	 * @return boolean
	 * @throws AutomatonException 
	 */
	private boolean checkConstInvariants(HashSet<Invariant> invariants) throws AutomatonException
	{
	
		Expression expr = null;
		String condition = "";
		
		// iterates all the invariants
		for (Iterator<Invariant> iter = invariants.iterator(); iter.hasNext(); )
		{
			Invariant i = iter.next();
	    	String result = "";		

	    	// if the current condition has no labels
			if(i.getCondition().getLabels().isEmpty())
			{        	
				// get the expression from the invariant
				expr = i.getCondition();
				
		    	condition = expr.toString();
		    	
		    	result = AutomatonUtils.evaluateExpression(condition);
        		
        		// if one of them is false an exception is thrown, since the StateTable will be empty
        		if(result.equals("false"))
        			throw new AutomatonException("The invariant " + i.toString() + " is always FALSE");      	
        		
        		// if it's true, it will be removed
        		else
        			iter.remove();
			}
			
		}
		
		return true;
	}

	
	/**
	 * Returns the State corresponding to the ID received in input
	 * 
	 * @param BigInteger id
	 * @return State s
	 * @throws AutomatonException 
	 */
	public State getStateFromID(BigInteger id) throws AutomatonException
	{
		// check the validity of the id given in input
		if( id.compareTo(this.size) >= 0 ||	id.compareTo(new BigInteger(new Integer(0).toString())) == -1 )
			// if it's out of range, an exception is thrown
			throw new AutomatonException("Index " + id + " out of range, it must be a value between 0 and " + (this.size().subtract(new BigInteger("1"))));
		
		HashSet<Variable> v = new HashSet<Variable>();
		
		// d is the number of combinations of the next variables values
		BigInteger d = this.size;
		
		// iterate all the ArrayList of Variables
		for(ArrayList<Variable> al : this.vars)
		{
			// update d removing the combination of the current label
			d = d.divide(new BigInteger( new Integer(al.size()).toString()));
			
			// get the value using the index calculated with the following formula
								
			v.add(al.get( id.divide(d).mod(new BigInteger( new Integer(al.size()).toString())).intValue() ));
		}
		return new State(v);
	}
	
	/**
	 * Returns the ID corresponding to the State received in input
	 * 
	 * @param State s
	 * @return BigInteger id
	 * @throws AutomatonException 
	 */
	public BigInteger getIDFromState(State s) throws AutomatonException
	{
		
		BigInteger result = new BigInteger("0");
		
		// p is the number of combinations of the next variables values
		BigInteger p = this.size;
		
		// iterate all the lists of variables
		for(ArrayList<Variable> al : this.vars)
		{
			// update p removing the combination of the current label
			p = p.divide(new BigInteger( new Integer(al.size()).toString()));
			
			// loop over all the variable of the current state
			for(Variable v : s.getVariables())
			{
				// when the variable of the current list correspond to the one of the state
				if(al.get(0).getName().equals(v.getName()))
				{
					// check if the variable is present on the list
					if(al.indexOf(v) != -1)
						
						// the index is the sum of p times the index of the current variable
						result = result.add(p.multiply(new BigInteger( new Integer(al.indexOf(v)).toString())));
								
					
					// if the variable is not present, an exception is thrown
					else
						throw new AutomatonException("The state you want to fetch is inconsistent:\n	" + s.toString());

					break;
				}
				else continue;	
			}
		}
		return result;
	}
	
	
	@Override
	public StatesTable clone()
	{
		ArrayList<ArrayList<Variable>> tmpVV = new ArrayList<ArrayList<Variable>>();
		for(ArrayList<Variable> a : this.getVars())
		{
			ArrayList<Variable> tmpV = new ArrayList<Variable>();
			for(Variable v : a)
				tmpV.add(v.clone());
			tmpVV.add(tmpV);
		}
		
		StatesTable newS = null;
		try 
		{
			newS = new StatesTable(tmpVV);
		} 
		catch (AutomatonException e) 
		{ e.printStackTrace(); }
		
		newS.setInvariant(this.getInvariant().clone());
		
		return newS;
	}
	
	public BigInteger size()
	{
		return size;
	}

	public ArrayList<ArrayList<Variable>> getVars() 
	{
		return vars;
	}

	public Invariant getInvariant() 
	{
		return invariant;
	}

	public void setInvariant(Invariant invariant) 
	{
		this.invariant = invariant;
	}
	
	public void setVars(ArrayList<ArrayList<Variable>> vars) 
	{
		this.vars = vars;
		
		// get the length of the table
		this.size = new BigInteger(new Integer(1).toString());
		for(ArrayList<Variable> al : this.vars)
			this.size = this.size.multiply(new BigInteger( new Integer(al.size()).toString()) );
	
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invariant == null) ? 0 : invariant.hashCode());
		result = prime * result + ((size == null) ? 0 : size.hashCode());
		result = prime * result + ((vars == null) ? 0 : vars.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
	
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		StatesTable other = (StatesTable) obj;
		if (invariant == null) 
		{
			if (other.invariant != null)
				return false;
		}
		else if (!invariant.equals(other.invariant))
			return false;
		if (size == null) 
		{
			if (other.size != null)
				return false;
		}
		else if (!size.equals(other.size))
			return false;
		if (vars == null) 
		{
			if (other.vars != null)
				return false;
		}
		else if (!vars.equals(other.vars))
			return false;
		return true;
	}

	@Override
	public String toString() 
	{
		String st = "";
		
		try
		{
			for(BigInteger i = new BigInteger("0"); i.compareTo(this.size())==-1; i=i.add(new BigInteger("1")))
			{
				State s = this.getStateFromID(i);
				st = st + "[" + i + "]" + s + "\n";
			}
		}
		catch( AutomatonException e){ e.printStackTrace(); }
		
		return st;
	}


	

}
