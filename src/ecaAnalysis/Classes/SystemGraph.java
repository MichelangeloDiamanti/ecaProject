package ecaAnalysis.Classes;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class SystemGraph 
{

	private HashMap<Label, HashSet<Label>> adjacencyList = new HashMap<Label, HashSet<Label>>(); 

	public SystemGraph(ECAfile ecafile) throws AutomatonException 
	{
		super();
		
		ECAfile ef = ecafile.clone();
		ef.setInvariantsInCNF();
		
		// iterate over all the labels in the system
		for(Label l : ef.getLabels())
		{
			HashSet<Label> lbls = new HashSet<Label>();
			
			// if the label appears in a rule, the all the labels of that rule are added
			for(ECArule r : ef.getRules())
				if(r.getLabels().contains(l))
					lbls.addAll(r.getLabels());
			
			// if the label appears in an invariant with other labels, the all the labels of that invariant are added
			for(Invariant i : ef.getInvariants())
				if(i.getCondition().getLabels().size() > 1 && i.getCondition().getLabels().contains(l))
					lbls.addAll(i.getCondition().getLabels());
					
			// the label itself is removed to avoid selfcycles
			lbls.remove(l);
			
			// the label is out in the list with all the other label of the rules it appears in
			adjacencyList.put(l, lbls);
		}		
	}
	
	public SystemGraph(HashMap<Label, HashSet<Label>> adjacencylist) 
	{
		super();
		
		HashMap<Label, HashSet<Label>> al = new HashMap<Label, HashSet<Label>>();
		for(Label lk : adjacencylist.keySet())
		{
			HashSet<Label> hl = new HashSet<Label>();
			for(Label ll : adjacencylist.get(lk))
				hl.add(ll.clone());
			
			al.put(lk.clone(), hl);
		}	
		
		this.adjacencyList = al;
	}

	public HashSet<ECAfile> getSubSystems(ECAfile ecafile) throws AutomatonException
	{
		// the set of subsystems to return
		HashSet<ECAfile> subSystems = new HashSet<ECAfile>();
		// the map that check if a node was visited or not 
		HashMap<Label, Boolean> visited = new HashMap<Label, Boolean>();
		// the queue that stores all the nodes 
		Queue<Label> q = new LinkedList<Label>();
		// clone of the passed ECAfile
		ECAfile tmpECAfile = ecafile.clone();
		tmpECAfile.setInvariantsInCNF();
		
		// set all the visited flags to false
		for(Label l : this.adjacencyList.keySet())
			visited.put(l, false);

		// iterate over all nodes
		while(visited.containsValue(false))
		{
			// sets and lists to create a new ECAfile
			HashSet<Label> subL = new HashSet<Label>(); 
			HashSet<Invariant> subI = new HashSet<Invariant>();
			HashSet<ECArule> subR = new HashSet<ECArule>();
			
			// set a root (the first element on the map that had not been visited)
			Label root = null;
			for(Label l : visited.keySet())
				if(visited.get(l)==false)
					root = l; 
			
			visited.put(root, true);
			q.add(root);      

			while(!q.isEmpty())
			{
				Label current = q.remove();
				
				// fill the attributes of the new subsystem
				subL.add(current); //labels
				for(Invariant si : tmpECAfile.getInvariants()) //invariants
					if(si.getCondition().getLabels().contains(current) && !subI.contains(si))
						subI.add(si);
				for(ECArule sr : tmpECAfile.getRules()) //rules
					if(sr.getLabels().contains(current) && !subR.contains(sr))
						subR.add(sr);
					
								
				for(Label l : this.adjacencyList.get(current))
				{
					if(visited.get(l)==false)
					{
						visited.put(l, true);
				        q.add(l);
					}
				}
			}
			subSystems.add(new ECAfile(subL,subI,subR));
		}	
		
		return subSystems;
	}
	
 
	public HashMap<Label, HashSet<Label>> getAdjacencyList() 
	{
		return adjacencyList;
	}

	@Override
	public String toString() 
	{
		String result = "";
		for(Label lk : this.adjacencyList.keySet())
		{
			result = result + "[" + lk.getName() + "] -> [";
			
			if(!this.adjacencyList.get(lk).isEmpty())
			{
				for(Label ll : this.adjacencyList.get(lk))
					result = result + ll.getName() + ",";
			
				result = result.substring(0, result.length()-1) + "]\n";
			}
			else
				result += "]\n";
				
		}
			
		return result;
	}

	
	
	
}