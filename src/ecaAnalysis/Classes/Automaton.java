package ecaAnalysis.Classes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.ListIterator;
import java.util.Stack;
import java.util.concurrent.ConcurrentHashMap;

import GUI.Classes.GUIutils;
import ecaAnalysis.Classes.AutomatonCore.Edge;
import ecaAnalysis.Classes.AutomatonCore.StopSimulationException;
import ecaAnalysis.Classes.AutomatonCore.Vertex;
import ecaAnalysis.Classes.Enumerations.Confluency;
import ecaAnalysis.Classes.Enumerations.Stability;
import ecaAnalysis.Classes.Enumerations.VisitColor;


public class Automaton 
{
	private ECAfile ecafile = null; 
	private StatesTable statestable = null;
	private Vertex root = null;

	private ConcurrentHashMap<BigInteger, Vertex> vertices = new ConcurrentHashMap<BigInteger, Vertex>();
	private ConcurrentHashMap<String, Edge> edges = new ConcurrentHashMap<String, Edge>();
	private ConcurrentHashMap<HashSet<Variable>, Cluster> clusters = new ConcurrentHashMap<HashSet<Variable>, Cluster>();

	private AutomatonCore core = null;
	
	/**
	 * Constructs and initializes an Automaton according to the parameters
	 * specified in the Ecafile.
	 *  
	 * @param ecafile
	 * @throws AutomatonException 
	 */
	public Automaton(ECAfile ecafile) throws AutomatonException 
	{
		super();

		this.ecafile = ecafile.clone();
		core = new AutomatonCore(this.ecafile);
		this.statestable = AutomatonCore.getStatestable();
	}
	
	public HashSet<Variable> getInitialConfig()
	{
		HashSet<Variable> ic = null;
		try 
		{
			ic = core.getInitialConfig();
		} 
		catch (StopSimulationException | AutomatonException e){}
		
		
		return ic;
	}
	
	public void startSequentialSimulation(HashSet<Variable> initialConfig) throws AutomatonException
	{
		try
		{			
			long s = System.nanoTime();
			core.startSequentialSimulation(initialConfig);
			long e = System.nanoTime();
			System.out.println((e-s)/1000000 + "ms");
			core.cloneResult(this);			
		}
		catch(StopSimulationException e){}
		finally
		{
			AutomatonCore.clear();			
		}
	}
	
	public void startParallelSimulation(HashSet<Variable> initialConfig) throws AutomatonException
	{		
		try 
		{
			core.startParallelSimulation(initialConfig);
			core.cloneResult(this);
		} 
		catch (StopSimulationException e1){}
		finally
		{
			AutomatonCore.clear();		
		}
	}	
	
	/** 
	 * finds all the paths between two given vertices
	 * 
	 * @param source node as a Vertex
	 * @param target node as a Vertex
	 * @return HashSet<ArrayList<Edge>> containing all the paths
	 * @throws AutomatonException 
	 */
	public HashSet<ArrayList<Edge>> findPaths(Vertex source, Vertex target) throws AutomatonException
	{
		if(root == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");
		
		HashSet<ArrayList<Edge>> paths = new HashSet<ArrayList<Edge>>();
		
		Stack<Edge> visitedEdgesStack = new Stack<Edge>();
		
		if(source.equals(target) == false)
			paths = computePaths(source, target, visitedEdgesStack);
		else
		{
			for(Edge s : source.getOutgoingEdges())
			{
				if(s.getTo().equals(source))
				{
					ArrayList<Edge> self = new ArrayList<Edge>();
					self.add(s);
					paths.add(self);
				}
			}
		}
		
		return paths;
	}
	
	/**
	 * finds all the cycles (non-elementary) in the graph
	 * 
	 * @return HashSet<ArrayList<Edge>>
	 * @throws AutomatonException 
	 */
	public HashSet<ArrayList<Edge>> findCycles() throws AutomatonException
	{
		if(root == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");

		// contains the edges that form the cycles
		HashSet<ArrayList<Edge>> cyclesEdges = new HashSet<ArrayList<Edge>>();
		
		// hashset of edges that cause an elementary cycle
		HashSet<Edge> conjunctions = new HashSet<Edge>();
				
		// to get the conjunctive edges we must perfrm a color DFS, so we start
		// setting all the vertices to white
		for (BigInteger v : this.vertices.keySet()) 
			this.vertices.get(v).setVisitState(VisitColor.WHITE);	
	
		// then we visit the graph recurively iterating over all the non-visited vertices
		for (BigInteger v : this.vertices.keySet())
			if(this.vertices.get(v).getVisitState() == VisitColor.WHITE)
				coloredDFS(this.vertices.get(v), conjunctions);

		// we get all the possible paths that lead from the starting point of the cycle to its end
		for(Edge c : conjunctions)
		{
			HashSet<ArrayList<Edge>> paths = findPaths(c.getTo(), c.getFrom());
			for(ArrayList<Edge> path : paths)
			{
				if(c.triggers(path.get(0).getRule())) // if the conjunctive edge actually triggers a cycle
				{
					if(c.getFrom().equals(c.getTo()) == false) // avoid adding self cycles twice
						path.add(c);
					cyclesEdges.add(path);	
				}
			}
		}

		return cyclesEdges;
		
	}

	public Confluency checkConfluency() throws AutomatonException
	{
		if(root == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");

		GUIutils.setPercentageProportion(this.vertices.size() * 2 + 1);
		int vCount = 1;
		
		Confluency res = Confluency.CONFLUENT;
		
		// if there is at least one incorrect state returns an error
		if( this.vertices.get(new BigInteger("-1")) != null )
			res = Confluency.INCORRECT;
		
		vCount++;
		GUIutils.updatePercentage(vCount);
				
		if(res == Confluency.CONFLUENT)
		{
			// hashset of edges that cause an elementary cycle
			HashSet<Edge> conjunctions = new HashSet<Edge>();
			
			
			// to get the conjunctive edges we must perfrm a color DFS, so we start
			// setting all the vertices to white
			for (BigInteger v : this.vertices.keySet())
				this.vertices.get(v).setVisitState(VisitColor.WHITE);	
		
			// then we visit the graph recurively iterating over all the non-visited vertices
			for (BigInteger v : this.vertices.keySet())
			{
				if(this.vertices.get(v).getVisitState() == VisitColor.WHITE)
					coloredDFS(this.vertices.get(v), conjunctions);
				// if we find at least one cycle
				if (!conjunctions.isEmpty())
				{	
					res = Confluency.CYCLIC;
					break;
				}
				vCount++;
				GUIutils.updatePercentage(vCount);
			}
			
			// if there are not cycles nor incorrect states, we check for non confluency
			if (res == Confluency.CONFLUENT) 
			{
				HashSet<Vertex> roots = new HashSet<Vertex>();
				HashSet<Vertex> leaves = new HashSet<Vertex>();
				
				for (Edge o : this.getRoot().getOutgoingEdges())
					roots.add(o.getTo());
				
				// check every stable vertex until it reaches the leaves
				while(!roots.isEmpty() && res != Confluency.NON_CONFLUENT)
				{
					for (Vertex r : roots) 
					{
						HashSet<Vertex> tmpLeaves = new HashSet<Vertex>();
						tmpLeaves = r.getNearestLeaves(tmpLeaves);
						leaves.addAll(tmpLeaves);
						
						// if it is not confluent
						if(tmpLeaves.size() > 1)
						{
							res = Confluency.NON_CONFLUENT;
							break;
						}						
					}
			
					// removes the roots and add the new ones (current leaves)
					roots.clear();
					for (Vertex l : leaves) 
						for (Edge o : l.getOutgoingEdges())
							roots.add(o.getTo());
					leaves.clear();		
					
					GUIutils.updatePercentage(vCount);
					vCount++;
				}
			}
		}
		return res;
	}
	
	/**
	 * Counts how many times each rules has been triggered
	 * @return HashMap containing the count for each rule
	 * @throws AutomatonException 
	 */
	public HashMap<ECArule, BigInteger> countRulesOccurrences() throws AutomatonException
	{		
		checkSimulationCorrectness(false);
		HashMap<ECArule, BigInteger> count = new HashMap<ECArule, BigInteger>();

		GUIutils.setPercentageProportion(this.ecafile.getRules().size());
		
		// initializes the map to 0 for each ecarule in the file
		for (ECArule r : this.ecafile.getRules()) 
			count.put(r, new BigInteger("0"));

		for(String e : this.edges.keySet())
		{
			if(this.edges.get(e).getRule() != null)
			{
				ECArule r = this.edges.get(e).getRule();
				BigInteger c = count.get(r);				
				c = c.add(new BigInteger("1"));
				
				count.put(r,c);
			}
		}
		
		HashSet<ArrayList<Edge>> cycles = findCycles();
		for(ArrayList<Edge> ale : cycles)
		{
			for(Edge e : ale)
			{
				ECArule r = e.getRule();
				count.put(r, new BigInteger("-1")); //infinite
			}
		}
		
		
		return count;
	}
	
	/**
	 * Counts all the instances of rules triggered by nature
	 * 
	 * @return HashMap<ECArule, BigInteger> the rules triggered by nature
	 * @throws AutomatonException 
	 */
	public HashMap<ECArule, BigInteger> countStartingRules() throws AutomatonException
	{
		checkSimulationCorrectness(false);
		
		HashMap<ECArule, BigInteger> count = new HashMap<ECArule, BigInteger>();
		
		GUIutils.setPercentageProportion(this.edges.size());
		int eCount = 0;
		for (String e : this.edges.keySet()) 
		{
			// if the edge comes from a stable vertex
			if (edges.get(e).getFrom().getStability() == Stability.STABLE) 
			{
				// count the rules triggered by nature
				for (Edge oe : edges.get(e).getTo().getOutgoingEdges())
				{
					if(edges.get(e).triggers(oe.getRule()))
					{
						// if the rule is already in the map increments the count by 1
						if (count.containsKey(oe.getRule()))
							count.put(oe.getRule(), count.get(oe.getRule()).add(new BigInteger("1")));
						// if the rule is not in the map sets the count to 1
						else
							count.put(oe.getRule(), new BigInteger("1"));
					}
				}
			}
			eCount++;
			GUIutils.updatePercentage(eCount);
		}
		
		return count;
	}
	
	/**
	 * Computes the most activated rules
	 * @return HashMap containing the most triggered rules and the count
	 * @throws AutomatonException 
	 */
	public HashMap<ECArule, BigInteger> findMostUsedRules() throws AutomatonException
	{
		checkSimulationCorrectness(false);
		
		// map containing how many times every rule has been triggered
		HashMap<ECArule, BigInteger> rc = countRulesOccurrences();
		// map containing the most used rules and their count
		HashMap<ECArule, BigInteger> mur = new HashMap<ECArule, BigInteger>();
		
		GUIutils.setPercentageProportion(rc.size() * 2);
		int eCount = 0;
			
		BigInteger max = new BigInteger("0");
		
		// computes the max triggered rules
		for (ECArule e : rc.keySet())
		{
			if(rc.get(e).compareTo(new BigInteger("-1")) == 0)
			{
				max = new BigInteger("-1"); //infinity;
				break;
			}
			
			if (rc.get(e).compareTo(max) == 1) 
				max = rc.get(e);
			
			eCount++;
			GUIutils.updatePercentage(eCount);
		}
		
		// put the max triggered rules in the map 
		for (ECArule e : rc.keySet()) 
		{
			if (rc.get(e).compareTo(max) == 0) 
				mur.put(e, max);
			
			eCount++;
			GUIutils.updatePercentage(eCount);
		}
			
		return mur;
	}
	
	/**
	 * Computes the unused rules (among the ones simulated)
	 * @return HashSet containing the unused rules
	 * @throws AutomatonException 
	 */
	public HashSet<ECArule> findUnusedRules() throws AutomatonException
	{
		checkSimulationCorrectness(false);
		
		// map containing how many times every rule has been triggered
		HashMap<ECArule, BigInteger> rc = countRulesOccurrences();
		// hashset containig the unused rules
		HashSet<ECArule> unused = new HashSet<ECArule>();
		
		GUIutils.setPercentageProportion(rc.size());
		int rCount = 0;
		// computes the unused rules
		for (ECArule e : rc.keySet()) 
		{
			if (rc.get(e).compareTo(new BigInteger("0")) == 0) 
				unused.add(e);
			GUIutils.updatePercentage(rCount);
			rCount++;
		}
		return unused;
	}
	
	/**
	 * Counts how many times each actuator has been updated
	 * @return HashMap containing the actuators and the count
	 * @throws AutomatonException 
	 */
	public HashMap<Label, BigInteger> countActuatorsUpdates() throws AutomatonException
	{
		checkSimulationCorrectness(false);
		// map containing how many times every rule has been triggered
		HashMap<ECArule, BigInteger> rc = countRulesOccurrences();

		// map containing how many times every actuator has been updated 
		HashMap<Label, BigInteger> ac = new HashMap<Label, BigInteger>();
		
		// initializes the map to 0 for each actuator in the file
		for (Label l : ecafile.getLabels()) 
			if(l.isActuator())
				ac.put(l, new BigInteger("0"));

		GUIutils.setPercentageProportion(rc.size());		
		// for each rule we must get the actions
		int rCount = 0;
		for(ECArule r : rc.keySet())
		{
			// if the rule was triggered infinite times
			if(rc.get(r).compareTo(new BigInteger("-1")) == 0)
			{
				// for each action we get the updating actuator
				for(Action a : r.getActions())
				{
					// we increment the actuator by the number of times the rule has been triggered
					if(a.getLabel().isActuator())
						ac.put(a.getLabel(), new BigInteger("-1"));
				}	
			}
			else
			{
				// for each action we get the updating actuator
				for(Action a : r.getActions())
				{
					// we increment the actuator by the number of times the rule has been triggered
					if(a.getLabel().isActuator())
					{
						// we update the count only if it is not infinite
						if(ac.get(a.getLabel()).compareTo(new BigInteger("-1")) != 0)
							ac.put(a.getLabel(), ac.get(a.getLabel()).add(rc.get(r)));
					}
						
				}	
			}
			rCount++;
			GUIutils.updatePercentage(rCount);
			
		}
		return ac;
	}

	
	/**
	 * Computes the most activated rules
	 * @return HashMap containing the most triggered rules and the count
	 * @throws AutomatonException 
	 */
	public HashMap<Label, BigInteger> findMostUpdatedActuators() throws AutomatonException
	{
		checkSimulationCorrectness(false);
		
		// map containing how many times every rule has been triggered
		HashMap<Label, BigInteger> ac = countActuatorsUpdates();
		// map containing the most used rules and their count
		HashMap<Label, BigInteger> mua = new HashMap<Label, BigInteger>();
		
		GUIutils.setPercentageProportion(ac.size() * 2);
		int lCount = 0;
		BigInteger max = new BigInteger("0");
		
		// computes the max triggered rules
		for (Label l : ac.keySet()) 
		{
			if(ac.get(l).compareTo(new BigInteger("-1")) == 0)
			{
				max = new BigInteger("-1"); //infinity;
				break;
			}
			
			if (ac.get(l).compareTo(max) == 1) 
				max = ac.get(l);
		
			lCount++;
			GUIutils.updatePercentage(lCount);
		}
		
		// put the max triggered rules in the map 
		for (Label l : ac.keySet()) 
		{
			if (ac.get(l).compareTo(max) == 0) 
				mua.put(l, max);
		
			lCount++;
			GUIutils.updatePercentage(lCount);
		}
		
		return mua;
	}
	
	public boolean isDeterministic() throws AutomatonException
	{
		checkSimulationCorrectness(true);
		
		boolean d = true;
		
		GUIutils.setPercentageProportion(this.vertices.size());
		int vCount = 0;
		for (BigInteger v : this.vertices.keySet())
		{
			if(this.vertices.get(v).getOutgoingEdges().size() > 1)
			{
				d = false;
				break;
			}
			vCount++;
			GUIutils.updatePercentage(vCount);
			
		}
		return d;
	}
	
	public HashSet<ECArule> findIncorrectRules() throws AutomatonException
	{
		if(root == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");
	
		HashSet<ECArule> incorrects = new HashSet<ECArule>();
		
		GUIutils.setPercentageProportion(this.vertices.size());
		
		int vCount = 0;
		for(BigInteger v : this.vertices.keySet())
		{
			if(this.vertices.get(v).getStability() == Stability.INCORRECT)
			{
				for(Edge e : this.vertices.get(v).getIncomingEdges())
					incorrects.add(e.getRule());
			}
			vCount++;
			GUIutils.updatePercentage(vCount);
		}
	
		incorrects.removeAll(findUnusedRules());
		return incorrects;
	}
		
	/**
	 * explores the adjacency list to find all the possible paths between two vertices
	 * 
	 * @param adj adjacency list
	 * @param start vertex
	 * @param end vertex
	 * @param visitedNodesStack 
	 * @return
	 * @throws AutomatonException 
	 */
	private HashSet<ArrayList<Edge>> computePaths(Vertex start, Vertex end, Stack<Edge> visitedEdgesStack) throws AutomatonException
	{
		// contains all the possible paths between the two vertices
		HashSet<ArrayList<Edge>> paths = new HashSet<ArrayList<Edge>>();
		
		// base case, if we reached the target node
		if(start.equals(end))
		{
			// fill the list with the vertices that compose the path
			ArrayList<Edge> path = new ArrayList<Edge>();
			
			ListIterator<Edge> itr = visitedEdgesStack.listIterator();
			while(itr.hasNext())
				path.add(itr.next());	
			
			paths.add(path);
			return paths;
		}
				
		if(start.getOutgoingEdges() != null)
		{
			for(Edge o : start.getOutgoingEdges())
			{
				
				// avoid following infinite loops
				if(!visitedEdgesStack.contains(o))
				{
					boolean triggered = true;
					
					if(o.getRule() != null && !visitedEdgesStack.isEmpty())
						triggered = visitedEdgesStack.peek().triggers(o.getRule());
 					
					if(triggered)
					{
						// keep track of the visited vertices with the stack
						visitedEdgesStack.push(o);
						
						paths.addAll(computePaths(o.getTo(), end, visitedEdgesStack));
						
						visitedEdgesStack.pop();	
						
					}
				}				
			}
		}	

		return paths;
	}
	
	private void coloredDFS(Vertex v, HashSet<Edge> conjunctions)
	{
		v.setVisitState(VisitColor.GRAY);
		
		for (Edge e : v.getOutgoingEdges()) 
		{
			if (e.getTo().getVisitState() == VisitColor.GRAY)
				conjunctions.add(e);
			else if (e.getTo().getVisitState() == VisitColor.WHITE) 
				coloredDFS(e.getTo(), conjunctions);
		}
		v.setVisitState(VisitColor.BLACK);
	}
		
		
	private void checkSimulationCorrectness(boolean extendedCheck) throws AutomatonException 
	{
		if(root == null)
			throw new AutomatonException("Cannot apply this method to an EMPTY automaton.\nThe simulation must be performed first");
		
		if(extendedCheck)
		{
			// if there is at least one incorrect state returns an error
			if(this.vertices.get(new BigInteger("-1")) != null)
				throw new AutomatonException("Cannot apply this method to an INCORRECT automaton.\nThe automaton cannot contain incorrect states");
			
			HashSet<Edge> conjunctions = new HashSet<Edge>();
		
			// to get the conjunctive edges we must perfrm a color DFS, so we start
			// setting all the vertices to white
			for (BigInteger v : this.vertices.keySet()) 
				this.vertices.get(v).setVisitState(VisitColor.WHITE);	
			
			// then we visit the graph recurively iterating over all the non-visited vertices
			for (BigInteger v : this.vertices.keySet()) 
			{
				if(this.vertices.get(v).getVisitState() == VisitColor.WHITE)
					coloredDFS(this.vertices.get(v), conjunctions);
				// if we find at least one cycle
				if (!conjunctions.isEmpty())
					throw new AutomatonException("Cannot apply this method to a CYCLIC automaton.\nThe automaton cannot contain cycles");
			}
		}
	}
	
	public ECAfile getEcafile() 
	{
		return ecafile;
	}

	public StatesTable getStatestable() 
	{
		return statestable;
	}

	public Vertex getRoot()
	{
		return this.root;
	}
	
	public void setRoot(Vertex r)
	{
		this.root = r;
	}
	
	public ConcurrentHashMap<BigInteger, Vertex> getVertices() 
	{
		return vertices;
	}
	
	public void addVertex(Vertex v)
	{
		this.vertices.put(v.getState(), v);
	}

	public ConcurrentHashMap<String, Edge> getEdges() 
	{
		return edges;
	}
	
	public void addEdge(Edge e)
	{
		this.edges.put(e.identifier(), e);
	}
	
	public ConcurrentHashMap<HashSet<Variable>, Cluster> getClusters() 
	{
		return clusters;
	}	
	
	public void addCluster(Cluster c)
	{
		this.clusters.put(c.getClusterID(), c);
	}
		
	@Override
	public Automaton clone()
	{
		Automaton newA = null;
	
		try 
		{
			newA = new Automaton(ecafile.clone());
			for(BigInteger v : this.vertices.keySet())
			{
				
				Vertex newV = this.core.new Vertex(v);;
				newV.setVisitState(this.vertices.get(v).getVisitState());
				newV.setStability(this.vertices.get(v).getStability());
				newA.addVertex(newV);
				if(v.equals(this.root.getState()))
					newA.setRoot(newV);
			}
		
			for(String e : this.edges.keySet())
			{
				BigInteger from = this.edges.get(e).getFrom().getState();
				BigInteger to = this.edges.get(e).getTo().getState();
				ECArule rule = this.edges.get(e).getRule();
				
				Edge newE = (rule == null) ? this.core.new Edge(newA.getVertices().get(from), newA.getVertices().get(to)) : this.core.new Edge(newA.getVertices().get(from), rule.clone(), newA.getVertices().get(to));
							
				newE.getFrom().addEdge(newE);
				newE.getTo().addEdge(newE);
								
				newA.addEdge(newE);
			}
				
		
			for(HashSet<Variable> c : this.clusters.keySet())
				newA.addCluster(this.clusters.get(c).clone());
		
			return newA;	
		} 
		catch (AutomatonException e1) 
		{
			e1.printStackTrace();
			return null;
		}
		
	}
	
	

}
