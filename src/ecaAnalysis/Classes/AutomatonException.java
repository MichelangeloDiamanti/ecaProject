package ecaAnalysis.Classes;

public class AutomatonException extends Exception
{
	private static final long serialVersionUID = 1L;

	public AutomatonException(String message)
	{
		super(message);
	}
}