package ecaAnalysis.Classes;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;

import com.microsoft.z3.BoolExpr;
import com.microsoft.z3.Context;
import com.microsoft.z3.Expr;
import com.microsoft.z3.Model;
import com.microsoft.z3.Solver;
import com.microsoft.z3.Status;

import GUI.GUI;
import GUI.Classes.ECAfileGUI;
import GUI.Classes.GUIutils;
import ecaAnalysis.Classes.Enumerations.Kind;
import ecaAnalysis.Classes.Enumerations.Operator;
import ecaAnalysis.Classes.Enumerations.Type;


public class ECAfile 
{	
	
	private HashSet<Label> labels = new HashSet<Label>(); 
	private HashSet<Invariant> invariants = new HashSet<Invariant>();
	private HashSet<ECArule> rules = new HashSet<ECArule>();
	
	
	public ECAfile(HashSet<Label> labels, HashSet<Invariant> invariants, HashSet<ECArule> rules) 
	{
		super();
		
		HashSet<Label> ls = new HashSet<Label>();
		for(Label l : labels)
			ls.add(l);
		this.labels = ls;
		
		HashSet<Invariant> is = new HashSet<Invariant>();
		for(Invariant i : invariants)
			is.add(i);
		this.invariants = is;
		
		HashSet<ECArule> es = new HashSet<ECArule>();
		for(ECArule r : rules)
			es.add(r);
		this.rules = es;
		
	}

	@Override
	public ECAfile clone()
	{
		HashSet<Label> tmpL = new HashSet<Label>();
		for(Label l : this.getLabels())
			tmpL.add(l.clone());
		
		HashSet<Invariant> tmpI = new HashSet<Invariant>();
		for(Invariant i : this.getInvariants())
			tmpI.add(i.clone());
		
		HashSet<ECArule> tmpR = new HashSet<ECArule>();
		for(ECArule r : this.getRules())
			tmpR.add(r.clone());
		
		ECAfile newE = new ECAfile(tmpL, tmpI, tmpR);
		return newE;
	}
	
	public HashSet<Label> getLabels() 
	{
		return labels;
	}
	
	public HashSet<Label> getSensors() 
	{
		HashSet<Label> sensors = new HashSet<Label>();
		for(Label l : this.labels)
			if(l.isSensor())
				sensors.add(l);
		
		return sensors;
	}
	
	public HashSet<Label> getActuators() 
	{
		HashSet<Label> actuators = new HashSet<Label>();
		for(Label l : this.labels)
			if(l.isActuator())
				actuators.add(l);
		
		return actuators;
	}
	
	public void setLabels(HashSet<Label> labels) 
	{
		this.labels = labels;
	}

	public HashSet<Invariant> getInvariants() 
	{
		return invariants;
	}

	public void setInvariants(HashSet<Invariant> invariants) 
	{
		this.invariants = invariants;
	}

	public void setInvariantsInCNF() throws AutomatonException 
	{
		HashSet<Expression> CNFs = new HashSet<Expression>();
		
		for(Invariant i : this.getInvariants())
			CNFs.addAll(i.getCondition().getExpressionsInCNF());
	
		this.invariants.clear();
		
		for(Expression expr : CNFs)
			this.invariants.add(new Invariant(expr));
	}
	
	public HashSet<ECArule> getRules() 
	{
		return rules;
	}

	public void setRules(HashSet<ECArule> rules) 
	{
		this.rules = rules;
	}

	/**
	 * check if the invariants merged together are SAT
	 * 
	 * @return true if they are SAT, false otherwise
	 * @throws AutomatonException 
	 */
	public boolean checkInvariantsSAT() throws AutomatonException
	{
		boolean sat = false;
		
		if(!this.invariants.isEmpty())
		{		
			// merge all the invariants expression in a single one
			Expression mergedInvs = Invariant.merge(this.invariants).getCondition();
	
			Model invsModel = mergedInvs.checkSatisfiability();
			
			if(invsModel != null)
				sat = true;
			else
				throw new AutomatonException("The invariants are UNSATISFIABLE");
		}
		else
			sat = true;
		
		return sat;
	}

	
	/**
	 * find all the rules that will never be used due to the invariants
	 * 
	 * @return a hashset of unused rules
	 * @throws AutomatonException 
	 */
	public HashSet<ECArule> findUnusableRules(boolean updateProgressBar) throws AutomatonException
	{
		HashSet<ECArule> unused = new HashSet<ECArule>(); 
		
		boolean invsSAT = this.checkInvariantsSAT();
		
        // if the invariants are consistent
        if ( invsSAT )
        {
        	if(updateProgressBar)
        		GUIutils.setPercentageProportion(this.rules.size());
        	
        	// get all the labels that cant change
        	HashSet<Label> unchangebles = findUnchangeableLabels();
        	
    		// merge all the invariants expression in a single one
    		Expression mergedInvs = Invariant.merge(this.invariants).getCondition();
    		
        	// A for loop over the rules: get the condition, make a conjunction with
			// invariants, check that it is satisfiable
    		int ruleCount = 0;
			for (ECArule r: this.rules) 
			{
				// merge the invariants with the current rule condition
				Expression condition =  Expression.merge(mergedInvs, r.getCondition());
	
				// check if the resulting condition is satisfiable	        	
				if(condition.checkSatisfiability() == null || unchangebles.containsAll(r.getGuards()))
					unused.add(r);
				
				if(updateProgressBar)
					GUIutils.updatePercentage(ruleCount);	
				ruleCount++;
			}
			
			HashSet<Label> unusedActuators = this.findUnusedActuators(unused);
			for (ECArule r: this.rules) 
			{
				if(!unused.contains(r)) // the rule can be used (for now)
				{
					boolean actFlag = true;
					for(Label g: r.getGuards())
					{
						if(!unusedActuators.contains(g))
						{
							actFlag = false;
							break;
						}
					}
					if(actFlag)
						unused.add(r);
				}
			}
			
        }

     
        return unused;
	}
	
	/**
	 * Iterates over every possible combination of rules in the ECAFile and checks if
	 * they're redundant one to eachother
	 * 
	 * @return HashMap containing each redundant rule and the rule(s) for which it's redundant
	 * @throws AutomatonException 
	 */
	public HashMap<ECArule, HashSet<ECArule>> findRedundantRules() throws AutomatonException 
	{
		// contains the redundant rules, the pair is Rule r1 is redundant w.r.t an hashset of rules
		HashMap<ECArule, HashSet<ECArule>> redRules = new HashMap<ECArule, HashSet<ECArule>>();
		boolean invsSAT = this.checkInvariantsSAT();
		
		// gets all the rules from the ecafile
		HashSet<ECArule> usedRules = this.getRules();
		// removes the ones that are not used
		usedRules.removeAll(this.findUnusableRules(false));
		usedRules.removeAll(this.findIncorrectRules(false));

		// if the invariants are consistent
        if ( invsSAT )
        {	
        	GUIutils.setPercentageProportion(this.rules.size() * (this.rules.size() -1));
        	
        	// merge all the invariants expression in a single one
    		Expression mergedInvs = Invariant.merge(this.invariants).getCondition();

    		// iterate over every possible pair of rules
			int rulesCount = 0;
    		for (ECArule r1: usedRules) 
			{
			    for (ECArule r2: usedRules) 
			    {
			    	// avoid checking for a rule with itself
			    	if ( !r1.equals(r2)) 
			    	{
			    		// check if the guards of the other rule contain 
			    		// all the guards of the current one
			    		if(r2.getGuards().containsAll(r1.getGuards()))
			    		{					
				            // Is r1 redundant w.r.t. r2?
				            // We need to see if there is a model in which r1 is true but r2 is not
				            // If this is the case, they are independent.
				            // Let's start from r1 => r2
			    			Expression r1_implies_r2 = new Expression(r1.getCondition(), Operator.IMP, r2.getCondition(), Type.BOOL);

				            // I assume redundant means: *for all* possible assignments to variables, if r1 is true then r2 is true
				            // If they were independent, there would be a model in which r1 is true and r2 is false.
				            // I translate this into the fact that the *negation* of r1_implies_r2 + invariants is SAT.
				            // So let's build the negation of r1 => r2               
				            r1_implies_r2 = new Expression(Operator.NOT, r1_implies_r2, Type.BOOL);    
				            
				            // Remember to add the invariants:
			    			r1_implies_r2 = Expression.merge(r1_implies_r2, mergedInvs);
				            // the rules are POTENTIALLY redundant if the implication is not TAUTOLOGY
				            if (r1_implies_r2.checkSatisfiability() == null) 
				            {
				            	// if checkActionRedundancy returns true, r1 is redundant w.r.t. r2
				            	if(checkActionRedundancy(r1,r2))
				            	{
				            		// add the new rule to the lit of redundant w.r.t. r1
				            		HashSet<ECArule> tmprr = new HashSet<ECArule>();
				            		if(redRules.get(r1) != null)
				            			tmprr.addAll(redRules.get(r1));
				            		tmprr.add(r2);
				            		redRules.put(r1, tmprr);
				            	}
							}
			    		}
			    	}
				    rulesCount++;			    	
			    	GUIutils.updatePercentage(rulesCount);
			    }
			}
        }
		return redRules;
	}
	
	private boolean checkActionRedundancy(ECArule r1, ECArule r2) throws AutomatonException
	{
		// contains the comparisons of the right part of all assignments and the invariants
		Expression mergedExpr = null;
		HashSet<Expression> allExpr = new HashSet<Expression>();
		
		// hashset of all the assignment labels in the first and second rule
		HashSet<Label> r1lbls = new HashSet<Label>();
		HashSet<Label> r2lbls = new HashSet<Label>();
		// hashset of all the label in play
		HashSet<Label> allbl = new HashSet<Label>();
		
		// fill the hashset of labels
		for(Action a : r1.getActions())
		{
			allbl.add(a.getLabel());
			allbl.addAll(a.getExpression().getLabels());
			r1lbls.add(a.getLabel());
		}
		for(Action a : r2.getActions())
		{
			allbl.add(a.getLabel());
			allbl.addAll(a.getExpression().getLabels());
			r2lbls.add(a.getLabel());
		}
		
		
		/* We need to compare each assignment in the first rule with its counterpart
		 * in the second one. For each label that occurs in the action we instantiate an expression
		 * relative to the assignment (if it does not occur the assignment is reflective A == A)
		 * 
		 * x = y + 2       these rules are redundant: we must compare y + 2 == y + 1 + 1
		 * x = y + 1 + 1   if they're equal for every possible value, the actions have the same semantics
		 */
		for(Label l : allbl)
		{
			Expression leftExpr = null;
			Expression rightExpr = null;
		
			// if the current label is assigned in the rule we get the expression
			if(r1lbls.contains(l))
				leftExpr = r1.getAction(l).getExpression();
			// if it does not occur we set x == x
			else
				leftExpr = new Expression(l, l.getType());
			
			if(r2lbls.contains(l))
				rightExpr = r2.getAction(l).getExpression();
			else
				rightExpr = new Expression(l, l.getType());
			
			
			// we compare the two right parts of the assignments
			Expression comparison = new Expression(leftExpr, Operator.EQ, rightExpr, Type.BOOL);
			allExpr.add(comparison);
		}
		
		/* we must check if for all possible values this expression in SAT, 
		 * which is the same as checking if the negative is NOT SAT
		 */
		mergedExpr = Expression.merge(allExpr);
		mergedExpr = new Expression(Operator.NOT, mergedExpr, Type.BOOL); // negation of the expression
		// we add the narrowest domain between the two rules (the one of the redundant rule)
		mergedExpr = Expression.merge(mergedExpr, r1.getCondition());
		// we add the invariants
		mergedExpr = Expression.merge(mergedExpr, Invariant.merge(this.invariants).getCondition());
		Model issat = mergedExpr.checkSatisfiability();
		return issat == null ? true : false;
	}
	
	
	/**
	 * iterate over all the rules and check if they are correct (they will never end up in a inconsistent state)
	 *   
	 * @return HashSet<ECArule> containing the incorrect rules
	 * @throws AutomatonException 
	 */
	public HashSet<ECArule> findIncorrectRules(boolean updateProgressBar) throws AutomatonException
	{
		// contains the incorrect rules
		HashSet<ECArule> incorrectRules = new HashSet<ECArule>();
		boolean invsSAT = this.checkInvariantsSAT();
	
		// gets all the rules from the ecafile
		HashSet<ECArule> usedRules = this.getRules();
	
        // if the invariants are consistent
        if ( invsSAT )
        {			
    		// removes the ones that are not used
    		usedRules.removeAll(this.findUnusableRules(false));
    		
        	if(updateProgressBar)
        		GUIutils.setPercentageProportion(this.rules.size());

        	int rulesCount = 0;
        	for(ECArule rule : usedRules)
        	{
        		// merge the invariants with the rule with and
        		Expression merged = Invariant.merge(this.getInvariants()).getCondition();        		
        		Expression wp = merged.clone();
        		
        		// merged must be in conjuction with the rule condition
        		merged = Expression.merge(rule.getCondition(), merged);
        		        		       		
        		// the weakest precondition (wp) must be updated with the new values of the actions
        		for(Action act : rule.getActions())
        			wp = wp.updateVar(act.getLabel(), act.getExpression());
        		
        		// we must check if merged implies wp is true for every value
        		// so we check if the negation is SAT 
        		Expression implies = new Expression(merged, Operator.IMP, wp, Type.BOOL);
        		implies = new Expression(Operator.NOT, implies, Type.BOOL);
      		
        		// if !(merged => wp) is SAT, the rule is incorrect
        		if(implies.checkSatisfiability() != null)
        			incorrectRules.add(rule);        
        		
        		if(updateProgressBar)
        			GUIutils.updatePercentage(rulesCount);
        		rulesCount++;
        	}
        }
        
		return incorrectRules;
	}
	
	/**
	 * check if exists a possible configuration that will lwad to a non deterministic 
	 * automaton, analysing all the rules in pair
	 * 
	 * @return boolean
	 * @throws AutomatonException 
	 */
	public boolean isDeterministic() throws AutomatonException
	{		
		boolean d = true;

		ECAfile tmpECAfile = this.clone();
		tmpECAfile.removeUnused();
	
		GUIutils.setPercentageProportion(tmpECAfile.rules.size() * (tmpECAfile.rules.size() -1));
		
		int rulesCount = 0;
		for(ECArule r1 : tmpECAfile.rules)
		{
			for(ECArule r2 : tmpECAfile.rules)
			{
				// don't check the rule with itself
				if(!r1.equals(r2))
				{
					// check if the guards of the rules have variables in common (non empty intersection)
					boolean eFlag = false;
					for(Label l1 : r1.getGuards())
					{
						if(r2.getGuards().contains(l1))
						{
							eFlag = true;
							break;
						}
					}
					
					// check if the conditions can be SAT with the invariants
					if(eFlag)
					{
						Expression cExpr = Invariant.merge(tmpECAfile.invariants).getCondition();
						cExpr = Expression.merge(cExpr, r1.getCondition());
						cExpr = Expression.merge(cExpr, r2.getCondition());
						
						// check if the rules differ by at least one action
						if(cExpr.checkSatisfiability() != null)
						{							
							if(!checkActionRedundancy(r1, r2))
							{
								d = false;
								break;
							}
						}
					}					
				}
				
				GUIutils.updatePercentage(rulesCount);
				rulesCount++;
			}
			if(!d) break;
		}
		
		return d;
	}
	
	
	/**
	 * removes all the unused rules, invariants and variables from the ecafile
	 * @throws AutomatonException 
	 */
	public void removeUnused() throws AutomatonException
	{
		// get all the rules and remove the ones unused
		this.rules.removeAll(this.findUnusableRules(false));
		
		// get all the useful labels from the remaining rules
		HashSet<Label> usedLabels = new HashSet<Label>();
		for(ECArule r : this.rules)
			usedLabels.addAll(r.getLabels());
		
		// check if there are invariants with only unused labels
		for(Iterator<Invariant> ii = this.getInvariants().iterator(); ii.hasNext();)
		{
			Invariant i = ii.next();
			boolean used = false;
			for(Label l : i.getCondition().getLabels())
				if(usedLabels.contains(l)) used = true;
			// if the invariant is unuseful it is removed
			if(!used) ii.remove();
		}
			
		// add to the useful labels the ones that cannot be removed
		for(Invariant i : this.invariants)
			usedLabels.addAll(i.getCondition().getLabels());
				
		
		// removes the unused guards
		HashSet<Label> unchangebles = findUnchangeableLabels();
		for(ECArule r : this.rules)
		{
			Iterator<Label> itl = r.getGuards().iterator();
			while(itl.hasNext())
			{
				Label l = itl.next();
				if(unchangebles.contains(l))
					itl.remove();
			}
		}
		
		this.setLabels(usedLabels);
	}
	
	
	/**
	 * even if the invariants dont "block" an actuator, if there are no assignments on it,
	 * its value cant change. this is checked only on usable rules
	 * @return
	 * @throws AutomatonException
	 */
	private HashSet<Label> findUnusedActuators(HashSet<ECArule> someUnusableRules) throws AutomatonException
	{
		HashSet<Label> usables = new HashSet<Label>();
		HashSet<Label> unusables = this.getActuators();
		
		for(ECArule r: this.getRules())
		{
			if(!someUnusableRules.contains(r))
				for(Action a: r.getActions())
					usables.add(a.getLabel());
		}
		
		unusables.removeAll(usables);
		
		return unusables;
	}
	
	/**
	 * we get all the devices that cant change due to the invariants
	 * eg:  [a==true] a can be only false, so the rule a[true].. cannot be activated	
	 * @return
	 * @throws AutomatonException
	 */
	private HashSet<Label> findUnchangeableLabels() throws AutomatonException
	{
		HashSet<Label> unchangebles = new HashSet<Label>();

		// Settin up Z3
		HashMap<String, String> cfg = new HashMap<String, String>();
		cfg.put("model", "true");
		Context ctx = new Context(cfg);
		
		// merge all the invariants
		Expression invs = Invariant.merge(this.invariants).getCondition();
		// get the sat model
		Model m = invs.checkSatisfiability(ctx);
		
		// iterate over all the variables in the invariants
		for(Label l : invs.getLabels())
		{
			// create an expression only with the current label (eg: A)
			Expression labelExpr = new Expression(l, l.getType());
			// get the z3 expr of the label (eg: 1)
			Expr labelExprZ3 = labelExpr.getZ3Consts(ctx).iterator().next();
			
			// specify that the current label must be of a different value from the one in the model (eg: A != 1)
			BoolExpr neqLabelZ3 = ctx.mkEq(labelExprZ3, m.eval(labelExprZ3, false));	
			neqLabelZ3 = ctx.mkNot(neqLabelZ3);
			neqLabelZ3 = ctx.mkAnd(neqLabelZ3, (BoolExpr) invs.z3Expression(ctx));
			    
		    Solver s = ctx.mkSolver();
		    s.add(neqLabelZ3);
		    if (s.check() != Status.SATISFIABLE)
		    	unchangebles.add(l);			
		}
		
		return unchangebles;
	}
	
	
	
	public String getT2file(ECAfileGUI file) throws AutomatonException
	{
		String path = "";
		
		try
		{
			HashMap<Label, Label> updates = new HashMap<Label, Label>();
			
            ECAfile tmpECAfile = this.clone();
            tmpECAfile.removeUnused();

			int count = 0;
			for(Label l : tmpECAfile.getSensors())
			{
				updates.put(l, new Label("S" + count, l.getType(), Kind.IN));
				count++;
			}
			count = 0;
			for(Label l : tmpECAfile.getActuators())
			{
				updates.put(l, new Label("A" + count, l.getType(), Kind.OUT));
				count++;
			}
	
			HashMap<Label, String> initialActuators = new HashMap<Label, String>();
			
			for(Label l : GUIutils.inputSubmit.keySet())
			{
				String varValue = "";
				if(l.getType() == Type.INT)
				{
					JSpinner tmpjs = (JSpinner) GUIutils.inputSubmit.get(l);
					varValue = tmpjs.getValue().toString();
				}
				else if(l.getType() == Type.BOOL)
				{
					JRadioButton tmprb = (JRadioButton) GUIutils.inputSubmit.get(l); //falseBTN
					if(tmprb.isSelected())
						varValue = "0";
					else
						varValue = "1";
				}
					
				initialActuators.put(updates.get(l), varValue);
			}
				
    	    //create a temp file
    	    File temp = File.createTempFile("t2t", ".t2");
    	    temp.deleteOnExit();
    	    
    	    //write it
    	    BufferedWriter bw = new BufferedWriter(new FileWriter(temp));
    	   
    	    bw.write("// " + file.getName() + "\n//\n");
    	    try (BufferedReader br = new BufferedReader(new FileReader(file))) 
    	    {
    	        String line;
    	        while ((line = br.readLine()) != null)
    	        	bw.write("// " + line + "\n");
    	    }
    	    
    	    String invs = Invariant.merge(tmpECAfile.getInvariants()).getCondition().replaceLabels(updates).T2Expression().toT2String();
    	    String boolsRestrictions = "";
    	    for(Label l : tmpECAfile.labels)
    	    	if(l.getType() == Type.BOOL)
    	    		boolsRestrictions += " && ( " + updates.get(l).getName() + ">=0 ) && ( " + updates.get(l).getName() + "<=1 )";
    	    
    	    bw.write("\n\nSTART: 10;\n");
    	    bw.write("FROM: 10;\n");
    	        	    
    	    bw.write("assume( " + invs + boolsRestrictions + " );\n\n");
     	    
    	    for(Label s : tmpECAfile.getSensors())
    	    {
    	     	bw.write(updates.get(s).getName() + " := nondet(); // " + s.getName() + "\n");
    	     	bw.write(updates.get(s).getName() + "_changed := nondet();\n");
    	    }
    	    
    	    for(Label a : tmpECAfile.getActuators())
    	    {
    	    	bw.write(updates.get(a).getName() + " := " + initialActuators.get(updates.get(a)) + "; // " + a.getName() + "\n");
    	    	bw.write(updates.get(a).getName() + "_changed := 0;\n");
    	    }
    	   
    	    bw.write("TO: 0;\n");
    	   
    	    for(ECArule r : tmpECAfile.getRules())
    	    {
    	    	bw.write("\n// " + r.getID() + "\n");
        	    bw.write("FROM: 0;\n");
        	    
        	    String guards = "";
        	    for(Label g : r.getGuards())
        	    	guards = guards + "(!(" + updates.get(g).getName() + "_changed == 0)) || ";
        	    guards = guards.substring(0, guards.length() - 4); // remove the last OR

        	    bw.write("assume( " + invs + boolsRestrictions + " && " + guards + " && " + r.getCondition().replaceLabels(updates).T2Expression().toT2String() + ");\n");
        	    
        	    HashSet<Label> unusedO = tmpECAfile.getActuators();
        	    
        	    for(Action o : r.getActions())
        	    {
            	    bw.write(updates.get(o.getLabel()).getName() + "_prime := " + o.getExpression().replaceLabels(updates).T2ExpressionWithoutBooleans().toT2String() + ";\n");
            	    bw.write(updates.get(o.getLabel()).getName() + "_changed := (" + updates.get(o.getLabel()).getName() + " - " + updates.get(o.getLabel()).getName() + "_prime);\n");
            	    bw.write(updates.get(o.getLabel()).getName() + " := " + updates.get(o.getLabel()).getName() + "_prime;\n");       	    	
            	    unusedO.remove(o.getLabel());
        	    }
        	    
        	    for(Label u : unusedO)
        	    	bw.write(updates.get(u).getName() + "_changed := 0;\n");

          	    for(Label i : tmpECAfile.getSensors())
        	    	bw.write(updates.get(i).getName() + "_changed := 0;\n");
          	  
          	    bw.write("TO: 0;\n");
    	    }
    	    
    	    bw.close();

    	    path = temp.getAbsolutePath();
    	}
		catch(IOException e)
		{
			e.printStackTrace();
    	}
		catch (AutomatonException e1) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e1.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
		}
		
		return path;
	}
		
	@Override
	public String toString()
	{
		String f = "";
	
		for (Label l : labels)
	   		f = f + l.toString() + "\n";
	   
		f = f + "\n";
		for (Invariant i : invariants)
	   		f = f + i.toString() + "\n";
		
		f = f + "\n";
		for (ECArule r : rules)
	   		f = f + r.toString() + "\n";
	   
		return f;
	}

	@Override
	public int hashCode() 
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invariants == null) ? 0 : invariants.hashCode());
		result = prime * result + ((labels == null) ? 0 : labels.hashCode());
		result = prime * result + ((rules == null) ? 0 : rules.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) 
	{
		if (this == obj)
			return true;
	
		if (obj == null)
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		ECAfile other = (ECAfile) obj;
		if (invariants == null) 
		{
			if (other.invariants != null)
				return false;
		}
		else if (!invariants.equals(other.invariants))
			return false;
		if (labels == null) 
		{
			if (other.labels != null)
				return false;
		}
		else if (!labels.equals(other.labels))
			return false;
		if (rules == null) 
		{
			if (other.rules != null)
				return false;
		}
		else if (!rules.equals(other.rules))
			return false;
		
		return true;
	}

	
}
