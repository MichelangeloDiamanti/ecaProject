package ecaAnalysis.Classes;

import java.io.IOException;
import java.util.Queue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.math.BigInteger;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

import ecaAnalysis.Classes.AutomatonCore.Vertex;

public class AutomatonUtils 
{

	public static final int INT_MIN_VAL = -128;
	public static final int INT_MAX_VAL = 127;
	
	// the engine to evaluate expressions
    public static ScriptEngine engine = new ScriptEngineManager().getEngineByName("JavaScript");

    // list of vertices that delimeter islands
    public static Queue<Vertex> parallelVertices = new ConcurrentLinkedQueue<Vertex>();
    private static ConcurrentHashMap<BigInteger, Vertex> parallelVerticesTrash = new ConcurrentHashMap<BigInteger, Vertex>();
    
    // logger (file)
	private static final Logger logger = Logger.getLogger(Automaton.class.getName());
	private static FileHandler fh;
    
	// negative count of incorrect states
	private static BigInteger incorrectStates = new BigInteger("0");
		
    public static String evaluateExpression(String expr)
    {
    	String result = "";
    	
    	// replace the AND, OR and := symbols to make the evaluator interprets them right
    	expr = expr.replace("|", "||");
    	expr = expr.replace("&", "&&");
    	expr = expr.replace(":=", "=");
		
		// evaluate the expression using the JavaScript engine
		try 
		{
			result = engine.eval(expr).toString();
		}
		catch (ScriptException e) { e.printStackTrace(); } 
    	
		return result;
    }
    
    /**
     * Initiates a logger to file for the automaton class
     * @return Logger
     */
    public static Logger getLogger()
    {
		try 
		{
			logger.setUseParentHandlers(false);
		    // This block configure the logger with handler and formatter  
			fh = new FileHandler("AutomatonLog");
		    logger.addHandler(fh);
		    SimpleFormatter formatter = new SimpleFormatter();  
		    fh.setFormatter(formatter);  
		} 
		catch (SecurityException | IOException e) { e.printStackTrace(); }
		
	    return AutomatonUtils.logger;
    }
    
    
    public static void offerParallelVertex(Vertex v)
    {
    	if(!AutomatonUtils.parallelVertices.contains(v) && !AutomatonUtils.parallelVerticesTrash.containsKey(v.getState()))
    		AutomatonUtils.parallelVertices.add(v);
    }
    
    public static void deleteParallelVertex(Vertex v)
    {
    	AutomatonUtils.parallelVerticesTrash.put(v.getState(), v);   	
    }
    
    public static synchronized BigInteger getIncorrectStateID()
    {
       	AutomatonUtils.incorrectStates = AutomatonUtils.incorrectStates.subtract(new BigInteger("1"));
    	return AutomatonUtils.incorrectStates;
    }
    
    public static void clear()
    {
        AutomatonUtils.parallelVertices.clear();
	    AutomatonUtils.parallelVerticesTrash.clear();
	    AutomatonUtils.incorrectStates = new BigInteger("0");
    }
    
}
