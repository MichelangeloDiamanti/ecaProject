package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.ECAfile;
import ecaAnalysis.Parser.ParseException;
import ecaAnalysis.Parser.ecaGrammar;

public class CheckErrorsAction implements ActionListener
{	
	public void actionPerformed(ActionEvent e)
	{			
		ECAfile ecafile = null;
	JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
		
		try 
		{
			ecafile = getCheckedEcafile();
			if(ecafile != null)
			{
				GUIutils.jerrorsareas.get(tabIndex).setText("No errors");
				GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
			}
		} 
		catch (AutomatonException e1) 
		{
			GUIutils.jerrorsareas.get(tabIndex).setText(e1.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
			e1.printStackTrace();
		}
		
	}
	
	public ECAfile getCheckedEcafile() throws AutomatonException
	{
		ECAfile ecafile = null;
		int fc = JFileChooser.APPROVE_OPTION;
		
		if(!GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent()).isSaved())
			fc = new SaveAction().save((JSplitPane) GUI.inputTabs.getSelectedComponent());
					
		if(fc == JFileChooser.APPROVE_OPTION)
		{
			if(GUIutils.ECAfilesGUI.size() > 0)
			{
				if(GUI.inputTabs.getSelectedIndex() != -1)
				{
					JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
					GUIutils.jerrorsareas.get(tabIndex).setText(""); 
					
					try 
					{
						Path inputPath = Paths.get(GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent()).getAbsolutePath());
						ecafile = ecaGrammar.getECAfile(inputPath);
					} 
					catch (ParseException e1)
					{
						GUIutils.jerrorsareas.get(tabIndex).setText(e1.getMessage());
						GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
						ecafile = null;
					}					
				}
            }
		}
		
		return ecafile;
	}
	
	
	
	
}