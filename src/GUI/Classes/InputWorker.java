package GUI.Classes;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashSet;

import javax.swing.AbstractButton;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JToolBar;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import GUI.GUI;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.ECAfile;

public class InputWorker extends SwingWorker<Integer, Integer>
{	
	private boolean errors = false;
	
	public InputWorker()
	{
		GUI.progressBar.setIndeterminate(true);
		
		if(GUIutils.automata.get(GUIutils.BGJobTabKey) != null)
		{
			if(GUIutils.automata.get(GUIutils.BGJobTabKey).getRoot() != null)
				GUIutils.automatonBAK = GUIutils.automata.get(GUIutils.BGJobTabKey).clone();
		}			
		else
			GUIutils.automatonBAK = null;
	}
	
	@Override
	protected Integer doInBackground() throws AutomatonException
	{
		try
		{
			GUIutils.setProgressLabel("Checking errors");
			
			ECAfile ecafile = new CheckErrorsAction().getCheckedEcafile();
			
			GUIutils.setProgressLabel("Generating input components");
			
			if(ecafile != null)
			{	
				ecafile.checkInvariantsSAT();
				GUIutils.automata.put(GUIutils.BGJobTabKey, new Automaton(ecafile));			
							
				if(!this.isCancelled())
					if(GUIutils.automata.get(GUIutils.BGJobTabKey) != null)
					{	
						createInputPanels(GUIutils.BGJobTabKey);
						
						// put the input components in a map in order to make the disable method easier
						HashSet<Component> simulationInput = new HashSet<Component>();
						for (Component c : GUIutils.jactuatorsareas.get(GUIutils.BGJobTabKey).getComponents()) 
							simulationInput.add(c);
						for (Component c : GUIutils.jsensorsareas.get(GUIutils.BGJobTabKey).getComponents()) 
							simulationInput.add(c);
						for (Component c : GUIutils.jinitialstateareas.get(GUIutils.BGJobTabKey).getComponents()) 
							simulationInput.add(c);

						simulationInput.add(GUIutils.jactuatorsareas.get(GUIutils.BGJobTabKey));
						simulationInput.add(GUIutils.jsensorsareas.get(GUIutils.BGJobTabKey));
						simulationInput.add(GUIutils.jinitialstateareas.get(GUIutils.BGJobTabKey));
						
						GUIutils.disabled.put("simulationInput", simulationInput);
					}
			}
			else 
				errors = true;
	
			return 0;
		}
		catch(AutomatonException e)
		{
			this.errors = true;
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
						
			return 1;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
			return 1;
		}
	}
	

	@Override
	protected void done()
	{	
		if(!this.isCancelled() && !errors)
		{
			GUI.progressBar.setIndeterminate(false);
			GUIutils.setProgressLabel("Waiting for input");
		}
		else if(this.errors)
		{
			new StopAction().stop();
		}
	
		GUI.progressBar.setValue(0);
	
		return;	
		
	}
	
	private void createInputPanels(JSplitPane i) throws AutomatonException
	{	
		if(GUIutils.automata.get(i) != null)
		{
					
			if(GUIutils.jinitialstateareas.get(i).getComponentCount() > 0)
				GUIutils.jinitialstateareas.get(i).removeAll();
			
			GUIutils.inputSubmit.clear();
			
			JSplitPane splitPanel = new JSplitPane();
			
			JPanel actuatorsWrapper = new JPanel(new BorderLayout());
			JToolBar aToolBar = new JToolBar();
			aToolBar.setFloatable(false);
			JLabel lblA = new JLabel("Actuators");
			aToolBar.add(lblA);
			actuatorsWrapper.add(aToolBar, BorderLayout.NORTH);
			
			Component verticalStrut = Box.createVerticalStrut(27);
			aToolBar.add(verticalStrut);
			
			JPanel actuatorsPanel = GUIutils.createInputComponents(GUIutils.automata.get(GUIutils.BGJobTabKey).getEcafile().getActuators(),GUIutils.automata.get(GUIutils.BGJobTabKey));
			JScrollPane scrollActuatorsPanel = new JScrollPane(actuatorsPanel);
			scrollActuatorsPanel.getVerticalScrollBar().setUnitIncrement(10);
			actuatorsWrapper.add(scrollActuatorsPanel , BorderLayout.CENTER);
			
			splitPanel.setLeftComponent(actuatorsWrapper);
			
			
			JPanel sensorsWrapper = new JPanel(new BorderLayout());
			JToolBar sToolBar = new JToolBar();
			sToolBar.setFloatable(false);
			JLabel lblS = new JLabel("Sensors");
			sToolBar.add(lblS);
			sensorsWrapper.add(sToolBar, BorderLayout.NORTH);
			
			Component horizontalGlue = Box.createHorizontalGlue();
			sToolBar.add(horizontalGlue);
						
			JCheckBox iscc = new JCheckBox("Specify the initial configuration");
			iscc.setToolTipText("If this option is unchecked the system will compute an initial configuration automatically");
			iscc.setHorizontalAlignment(SwingConstants.CENTER);
			sToolBar.add(iscc);
			
		    ActionListener isccActionListener = new ActionListener()
		    {
		    	@Override
		        public void actionPerformed(ActionEvent actionEvent) 
		        {
		          AbstractButton abstractButton = (AbstractButton) actionEvent.getSource();
		          if(abstractButton.getModel().isSelected())
		        	  setSensorPanelEnable(true);
		          else
		        	  setSensorPanelEnable(false);
		        }
		    };
		    iscc.addActionListener(isccActionListener);
		      
		      
			JPanel sensorsPanel = GUIutils.createInputComponents(GUIutils.automata.get(GUIutils.BGJobTabKey).getEcafile().getSensors(),GUIutils.automata.get(GUIutils.BGJobTabKey));
			JScrollPane scrollSensorsPanel = new JScrollPane(sensorsPanel);
			scrollSensorsPanel.getVerticalScrollBar().setUnitIncrement(10);
			sensorsWrapper.add(scrollSensorsPanel , BorderLayout.CENTER);
				
			splitPanel.setRightComponent(sensorsWrapper);
			splitPanel.setResizeWeight(0.5);
			splitPanel.setContinuousLayout(true);
			
			GUIutils.jinitialstateareas.get(i).add(splitPanel, BorderLayout.CENTER);
			
			JButton submit = new JButton("Submit");
			submit.addActionListener(new SubmitAction());
			GUIutils.jinitialstateareas.get(i).add(submit, BorderLayout.SOUTH);
			
			GUIutils.joutputtabs.get(GUIutils.BGJobTabKey).setSelectedIndex(0); //0=input
			  
			GUIutils.jactuatorsareas.put(i, actuatorsPanel);
			GUIutils.jsensorsareas.put(i, sensorsPanel);
			setSensorPanelEnable(false);
			
			
			GUI.frame.repaint();
		}
	}
	
	void setSensorPanelEnable(boolean enable)
	{
		if(GUIutils.BGJobTabKey != null)
		{
			for (Component c : GUIutils.jsensorsareas.get(GUIutils.BGJobTabKey).getComponents()) 
				c.setEnabled(enable);
		}
		
		GUIutils.sensorConfigFlag.put(GUIutils.BGJobTabKey, enable);
	}
	
}