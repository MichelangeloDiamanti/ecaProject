package GUI.Classes;
 
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;

import ecaAnalysis.Classes.AutomatonCore.Vertex;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.StatesTable;
 
/**
 * Custom renderer to display a country's flag alongside its name
 *
 * @author wwww.codejava.net
 */
@SuppressWarnings("serial")
public class VertexRenderer extends JLabel implements ListCellRenderer<Vertex> {

	StatesTable st = null;
	
	public VertexRenderer(StatesTable st){
		this.st = st;
	}
	
	@Override
	public Component getListCellRendererComponent(JList<? extends Vertex> list, Vertex vertex, int index,
	        boolean isSelected, boolean cellHasFocus) 
	{
        try 
        {
        	setOpaque(true);
        	setText(vertex.toString() + " " + vertex.toString(this.st));
			
            if (isSelected) 
            {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } 
            else 
            {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            } 
	        
		} 
        catch (AutomatonException e)
        {
			e.printStackTrace();
		} 
        return this;	
    }
     
}