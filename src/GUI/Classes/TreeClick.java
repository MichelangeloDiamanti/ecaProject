package GUI.Classes;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import GUI.GUI;

public class TreeClick 
{
	public MouseAdapter getMouseAdapter()
	{
		return new MouseAdapter()
		{
			public void mouseClicked(MouseEvent e) 
			{
				if (e.getClickCount() == 2) 
				{
					if(GUI.tree.getLastSelectedPathComponent() != null)
					{
						ECAfileGUI file = (ECAfileGUI) GUI.tree.getLastSelectedPathComponent();
						if(file.getName().endsWith(".txt"))
							new OpenAction().createTab(file);
					}
				}
			}
		};
	}
}