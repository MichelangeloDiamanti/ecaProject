package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSplitPane;

import GUI.GUI;
import GUI.Classes.Enumerations.QueriesJobs;

public class FindPathsSubmitAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		GUIutils.BGJobTabKey = (JSplitPane) GUI.inputTabs.getSelectedComponent();
		GUIutils.backgroundJob = new QueriesWorker(QueriesJobs.FIND_PATHS);
		GUIutils.backgroundJob.execute();
	}
}