package GUI.Classes;

import java.io.File;
import java.io.IOException;

public class ECAfileGUI extends File
{

	private static final long serialVersionUID = 1L;

	private boolean isDummy = false;
	private boolean isSaved = true;
	
	public ECAfileGUI(File file)
	{
		super(file.getPath());
	}
	
	public ECAfileGUI(String path)
	{
		super(path);
	}
	
	@Override
	public String getCanonicalPath()
	{
		String cp = null;
		try 
		{
			cp = super.getCanonicalPath();
		} 
		catch (IOException e) {	e.printStackTrace(); }
		
		return cp;
	}
	
	@Override
	public String toString()
	{
		return super.getName();
	}

	public boolean isDummy() 
	{
		return isDummy;
	}

	public void setDummy(boolean isDummy) 
	{
		this.isDummy = isDummy;
	}

	public boolean isSaved() 
	{
		return isSaved;
	}

	public void setSaved(boolean isSaved) 
	{
		this.isSaved = isSaved;
	}
	
	
}
