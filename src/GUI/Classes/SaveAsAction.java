package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import GUI.GUI;

public class SaveAsAction implements ActionListener
{

	public void actionPerformed(ActionEvent e)
	{
		saveAs((JSplitPane) GUI.inputTabs.getSelectedComponent());
	}
	
	/**
	 * exports a graph into an existing file (selected with a file chooser)
	 * using the standard GraphML (XML for graphs) 
	 */
	public int saveAs(JSplitPane index)
	{		
		int fc = JFileChooser.CANCEL_OPTION;
		if(GUI.inputTabs != null)
		{
			if(index != null)
			{
				JFileChooser fileChooser = new JFileChooser();
				fileChooser.setDialogTitle("Choose a file to save the file");
				fileChooser.setApproveButtonText("Save");
				fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				fileChooser.setFileFilter(new FileNameExtensionFilter("text file","txt"));
				
				fc = fileChooser.showOpenDialog(null);
				if(fc == JFileChooser.APPROVE_OPTION)
				{
					File file = fileChooser.getSelectedFile();
					ECAfileGUI savedFile = new ECAfileGUI(file);
					
					try 
					{
						// append the extension to the filename if necessary
						String destination = savedFile.getCanonicalPath().endsWith(".txt") ? savedFile.getCanonicalPath() : savedFile.getCanonicalPath() + ".txt";
						savedFile = new ECAfileGUI(destination);
						
						PrintWriter writer = new PrintWriter(destination, "UTF-8");
				
						String text = GUIutils.jtextpanes.get(index).getText();
						writer.println(text);
						writer.close();
						
						JSplitPane key = (JSplitPane) GUI.inputTabs.getSelectedComponent();
						GUIutils.ECAfilesGUI.put(key, savedFile);
						GUI.frame.setTitle(GUIutils.getTitle());
						
						GUI.inputTabs.setTitleAt(GUI.inputTabs.getSelectedIndex(), savedFile.getName() + "   ");
						
						new RefreshAction().refresh();
						
					} catch (FileNotFoundException | UnsupportedEncodingException e1) { e1.printStackTrace(); }
				}
			}
		}
		return fc;
	}
}