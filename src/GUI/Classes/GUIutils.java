package GUI.Classes;

import java.awt.Component;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.prefs.Preferences;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingWorker;
import javax.swing.undo.UndoManager;

import org.graphstream.graph.Graph;

import GUI.GUI;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonCore.Vertex;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.Label;
import ecaAnalysis.Classes.Variable;
import ecaAnalysis.Classes.Enumerations.Type;
import net.miginfocom.swing.MigLayout;

public class GUIutils 
{
	private static BigInteger hundredPercent = new BigInteger("-1");
	
	public static boolean SeqOrPar = true; //true = sequential
	public static boolean ExtOrCom = true; //true = extended
	public static Automaton automatonBAK = null;
	
	public static SwingWorker<Integer, Integer> backgroundJob = null;
	public static HashSet<Variable> initialState = new HashSet<Variable>(); 
	public static HashMap<Label, Component> inputSubmit = new HashMap<Label, Component>();
	public static HashMap<String, HashSet<Component>> disabled = new HashMap<String, HashSet<Component>>();

	public static JSplitPane BGJobTabKey = null;
	
	public static HashMap<JSplitPane,ECAfileGUI> ECAfilesGUI = new HashMap<JSplitPane,ECAfileGUI>();
	public static HashMap<JSplitPane,Automaton> automata = new HashMap<JSplitPane,Automaton>();
    public static HashMap<JSplitPane,UndoManager> undoManagers = new HashMap<JSplitPane,UndoManager>();
	public static HashMap<JSplitPane,JTabbedPane> joutputtabs = new HashMap<JSplitPane,JTabbedPane>();
	public static HashMap<JSplitPane,JTextPane> jtextpanes = new HashMap<JSplitPane,JTextPane>();
	public static HashMap<JSplitPane,JPanel> jgraphsareas = new HashMap<JSplitPane,JPanel>();
	public static HashMap<JSplitPane,Graph> graphs = new HashMap<JSplitPane,Graph>();
	public static HashMap<JSplitPane,JTextArea> jerrorsareas = new HashMap<JSplitPane,JTextArea>();
	public static HashMap<JSplitPane,JPanel> jinitialstateareas = new HashMap<JSplitPane,JPanel>();
	public static HashMap<JSplitPane,JPanel> jactuatorsareas = new HashMap<JSplitPane,JPanel>();
	public static HashMap<JSplitPane,JPanel> jsensorsareas = new HashMap<JSplitPane,JPanel>();
	public static HashMap<JSplitPane,JPanel> jtermactuatorsareas = new HashMap<JSplitPane,JPanel>();
	public static HashMap<JSplitPane,JSplitPane> jtermactuatorsparent = new HashMap<JSplitPane,JSplitPane>();
	public static HashMap<JSplitPane,JTabbedPane> jqueriesareas = new HashMap<JSplitPane,JTabbedPane>();
	public static HashMap<JSplitPane,ArrayList<Component>> jqueriesresults = new HashMap<JSplitPane,ArrayList<Component>>();
	public static HashMap<JSplitPane,JTabbedPane> janalysisareas = new HashMap<JSplitPane,JTabbedPane>();
	public static HashMap<JSplitPane,ArrayList<JTextArea>> janalysistexts = new HashMap<JSplitPane,ArrayList<JTextArea>>();
	public static HashMap<JSplitPane,Integer> analysisPreviousTab = new HashMap<JSplitPane,Integer>();
	public static HashMap<JSplitPane,JList<Vertex>> fromList = new HashMap<JSplitPane,JList<Vertex>>();
	public static HashMap<JSplitPane,JList<Vertex>> toList = new HashMap<JSplitPane,JList<Vertex>>();
	public static HashMap<JSplitPane,Boolean> sensorConfigFlag = new HashMap<JSplitPane,Boolean>();
	
	public static Preferences prefs = Preferences.userNodeForPackage(GUIutils.class);		

	
	public static String getTitle()
	{
		String title = "ECA Analyzer";
		
		
		if(GUIutils.ECAfilesGUI.size() == GUI.inputTabs.getTabCount() && GUI.inputTabs.getTabCount() > 0)
		{
			ECAfileGUI file = GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent());
			
			title = file.getCanonicalPath();
			
			if(file.isDummy())
				title = file.getName();
			
			if(!file.isSaved())
				title = title + " *";	
		}
		
			
		return title;
	}
	
	public static void setOptionsEnabled(String key, boolean d)
	{
		if(GUIutils.disabled.get(key)!=null)
			for(Component c : GUIutils.disabled.get(key))
				c.setEnabled(d);
	}
	
	public static void setOptionsEnabled(boolean d)
	{
		for(String key : GUIutils.disabled.keySet())
			if(GUIutils.disabled.get(key)!=null)
				for(Component c : GUIutils.disabled.get(key))
					c.setEnabled(d);				
	}
	
	public static void setProgressLabel(String label)
	{
		if(GUI.progressLabel != null && GUI.progressSplitPane != null)
		{
			GUI.progressLabel.setText(label);
			int lblWidth = ((int) GUI.progressLabel.getPreferredSize().getWidth()) + 25;
			if(GUI.progressSplitPane.getDividerLocation() < lblWidth)
				GUI.progressSplitPane.setDividerLocation(lblWidth);
		}		
	}
	
    public static void setPercentageProportion(BigInteger p)
    {
    	if(p.compareTo(new BigInteger("1")) == -1)
    		p = new BigInteger("1");
    	
    	GUIutils.hundredPercent = p;
    }
    
    public static void setPercentageProportion(int p)
    {
    	if(p < 1)
    		p=1;
    	GUIutils.hundredPercent = new BigInteger(Integer.toString(p));
    }
        
    public static void updatePercentage(Integer clusterCount)
    {
    	final int perc = new BigInteger(clusterCount.toString()).multiply(new BigInteger("100")).divide(GUIutils.hundredPercent).intValue();
    	
    	if(perc < 100)
	    	GUI.progressBar.setValue(perc);
    		
    }
    
    public static JPanel createInputComponents(HashSet<Label> devices, Automaton automaton) throws AutomatonException
	{	
		JPanel inPanel = new JPanel();
		inPanel.setLayout(new MigLayout("", "[][][]", "[][]"));

		HashSet<Variable> init = automaton.getInitialConfig(); 			
		int row = 0;
		for(ArrayList<Variable> alv : automaton.getStatestable().getVars())
		{ 		
			if(devices.contains(alv.get(0).getLabel()))
			{
				JLabel label = new JLabel(alv.get(0).getName());
				inPanel.add(label, "cell 0 " + row);
				
				if(alv.get(0).getType() == Type.INT)
				{
					int min = Integer.parseInt(alv.get(0).getValue()); 
					int max = Integer.parseInt(alv.get(alv.size()-1).getValue());
					int iv = 0;
					for(Variable v: init)
					{
						if(alv.contains(v))
						{
							iv = Integer.parseInt(v.getValue());
							break;
						}
					}
					JSpinner spinner = new JSpinner();
					spinner.setModel(new SpinnerNumberModel( iv, min, max, 1));
					
					if(alv.get(0).isActuator()) 
						inPanel.add(spinner, "cell 1 " + row + ",grow");	
					else if(alv.get(0).isSensor())
						inPanel.add(spinner, "cell 1 " + row + ",grow");	
											
					GUIutils.inputSubmit.put(alv.get(0).getLabel(), spinner);
				}
				else if(alv.get(0).getType() == Type.BOOL)
				{			
					ButtonGroup trueFalseGroup = new ButtonGroup();
											
					JRadioButton falseBtn = new JRadioButton("false");
					trueFalseGroup.add(falseBtn);
											
					JRadioButton trueBtn = new JRadioButton("true");
					trueFalseGroup.add(trueBtn);
											
					if(alv.get(0).isActuator()) 
					{
						inPanel.add(falseBtn, "cell 1 " + row);
						inPanel.add(trueBtn, "cell 1 " + row);
					}	
					else if(alv.get(0).isSensor())
					{
						inPanel.add(falseBtn, "cell 1 " + row);
						inPanel.add(trueBtn, "cell 1 " + row);	
					}
								
					for(Variable v: init)
					{
						if(alv.contains(v))
						{
							if(Boolean.parseBoolean(v.getValue()))
								trueBtn.setSelected(true);
							else
								falseBtn.setSelected(true);		
							break;
						}
					}
									
					GUIutils.inputSubmit.put(alv.get(0).getLabel(), falseBtn);
				}
			
			row++;
			}
		}
		return inPanel;
	}
	
   
	
    
}

