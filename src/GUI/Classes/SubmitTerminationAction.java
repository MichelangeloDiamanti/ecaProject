package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSplitPane;

import GUI.GUI;
import GUI.Classes.Enumerations.AnalyisiJobs;

public class SubmitTerminationAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		GUIutils.BGJobTabKey = (JSplitPane) GUI.inputTabs.getSelectedComponent();
		
		int componentsCount = GUIutils.jtermactuatorsareas.get(GUIutils.BGJobTabKey).getComponentCount();
		if(componentsCount > 0)
			GUIutils.setOptionsEnabled("terminationInput", false);
		
		GUIutils.backgroundJob = new AnalysisWorker(AnalyisiJobs.TERMINATION); //termination
        GUIutils.backgroundJob.execute();
	}
}