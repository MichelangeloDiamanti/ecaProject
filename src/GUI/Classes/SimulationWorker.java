package GUI.Classes;

import javax.swing.JSplitPane;
import javax.swing.SwingWorker;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonException;

public class SimulationWorker extends SwingWorker<Integer, Integer>
{	
	boolean exception = false;
	
	public SimulationWorker()
	{
		GUI.progressBar.setValue(0);
		GUIutils.setProgressLabel("Generating the automaton");
	}
	
	@Override
	protected Integer doInBackground() 
	{
		try
		{
			if(GUIutils.SeqOrPar)
				GUIutils.automata.get(GUIutils.BGJobTabKey).startSequentialSimulation(GUIutils.initialState);
            else
            	GUIutils.automata.get(GUIutils.BGJobTabKey).startParallelSimulation(GUIutils.initialState);
		
			return 0;
		}
		catch(AutomatonException e)
		{
			this.exception = true;
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
			
			return 1;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
			return 1;
		}
	}

	@Override
	protected void done()
	{
		if(!this.isCancelled() && !this.exception )
		{
			// check if the graph obj exists, eventually removes it
			if(GUIutils.jgraphsareas.containsKey(GUIutils.BGJobTabKey))
				if(GUIutils.jgraphsareas.get(GUIutils.BGJobTabKey).getComponentCount() > 0)
					GUIutils.jgraphsareas.get(GUIutils.BGJobTabKey).removeAll();
            
            GUIutils.setOptionsEnabled("graph", true);
          	                	
            GUI.progressBar.setValue(100);
            GUIutils.setProgressLabel("Simulation completed");
            
			DisplayWorker dw = new DisplayWorker(GUIutils.jgraphsareas.get(GUIutils.BGJobTabKey), GUIutils.automata.get(GUIutils.BGJobTabKey), GUIutils.BGJobTabKey);
            dw.execute();   
		}
		else if(this.exception)
		{
			new StopAction().stop();
		}
		
		GUI.frame.repaint();
		
	}
	
}