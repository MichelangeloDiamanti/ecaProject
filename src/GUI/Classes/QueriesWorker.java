package GUI.Classes;

import java.awt.BorderLayout;
import java.awt.Font;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;

import GUI.GUI;
import GUI.Classes.Enumerations.QueriesJobs;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonCore;
import ecaAnalysis.Classes.AutomatonCore.Edge;
import ecaAnalysis.Classes.AutomatonCore.Vertex;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.ECArule;
import ecaAnalysis.Classes.Label;
import ecaAnalysis.Classes.Enumerations.Confluency;
import ecaAnalysis.Classes.Enumerations.Stability;

public class QueriesWorker extends SwingWorker<Integer, Integer>
{	
	QueriesJobs jobToDo = null;
	
	public QueriesWorker(QueriesJobs job)
	{
		GUI.progressBar.setValue(0);
		jobToDo = job;
		GUIutils.setOptionsEnabled("start", false);
		GUIutils.setOptionsEnabled("analysis", false);
		GUIutils.setOptionsEnabled("queries", false);
		
		switch (jobToDo) 
		{
			case UNUSED_RULES:
				GUIutils.setProgressLabel("Finding the unused rules");
				break;
			
			case INCORRECT_RULES:
				GUIutils.setProgressLabel("Finding the incorrect rules");
				break;
			
			case DETERMINISM:
				GUIutils.setProgressLabel("Checking the determinism");
				break;
			
			case CONFLUENCY:
				GUIutils.setProgressLabel("Checking the confluency");
				break;
			
			case RULES_COUNT:
				GUIutils.setProgressLabel("Counting the rules");
				break;
			
			case MOST_USED_RULES:
				GUIutils.setProgressLabel("Finding the most used rules");
				break;
			
			case STARTING_RULES:
				GUIutils.setProgressLabel("Finding the starting rules");
				break;
			
			case ACTUATORS_UPDATES:
				GUIutils.setProgressLabel("Counting actuators updates");
				break;
			
			case MOST_UPDATED_ACTUATORS:
				GUIutils.setProgressLabel("Finding the most updated actuators");
				break;
			
			case FIND_CYCLES:
				GUIutils.setProgressLabel("Finding the cycles");
				break;
			
			case FIND_PATHS_INPUT:
				GUIutils.setProgressLabel("Generating the input");
				break;
			
			case FIND_PATHS:
				GUIutils.setProgressLabel("Finding the paths");
				break;
		}	
		
	
	}
	
	@Override
	protected Integer doInBackground() 
	{
		try 
		{
			JSplitPane tabIndex =  GUIutils.BGJobTabKey;
			String result = "";
			GUIutils.jqueriesareas.get(tabIndex).setSelectedIndex(jobToDo.jobTabIndex());
					
			// we must reinitialize the core (statestable etc..)
			new AutomatonCore(GUIutils.automata.get(tabIndex).getEcafile());
						
			switch (jobToDo) 
			{
				// UNUSED RULES
				case UNUSED_RULES:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashSet<ECArule> unused = GUIutils.automata.get(tabIndex).findUnusedRules();
					
					if(unused.isEmpty())
						result = "There are no UNUSED rules";
					else
						for (ECArule r : unused)
							result += r + "\n";
										
					GUI.progressBar.setValue(100);		
										
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
					
					break;
				}
				// INCORRECT RULES
				case INCORRECT_RULES:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashSet<ECArule> incorrects = GUIutils.automata.get(tabIndex).findIncorrectRules();
					
					if(incorrects.isEmpty())
						result = "There are no INCORRECT rules\n";
					else
					{
						for (ECArule r : incorrects)
							result += r + "\n";
					}
					
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// DETERMINISM
				case DETERMINISM:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					if(GUIutils.automata.get(tabIndex).isDeterministic())
						result="The automaton is DETERMINISTIC";
					else
						result = "The automaton is NOT DETERMINISTIC";
					
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// CONFLUENCY
				case CONFLUENCY:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					Confluency res = GUIutils.automata.get(tabIndex).checkConfluency();
					
					if(res == Confluency.CONFLUENT)
						result = "The automaton is CONFLUENT";
					else if(res == Confluency.NON_CONFLUENT)
						result = "The automaton is NON CONFLUENT";
					else if(res == Confluency.CYCLIC)
						result = "The automaton is CYCLIC";
					else if(res == Confluency.INCORRECT)
						result = "The automaton is INCORRECT";
					
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// RULES COUNT
				case RULES_COUNT:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashMap<ECArule, BigInteger> count = GUIutils.automata.get(tabIndex).countRulesOccurrences();
					
					for (ECArule r : count.keySet())
					{
						String times = (count.get(r).compareTo(new BigInteger("-1")) != 0 ) ? count.get(r).toString() : "∞";
						result += r.getID() + " - " + times + " times \n";
						if(times.equals("1")) 
							result = result.replace("times", "time");
					}
						
										
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// MOST USED RULES
				case MOST_USED_RULES:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashMap<ECArule, BigInteger> most = GUIutils.automata.get(tabIndex).findMostUsedRules();
					
					for (ECArule r : most.keySet())
					{
						String times = (most.get(r).compareTo(new BigInteger("-1")) != 0 ) ? most.get(r).toString() : "∞";
						result += r.getID() + " - " + times + " times \n";
						if(times.equals("1")) 
							result = result.replace("times", "time");
					}
					
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// STARTING RULES
				case STARTING_RULES:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashMap<ECArule, BigInteger> count = GUIutils.automata.get(tabIndex).countStartingRules();
					
					for (ECArule r : count.keySet())
					{
						String times = (count.get(r).compareTo(new BigInteger("-1")) != 0 ) ? count.get(r).toString() : "∞";
						result += r.getID() + " - " + times + " times \n";
						if(times.equals("1")) 
							result = result.replace("times", "time");
					}
										
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// ACTUATORS UPDATES
				case ACTUATORS_UPDATES:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashMap<Label, BigInteger> updates = GUIutils.automata.get(tabIndex).countActuatorsUpdates();
	
					for (Label l : updates.keySet())
					{
						String times = (updates.get(l).compareTo(new BigInteger("-1")) != 0 ) ? updates.get(l).toString() : "∞";
						result += l.getName() + " - " + times + " times \n";
						if(times.equals("1")) 
							result = result.replace("times", "time");
					}
					
					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
				// MOST UPDATED ACTUATORS
				case MOST_UPDATED_ACTUATORS:
				{
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText("");
					
					HashMap<Label, BigInteger> most = GUIutils.automata.get(tabIndex).findMostUpdatedActuators();
					
					for (Label l : most.keySet())
					{
						String times = (most.get(l).compareTo(new BigInteger("-1")) != 0 ) ? most.get(l).toString() : "∞";
						result += l.getName() + " - " + times + " times \n";
						if(times.equals("1")) 
							result = result.replace("times", "time");
					}

					GUI.progressBar.setValue(100);						
					((JTextArea) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex())).setText(result);
	
					break;
				}
	
	
				// CYCLES
				case FIND_CYCLES:
				{
					HashSet<ArrayList<Edge>> cycles = GUIutils.automata.get(tabIndex).findCycles();
					
					JPanel parent = ((JPanel) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex()));

					if(cycles.isEmpty())
					{		
						parent.removeAll();
		                JTextArea fpta = new JTextArea();
		                fpta.setFont(new Font("Monospace", Font.BOLD, 12));
		                fpta.setEditable(false);
		                fpta.setText("There are no CYCLES");
		                parent.add(fpta, BorderLayout.CENTER);
					}
					else
					{
						createPortionGraphs(cycles, parent);
					}
						
											
					GUI.progressBar.setValue(100);						
			
					break;
				}
				// PATHS INPUT
				case FIND_PATHS_INPUT:
				{
					createPathsInput();				
					break;
				}
				// PATHS
				case FIND_PATHS:
				{
					Vertex fromVertex = GUIutils.fromList.get(GUI.inputTabs.getSelectedComponent()).getSelectedValue();
					Vertex toVertex = GUIutils.toList.get(GUI.inputTabs.getSelectedComponent()).getSelectedValue();

					HashSet<ArrayList<Edge>> paths = GUIutils.automata.get(tabIndex).findPaths(fromVertex, toVertex);
					
					JPanel parent = ((JPanel) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex()));
					
					if(paths.isEmpty() == false)
					{
						createPortionGraphs(paths, parent);
					}
					else 
					{
						parent.removeAll();
		                JTextArea fpta = new JTextArea();
		                fpta.setFont(new Font("Monospace", Font.BOLD, 12));
		                fpta.setEditable(false);
		                fpta.setText("There are no PATHS");
		                parent.add(fpta, BorderLayout.CENTER);
					}
					
					GUI.progressBar.setValue(100);						
			
					break;
				}
			}
			return 0;
		} 
		catch (AutomatonException e) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
	
			return 1;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
			return 1;
		}

	
	}

	@Override
	protected void done()
	{		
		GUIutils.BGJobTabKey = null;
		GUIutils.setOptionsEnabled("start", true);
		GUIutils.setOptionsEnabled("analysis", true);
		GUIutils.setOptionsEnabled("queries", true); // we know that there is an automaton
		
		GUIutils.backgroundJob = null;

		AutomatonCore.clear();
		
		if(this.isCancelled())
		{
			GUI.progressBar.setIndeterminate(false);
			GUI.progressBar.setValue(0);
			GUIutils.setProgressLabel("Job cancelled");
		}
	}
	
	private void createPortionGraphs(HashSet<ArrayList<Edge>> paths, JPanel parent) throws AutomatonException
	{
		// we must cast to a JViewport instead of a JScrollPane
		parent.removeAll();
		
		JTabbedPane tabPaths = new JTabbedPane();
		int pCount = 0;
		for(ArrayList<Edge> ale : paths)
		{
			JPanel wrapper = new JPanel(new BorderLayout());
			
			DisplayWorker dw = new DisplayWorker(wrapper, ale, GUIutils.BGJobTabKey);
            dw.execute();   
			
        	tabPaths.addTab(Integer.toString(pCount), wrapper);
        	pCount++;
		}
		parent.add(tabPaths);
	}
	
	private void createPathsInput() throws AutomatonException
	{	
		JSplitPane tabIndex =  GUIutils.BGJobTabKey;

		JPanel parent = ((JPanel) GUIutils.jqueriesresults.get(tabIndex).get(jobToDo.jobTabIndex()));
		parent.removeAll();
				
		parent.setLayout(new BorderLayout());
		JSplitPane fromToSplit = new JSplitPane();
		parent.add(fromToSplit, BorderLayout.CENTER);
		JButton submit = new JButton("Compute Paths");
		submit.addActionListener(new FindPathsSubmitAction());
		
		parent.add(submit, BorderLayout.SOUTH);

		DefaultListModel<Vertex> states = new DefaultListModel<Vertex>();
		Automaton a = GUIutils.automata.get(tabIndex);
		ArrayList<BigInteger> orderedVertices = new ArrayList<BigInteger>(GUIutils.automata.get(tabIndex).getVertices().keySet());
		Collections.sort(orderedVertices);
		for(BigInteger v : orderedVertices)
		{
			if(a.getVertices().get(v).getStability() != Stability.INCORRECT)
				states.addElement(a.getVertices().get(v));
		}
			
		
		JPanel fromPanel = new JPanel(new BorderLayout());
		JLabel fromLabel = new JLabel("FROM:");
		fromLabel.setHorizontalAlignment(SwingConstants.CENTER);
		fromPanel.add(fromLabel, BorderLayout.NORTH);
				
		JList<Vertex> fromList = GUIutils.fromList.get(tabIndex);
		fromList.setModel(states);
		fromList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane fromListScroll = new JScrollPane(fromList);
		fromPanel.add(fromListScroll, BorderLayout.CENTER);
		fromList.setCellRenderer(new VertexRenderer(a.getStatestable()));
		fromList.setSelectedIndex(0);
		
		fromToSplit.setLeftComponent(fromPanel);
		
		JPanel toPanel = new JPanel(new BorderLayout());
		JLabel toLabel = new JLabel("TO:");
		toLabel.setHorizontalAlignment(SwingConstants.CENTER);
		toPanel.add(toLabel, BorderLayout.NORTH);
		
		
		JList<Vertex> toList = GUIutils.toList.get(tabIndex);
		toList.setModel(states);
		toList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		JScrollPane toListScroll = new JScrollPane(toList);
		toPanel.add(toListScroll, BorderLayout.CENTER);
		toList.setCellRenderer(new VertexRenderer(a.getStatestable()));
		toList.setSelectedIndex(0);
		
		fromToSplit.setRightComponent(toPanel);
		
		fromToSplit.setResizeWeight(0.5);
		fromToSplit.setContinuousLayout(true);
		
		parent.add(fromToSplit);
		
		GUIutils.setProgressLabel("Waiting for input");
	}
	

	
}