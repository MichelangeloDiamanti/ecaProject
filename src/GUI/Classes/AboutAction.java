package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import GUI.GUI;

public class AboutAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		String about = "vIRONy 1.1 \n\n"
					 + "A tool for analyzing and simulating programs written in IRON format \n";
		
		JOptionPane.showMessageDialog(GUI.frame, about);
	}
}