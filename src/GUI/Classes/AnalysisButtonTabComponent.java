package GUI.Classes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashSet;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.UIManager;
import javax.swing.plaf.basic.BasicButtonUI;

import GUI.GUI;
import GUI.Classes.Enumerations.AnalyisiJobs;

public class AnalysisButtonTabComponent extends JPanel 
{

	private static final long serialVersionUID = 1L;
	private final JTabbedPane pane;

    public AnalysisButtonTabComponent(final JTabbedPane pane, final String lbl) 
    {
        //unset default FlowLayout' gaps
        super(new FlowLayout(FlowLayout.LEFT, 0, 0));
        if (pane == null) 
            throw new NullPointerException("TabbedPane is null");
        
        this.pane = pane;
        setOpaque(false);
        	        
        //make JLabel read titles from JTabbedPane
        JLabel label = new JLabel(lbl);

        int offset = 130 - (int) label.getPreferredSize().getWidth();
       	label.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, offset)); 	
     	
        add(label);
        //tab button
        JButton button = new PlayTabButton();
        add(button);
        //add more space to the top of the component
        setBorder(BorderFactory.createEmptyBorder(2, 0, 0, 0));
        
        HashSet<Component> analisysDis = GUIutils.disabled.get("analysis");
        analisysDis.add(label);
        analisysDis.add(button);
        GUIutils.disabled.put("analysis", analisysDis);
    }
    
    private class PlayTabButton extends JButton implements ActionListener 
    {
		private static final long serialVersionUID = 1L;

		public PlayTabButton() 
		{
            int size = 17;
            setPreferredSize(new Dimension(size, size));
            //Make the button looks the same for all Laf's
            setUI(new BasicButtonUI());
            //Make it transparent
            setContentAreaFilled(false);
            //No need to be focusable
            setFocusable(false);
            setBorder(BorderFactory.createEtchedBorder());
            setBorderPainted(false);
            //Making nice rollover effect
            //we use the same listener for all buttons
            addMouseListener(buttonMouseListener);
            setRolloverEnabled(true);
            //Close the proper tab by clicking the button
            addActionListener(this);
        }

        public void actionPerformed(ActionEvent e) 
        {
            int i = pane.indexOfTabComponent(AnalysisButtonTabComponent.this);
            if (i != -1) 
            {
            	GUIutils.BGJobTabKey = (JSplitPane) GUI.inputTabs.getSelectedComponent();

				int fc = JFileChooser.APPROVE_OPTION;
				
				if(!GUIutils.ECAfilesGUI.get(GUIutils.BGJobTabKey).isSaved())
					fc = new SaveAction().save(GUIutils.BGJobTabKey);
				
				if(fc == JFileChooser.APPROVE_OPTION)
				{
					GUIutils.backgroundJob = new AnalysisWorker(AnalyisiJobs.jobFromInt(i));
	                GUIutils.backgroundJob.execute();
				}
				else
					GUIutils.BGJobTabKey = null;
            }
        }

        //we don't want to update UI for this button
        public void updateUI() 
        {
        }

        //paint the cross
        protected void paintComponent(Graphics g) 
        {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g.create();
            //shift the image for pressed buttons
            if (getModel().isPressed()) 
            	g2.translate(1, 1);
            g2.setStroke(new BasicStroke(2));
            g2.setColor(UIManager.getColor("Button.foreground"));
            if (getModel().isRollover()) 
                g2.setColor(new Color(51, 204, 51));
            int delta = 6;
            
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g2.drawLine(delta, delta, getWidth() - delta - 1, getHeight() - delta - 3);
            g2.drawLine(getWidth() - delta - 1, getHeight() - delta - 3, delta, getHeight() - delta - 1);
            g2.drawLine(delta, delta, delta, getHeight() - delta - 1);

            g2.dispose();
        }
    }

    private final MouseListener buttonMouseListener = new MouseAdapter() 
    {
        public void mouseEntered(MouseEvent e) 
        {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) 
            {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(true);
            }
        }

        public void mouseExited(MouseEvent e) 
        {
            Component component = e.getComponent();
            if (component instanceof AbstractButton) 
            {
                AbstractButton button = (AbstractButton) component;
                button.setBorderPainted(false);
            }
        }
    };
}
