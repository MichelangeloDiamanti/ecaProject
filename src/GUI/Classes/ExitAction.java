package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;

public class ExitAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		confirm();
	}
	
	public void confirm()
	{
		boolean d = false;
		for(Map.Entry<JSplitPane,ECAfileGUI> file : GUIutils.ECAfilesGUI.entrySet())
		{
			if(!file.getValue().isSaved())// valid for dummy files too
			{
				d = true;
				break;
			}
		}
	
		if(d)
		{
			int response = JOptionPane.showOptionDialog(null, "Do you want to save all the files before exit?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[]{"Yes", "No", "Cancel"}, "default");			
		    if (response == JOptionPane.NO_OPTION)
		    {
		    	System.exit(0);
		    }
		    else if(response == JOptionPane.YES_OPTION)
		    {
		
		    	for(Map.Entry<JSplitPane,ECAfileGUI> file : GUIutils.ECAfilesGUI.entrySet())
		    	{
		    		if(!file.getValue().isSaved())
		    			new SaveAction().save(file.getKey());
		    	}
		    	System.exit(0);
		    }
		    else if(response == JOptionPane.CANCEL_OPTION)
		    	return;
		}
		else
			System.exit(0);
		
		
		
	}
}