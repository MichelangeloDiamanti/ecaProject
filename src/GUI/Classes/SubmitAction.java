package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.Label;
import ecaAnalysis.Classes.Variable;
import ecaAnalysis.Classes.Enumerations.Type;

public class SubmitAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		int componentsCount = GUIutils.jinitialstateareas.get(GUIutils.BGJobTabKey).getComponentCount();
		if(componentsCount > 0)
			GUIutils.setOptionsEnabled("simulationInput", false);
		
		GUIutils.initialState.clear();
		
		for(Label l : GUIutils.inputSubmit.keySet())
		{
			if(l.isActuator() | (l.isSensor() & GUIutils.sensorConfigFlag.get(GUIutils.BGJobTabKey)))
			{			
				String varValue = "";
				if(l.getType() == Type.INT)
				{
					JSpinner tmpjs = (JSpinner) GUIutils.inputSubmit.get(l);
					varValue = tmpjs.getValue().toString();
				}
				else if(l.getType() == Type.BOOL)
				{
					JRadioButton tmprb = (JRadioButton) GUIutils.inputSubmit.get(l); //falseBTN
					if(tmprb.isSelected())
						varValue = "false";
					else
						varValue = "true";
				}
				
				try 
				{
					GUIutils.initialState.add(new Variable(l, varValue));
				}
				catch (AutomatonException e1) 
				{
					JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
					GUIutils.jerrorsareas.get(tabIndex).setText(e1.getMessage());
					GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
				}
			}
		}
		
		GUIutils.backgroundJob = new SimulationWorker();
		GUIutils.backgroundJob.execute();
	}
}