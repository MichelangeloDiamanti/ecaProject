package GUI.Classes;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;

import GUI.GUI;

public class ChangeText
{
	public DocumentListener getDocumentListener()
	{

		return new DocumentListener() 
		{
			
			private void modified()
			{
				if(GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent()).isSaved())
				{
					GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent()).setSaved(false);
					GUI.frame.setTitle(GUIutils.getTitle());					
				}
								
			}
			
			public void color(String text)
			{
			}	
			
			@Override
			public void changedUpdate(DocumentEvent arg0) 
			{
				modified();
				try 
				{
					color(arg0.getDocument().getText(0, arg0.getDocument().getLength()));
				}
				catch (BadLocationException e) { e.printStackTrace(); }
			}

			@Override
			public void insertUpdate(DocumentEvent arg0) 
			{
				modified();
				try 
				{
					color(arg0.getDocument().getText(0, arg0.getDocument().getLength()));
				}
				catch (BadLocationException e) { e.printStackTrace(); }
			}

			@Override
			public void removeUpdate(DocumentEvent arg0) 
			{
				modified();
				try 
				{
					color(arg0.getDocument().getText(0, arg0.getDocument().getLength()));
				}
				catch (BadLocationException e) { e.printStackTrace(); }
			}
		};
	}
}