package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonCore;

public class StopAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		stop();
	}
	
	public void stop()
	{
		if(GUIutils.backgroundJob != null)
		{				
			if( GUIutils.backgroundJob instanceof InputWorker ||
				GUIutils.backgroundJob instanceof SimulationWorker)
			{
				AutomatonCore.stopExecution();
			}

			GUIutils.backgroundJob.cancel(true);				
			
			if(GUIutils.automatonBAK != null)
				GUIutils.automata.put(GUIutils.BGJobTabKey, GUIutils.automatonBAK.clone());
			else
				GUIutils.automata.put(GUIutils.BGJobTabKey, null);
			
			GUIutils.automatonBAK = null;
			
			GUI.progressBar.setValue(0);			
			GUIutils.setProgressLabel("Simulation cancelled");
		}
		
		SimulationEndAction.end();
	}
}