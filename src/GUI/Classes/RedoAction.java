package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSplitPane;
import javax.swing.undo.CannotRedoException;
import javax.swing.undo.UndoManager;

import GUI.GUI;

public class RedoAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		if(GUI.inputTabs.getSelectedIndex() != -1)
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			UndoManager undoManager = GUIutils.undoManagers.get(tabIndex);
			try {
				undoManager.redo();
			} catch (CannotRedoException cannotRedoException) {}
		}
	}
}