package GUI.Classes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;

import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextPane;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.undo.UndoManager;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonCore.Vertex;

public class OpenAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		File file = null;
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Choose an input file");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setFileFilter(new FileNameExtensionFilter("ECA file","txt"));
		int fc = fileChooser.showOpenDialog(null);
		
		if(fc == JFileChooser.APPROVE_OPTION)
		{
			GUI.progressBar.setIndeterminate(true);
			GUIutils.setProgressLabel("Opening file");
			
			file = fileChooser.getSelectedFile();
			if(!file.getAbsolutePath().endsWith(".txt"))
				file = null;
			
			ECAfileGUI ecafile = new ECAfileGUI(file);
			
			Queue<String> queuePath = new LinkedList<String>();
			String[] tokenizedPath = ecafile.getCanonicalPath().split(File.separator);
		    for (int t = 0; t < tokenizedPath.length; t++)
		    	queuePath.add(tokenizedPath[t]);
		     
		    FileTreeModel.expandFilePath(queuePath, GUI.tree.getModel().getRoot(), 0);
					
			createTab(ecafile);
		
			GUI.progressBar.setIndeterminate(false);
			GUIutils.setProgressLabel("");
		}
		
        GUI.frame.repaint();
	}
	
	public void createTab(ECAfileGUI ecafile)
	{			
		if(ecafile != null)
		{
			if(GUI.inputTabs == null)
			{
				GUI.inputTabs = new JTabbedPane();	
				GUI.inputTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
				GUI.inputTabs.addChangeListener(new SwitchTab().getChangeListener() );
				GUI.splitPaneTree.setRightComponent(GUI.inputTabs);				
			}
		
			//if the file was already open
			boolean open = false;
			
			for(Map.Entry<JSplitPane,ECAfileGUI> file : GUIutils.ECAfilesGUI.entrySet())
			{
				if(file.getValue().getCanonicalPath().equals(ecafile.getCanonicalPath()))
				{
					GUI.inputTabs.setSelectedComponent(file.getKey());
					open = true;
					break;
				}			
			}
			
		
			if(!open)
			{		
				String OS = System.getProperty("os.name").toLowerCase();
				
				JSplitPane splitPane = new JSplitPane();
				splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
				splitPane.setDividerLocation(450);		
				splitPane.setContinuousLayout(true);

				GUIutils.ECAfilesGUI.put(splitPane, ecafile);
				
				JPanel inputPanel = new JPanel(false);
				inputPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
				inputPanel.setLayout(new BorderLayout());
			
				GUI.inputTabs.addTab(ecafile.getName(), splitPane);
			
				GUI.inputTabs.setTabComponentAt(GUI.inputTabs.getTabCount() -1, new CloseButtonTabComponent(GUI.inputTabs, splitPane));
				
				JTextPane textPane = new JTextPane()
				{
					private static final long serialVersionUID = 1L;
					public boolean getScrollableTracksViewportWidth()
				    {
				        return getUI().getPreferredSize(this).width <= getParent().getSize().width;
				    }
				};
				
				textPane.setFont(new Font("Monospace", Font.BOLD, 16));
				
				if(!ecafile.isDummy())
				{
					try 
					{
						String content = "";
						List<String> lines = Files.readAllLines(Paths.get(ecafile.getCanonicalPath()));
						for(String s : lines)
							content = content + s + "\n"; 
						content.replaceFirst("\\s+$", "");
						textPane.setText(content);
					} 
					catch (IOException e1){	e1.printStackTrace(); }
				}

				
				UndoManager undoManager = new UndoManager();
			    textPane.getDocument().addUndoableEditListener(undoManager);
			    
				textPane.getDocument().addDocumentListener(new ChangeText().getDocumentListener());
									
				JScrollPane scrollTextArea = new JScrollPane(textPane);
				textPane.setCaretPosition(0);
				
				TextLineNumber tln = new TextLineNumber(textPane);
				scrollTextArea.setRowHeaderView( tln );
				
				inputPanel.add(scrollTextArea, BorderLayout.CENTER);
				splitPane.setLeftComponent(inputPanel);
				
				JTabbedPane outputTabs = new JTabbedPane();
				
				
				JPanel initialStatePanel = new JPanel(new BorderLayout());
						
				JPanel graphPanel = new JPanel();
				
				
				HashSet<Component> queriesDisabled = GUIutils.disabled.get("queries"); // we konw is not null because it was already initialized
				JTabbedPane queriesPanel = new JTabbedPane(JTabbedPane.LEFT);
											
				queriesPanel.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
				queriesPanel.setBackground(Color.WHITE);
				JPanel queriesWrapper = new JPanel(new BorderLayout());
				queriesWrapper.add(queriesPanel, BorderLayout.CENTER);
				queriesDisabled.add(queriesPanel);
				GUIutils.disabled.put("queries", queriesDisabled);
				
				
				HashSet<Component> analysisDisabled = GUIutils.disabled.get("analysis"); // we konw is not null because it was already initialized
				JTabbedPane analysisPanel = new JTabbedPane(JTabbedPane.LEFT);
				analysisPanel.setBackground(Color.WHITE);
				analysisPanel.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
				analysisPanel.addChangeListener(new SwitchAnalysisTab().getChangeListener());
				JPanel analysisWrapper = new JPanel(new BorderLayout());
				analysisWrapper.add(analysisPanel, BorderLayout.CENTER);
				analysisDisabled.add(analysisPanel);
				GUIutils.disabled.put("analysis", analysisDisabled);
									
				JPanel errorsPanel = new JPanel();
				
				outputTabs.addTab("Input", initialStatePanel);
				outputTabs.addTab("Graph", graphPanel);
				outputTabs.addTab("Queries", queriesWrapper);
				outputTabs.addTab("Analysis", analysisWrapper);
				outputTabs.addTab("Errors", errorsPanel);
				
				JTextArea errorsArea = new JTextArea();
				errorsArea.setForeground(Color.RED);
				errorsArea.setEditable(false);
				JScrollPane scrollErrorsArea = new JScrollPane(errorsArea);
				errorsPanel.setLayout(new BorderLayout());
				errorsPanel.add(scrollErrorsArea, BorderLayout.CENTER);
				
				graphPanel.setLayout(new BorderLayout());
				
				ArrayList<JTextArea> analisysTexts = new ArrayList<JTextArea>();
				
				JTextArea urta = new JTextArea();
				urta.setFont(new Font("Monospace", Font.BOLD, 12));
				urta.setEditable(false);
				analisysTexts.add(urta);
				JScrollPane urtas = new JScrollPane(urta);
				analysisPanel.addTab("Unusable Rules", null, urtas, "Check if there are unusable rules");
				analysisPanel.setTabComponentAt(0, new AnalysisButtonTabComponent(analysisPanel, "Unusable Rules"));
				
				JTextArea irta = new JTextArea();
				irta.setFont(new Font("Monospace", Font.BOLD, 12));
				irta.setEditable(false);
				analisysTexts.add(irta);
				JScrollPane irtas = new JScrollPane(irta);
				analysisPanel.addTab("Incorrect Rules", null, irtas, "Check if there are potentially incorrect rules");
				analysisPanel.setTabComponentAt(1, new AnalysisButtonTabComponent(analysisPanel, "Incorrect Rules"));
				
				JTextArea rrta = new JTextArea();
				rrta.setFont(new Font("Monospace", Font.BOLD, 12));
				rrta.setEditable(false);
				analisysTexts.add(rrta);
				JScrollPane rrtas = new JScrollPane(rrta);
				analysisPanel.addTab("Redundant Rules", null, rrtas, "Check if some rules are redundant with respect to others");
				analysisPanel.setTabComponentAt(2, new AnalysisButtonTabComponent(analysisPanel, "Redundant Rules"));
				
				JTextArea dta = new JTextArea();
				dta.setFont(new Font("Monospace", Font.BOLD, 12));
				dta.setEditable(false);
				analisysTexts.add(dta);
				JScrollPane dtas = new JScrollPane(dta);
				analysisPanel.addTab("Determinism", null, dtas, "Check if some rules can generate a fork");
				analysisPanel.setTabComponentAt(3, new AnalysisButtonTabComponent(analysisPanel, "Determinism"));
				
				JTextArea tta = new JTextArea();
				tta.setFont(new Font("Monospace", Font.BOLD, 12));
				tta.setEditable(false);
				analisysTexts.add(tta);
				JScrollPane ttas = new JScrollPane(tta);
				JPanel tp = new JPanel();
				JScrollPane tps = new JScrollPane(tp);
				JSplitPane tsp = new JSplitPane();
				tsp.setLeftComponent(ttas);
				tsp.setRightComponent(tps);
				tsp.setResizeWeight(0.5);
				analysisPanel.addTab("Termination", null, tsp, "Check if the computation of the program ends");
				analysisPanel.setTabComponentAt(4, new AnalysisButtonTabComponent(analysisPanel, "Termination"));
				GUIutils.jtermactuatorsparent.put(splitPane,tsp);
				
				GUIutils.janalysistexts.put(splitPane,analisysTexts);
				
				analysisPanel.addTab("Consistency", null, new JTextArea(), "Check if there are any unusable, incorrect or redundant rules");
				analysisPanel.setBackgroundAt(5, new Color(144, 238, 144));			
				
				ArrayList<Component> queriesRes = new ArrayList<Component>();

                JTextArea urta1 = new JTextArea();
                urta1.setFont(new Font("Monospace", Font.BOLD, 12));
                urta1.setEditable(false);
                queriesRes.add(urta1);
                JScrollPane urtas1 = new JScrollPane(urta1);
                queriesPanel.addTab("Unused Rules", null, urtas1, "Displays the rules that are not used in the resulting automaton");
                queriesPanel.setTabComponentAt(0, new QueriesButtonTabComponent(queriesPanel, "Unused Rules"));

                JTextArea irta1 = new JTextArea();
                irta1.setFont(new Font("Monospace", Font.BOLD, 12));
                irta1.setEditable(false);
                queriesRes.add(irta1);
                JScrollPane irtas1 = new JScrollPane(irta1);
                queriesPanel.addTab("Incorrect Rules", null, irtas1, "Find all the rules that lead the automaton to an incorrect state");
                queriesPanel.setTabComponentAt(1, new QueriesButtonTabComponent(queriesPanel, "Incorrect Rules"));

                JTextArea dta1 = new JTextArea();
                dta1.setFont(new Font("Monospace", Font.BOLD, 12));
                dta1.setEditable(false);
                queriesRes.add(dta1);
                JScrollPane dtas1 = new JScrollPane(dta1);
                queriesPanel.addTab("Determinism", null, dtas1, "Check if the resulting automaton is deterministic (it has no forks)");
                queriesPanel.setTabComponentAt(2, new QueriesButtonTabComponent(queriesPanel, "Determinism"));

                JTextArea cta = new JTextArea();
                cta.setFont(new Font("Monospace", Font.BOLD, 12));
                cta.setEditable(false);
                queriesRes.add(cta);
                JScrollPane ctas = new JScrollPane(cta);
                queriesPanel.addTab("Confluency", null, ctas, "Check if the resulting automaton is cyclic, incorrect, non confluent or confluent");
                queriesPanel.setTabComponentAt(3, new QueriesButtonTabComponent(queriesPanel, "Confluency"));

                JTextArea rcta = new JTextArea();
                rcta.setFont(new Font("Monospace", Font.BOLD, 12));
                rcta.setEditable(false);
                queriesRes.add(rcta);
                JScrollPane rctas = new JScrollPane(rcta);
                queriesPanel.addTab("Rules Count", null, rctas, "Counts how many times each rule has been triggered");
                queriesPanel.setTabComponentAt(4, new QueriesButtonTabComponent(queriesPanel, "Rules Count"));

                JTextArea murta = new JTextArea();
                murta.setFont(new Font("Monospace", Font.BOLD, 12));
                murta.setEditable(false);
                queriesRes.add(murta);
                JScrollPane murtas = new JScrollPane(murta);
                queriesPanel.addTab("Most Used Rules", null, murtas, "Displays the rule(s) that have been activated the most");
                queriesPanel.setTabComponentAt(5, new QueriesButtonTabComponent(queriesPanel, "Most Used Rules"));

                JTextArea srta = new JTextArea();
                srta.setFont(new Font("Monospace", Font.BOLD, 12));
                srta.setEditable(false);
                queriesRes.add(srta);
                JScrollPane srtas = new JScrollPane(srta);
                queriesPanel.addTab("Starting Rules", null, srtas, "Counts all the instances of rules triggered by nature");
                queriesPanel.setTabComponentAt(6, new QueriesButtonTabComponent(queriesPanel, "Starting Rules"));

                JTextArea auta = new JTextArea();
                auta.setFont(new Font("Monospace", Font.BOLD, 12));
                auta.setEditable(false);
                queriesRes.add(auta);
                JScrollPane autas = new JScrollPane(auta);
                queriesPanel.addTab("Actuators Updates", null, autas, "Counts how many times each actuator has been updated");
                queriesPanel.setTabComponentAt(7, new QueriesButtonTabComponent(queriesPanel, "Actuators Updates"));

                JTextArea muata = new JTextArea();
                muata.setFont(new Font("Monospace", Font.BOLD, 12));
                muata.setEditable(false);
                queriesRes.add(muata);
                JScrollPane muatas = new JScrollPane(muata);
                queriesPanel.addTab("Most Updated Actuators", null, muatas, "Displays the actuators(s) that have been updated the most");
                queriesPanel.setTabComponentAt(8, new QueriesButtonTabComponent(queriesPanel, "Most Updated Actuators"));


                JPanel fcta = new JPanel(new BorderLayout());
                queriesRes.add(fcta);
                queriesPanel.addTab("Find Cycles", null, fcta, "Find all the strongly connected components that exist in the resulting automaton");
                queriesPanel.setTabComponentAt(9, new QueriesButtonTabComponent(queriesPanel, "Find Cycles"));

                JPanel fpta = new JPanel(new BorderLayout());
                queriesRes.add(fpta);
                queriesPanel.addTab("Find Paths", null, fpta, "Find all the possible paths between two given vertices");
                queriesPanel.setTabComponentAt(10, new QueriesButtonTabComponent(queriesPanel, "Find Paths"));
				GUIutils.jqueriesresults.put(splitPane,queriesRes);
				
				GUIutils.setOptionsEnabled("queries", false);
				
				splitPane.setRightComponent(outputTabs);
				
				GUI.inputTabs.setSelectedComponent(splitPane);
				GUI.frame.setTitle(GUIutils.getTitle());
				
				if(OS.indexOf("mac") >= 0) //if is mac
				{
					queriesPanel.setTabPlacement(JTabbedPane.TOP);
					analysisPanel.setTabPlacement(JTabbedPane.TOP);
				}
						
				
				textPane.requestFocus();
				
				GUIutils.jtextpanes.put(splitPane,textPane);
				GUIutils.joutputtabs.put(splitPane,outputTabs);
				GUIutils.jerrorsareas.put(splitPane,errorsArea);
				GUIutils.jgraphsareas.put(splitPane,graphPanel);
				GUIutils.graphs.put(splitPane,null);
				GUIutils.undoManagers.put(splitPane,undoManager);
				GUIutils.jinitialstateareas.put(splitPane,initialStatePanel);
				GUIutils.jqueriesareas.put(splitPane,queriesPanel);
				GUIutils.janalysisareas.put(splitPane,analysisPanel);
				GUIutils.fromList.put(splitPane,new JList<Vertex>());
				GUIutils.toList.put(splitPane,new JList<Vertex>());
				GUIutils.analysisPreviousTab.put(splitPane,0);
				GUIutils.sensorConfigFlag.put(splitPane, false);
				GUIutils.jtermactuatorsareas.put(splitPane, tp);
				
				GUIutils.automata.put(splitPane,null); 
			}
		}
	}


}