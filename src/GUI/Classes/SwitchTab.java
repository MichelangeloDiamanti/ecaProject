package GUI.Classes;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import GUI.GUI;

public class SwitchTab
{
	public ChangeListener getChangeListener()
	{
		return new ChangeListener() 
		{
			public void stateChanged(ChangeEvent changeEvent) 
			{
				GUI.frame.setTitle(GUIutils.getTitle());
				
				if(GUI.inputTabs.getTabCount() > 0)
				{
					GUIutils.setOptionsEnabled("tab", true);
					
					if(GUIutils.BGJobTabKey != null) // if the tab index is >= 0 then the simulation has started
					{
						GUIutils.setOptionsEnabled("start", false);
						GUIutils.setOptionsEnabled("stop", true);
						GUIutils.setOptionsEnabled("analysis", false);
					}
					else
					{
						GUIutils.setOptionsEnabled("start", true);
						GUIutils.setOptionsEnabled("stop", false);
						GUIutils.setOptionsEnabled("analysis", true);
					}
					                        
					// check if the graph obj exists
					GUIutils.setOptionsEnabled("graph", false);
					if(GUIutils.graphs.get(GUI.inputTabs.getSelectedComponent()) != null)
						GUIutils.setOptionsEnabled("graph", true);
	                					
					// check if the simulation was done
					GUIutils.setOptionsEnabled("queries", false);
					if(GUIutils.BGJobTabKey == null)
						if(GUIutils.automata.size() == GUI.inputTabs.getTabCount())
							if(GUIutils.automata.get(GUI.inputTabs.getSelectedComponent()) != null)
								if(GUIutils.automata.get(GUI.inputTabs.getSelectedComponent()).getRoot() != null)
									GUIutils.setOptionsEnabled("queries", true);
					
				}
				else
					GUIutils.setOptionsEnabled(false);
			}
		};
	}
}