package GUI.Classes.Enumerations;

public enum QueriesJobs 
{
	UNUSED_RULES,
	INCORRECT_RULES,
	DETERMINISM,
	CONFLUENCY,
	RULES_COUNT,
	MOST_USED_RULES,
	STARTING_RULES,
	ACTUATORS_UPDATES,
	MOST_UPDATED_ACTUATORS,
	FIND_CYCLES,
	FIND_PATHS_INPUT,
	FIND_PATHS;

	public int jobTabIndex()
	{
		int index = -1;
		
		switch (this) 
		{
		    case UNUSED_RULES:
		        index = 0;
		        break;
		    case INCORRECT_RULES:
		        index = 1;
		        break;
		    case DETERMINISM:
		        index = 2;
		        break;
		    case CONFLUENCY:
		        index = 3;
		        break;
		    case RULES_COUNT:
		        index = 4;
		        break;
		    case MOST_USED_RULES:
		        index = 5;
		        break;
		    case STARTING_RULES:
		        index = 6;
		        break;
		    case ACTUATORS_UPDATES:
		        index = 7;
		        break;
		    case MOST_UPDATED_ACTUATORS:
		        index = 8;
		        break;
		    case FIND_CYCLES:
		        index = 9;
		        break;
		    case FIND_PATHS_INPUT:
		        index = 10;
		        break;
		    case FIND_PATHS:
		        index = 10;
		        break;
		    default:
				break;
		}
		return index;
	}
	
	public static QueriesJobs jobFromInt(int i)
	{
		QueriesJobs job = null;
		
		switch (i) 
		{
	        case 0:
	            job = UNUSED_RULES;
	            break;
	        case 1:
	            job = INCORRECT_RULES;
	            break;
	        case 2:
	            job = DETERMINISM;
	            break;
	        case 3:
	            job = CONFLUENCY;
	            break;
	        case 4:
	            job = RULES_COUNT;
	            break;
	        case 5:
	            job = MOST_USED_RULES;
	            break;
	        case 6:
	            job = STARTING_RULES;
	            break;
	        case 7:
	            job = ACTUATORS_UPDATES;
	            break;
	        case 8:
	            job = MOST_UPDATED_ACTUATORS;
	            break;
	        case 9:
	            job = FIND_CYCLES;
	            break;
	        case 10:
	            job = FIND_PATHS_INPUT;
	            break;
	        case 11:
	            job = FIND_PATHS;
	            break;
			default:
				break;
		}
		
		return job;
	}

}
