package GUI.Classes.Enumerations;

public enum AnalyisiJobs 
{
	UNUSABLE_RULES,
	INCORRECT_RULES,
	REDUNDANT_RULES,
	DETERMINISM,
	TERMINATION_INPUT,
	TERMINATION,
	CONSISTENCY;

	public int jobTabIndex()
	{
		int index = -1;
		
		switch (this) 
		{
		    case UNUSABLE_RULES:
		        index = 0;
		        break;
		    case INCORRECT_RULES:
		        index = 1;
		        break;
		    case REDUNDANT_RULES:
		        index = 2;
		        break;
		    case DETERMINISM:
		        index = 3;
		        break;
		    case TERMINATION_INPUT:
		        index = 4;
		        break;
		    case TERMINATION:
		        index = 4;
		        break;
		    case CONSISTENCY:
		        index = 5;
		        break;
		    default:
				break;
		}
		return index;
	}
	
	public static AnalyisiJobs jobFromInt(int i)
	{
		AnalyisiJobs job = null;
		
		switch (i) 
		{
	        case 0:
	            job = UNUSABLE_RULES;
	            break;
	        case 1:
	            job = INCORRECT_RULES;
	            break;
	        case 2:
	            job = REDUNDANT_RULES;
	            break;
	        case 3:
	            job = DETERMINISM;
	            break;
	        case 4:
	            job = TERMINATION_INPUT;
	            break;
	        case 5:
	            job = TERMINATION;
	            break;
	        case 6:
	            job = CONSISTENCY;
	            break;
			default:
				break;
		}
		
		return job;
	}

}
