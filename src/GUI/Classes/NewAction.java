package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Map;

import javax.swing.JSplitPane;

public class NewAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		ECAfileGUI dummyFile = new ECAfileGUI(new File(getDummyFileName()));
		dummyFile.setDummy(true);
		dummyFile.setSaved(false);
		
		new OpenAction().createTab(dummyFile);
	}
	
	private String getDummyFileName()
	{
		String name = "Untitled";
		int count = 1;
		
		for(Map.Entry<JSplitPane, ECAfileGUI> f : GUIutils.ECAfilesGUI.entrySet())
			if(f.getValue().isDummy())
				count++;
				
		return name + count;
	}
}