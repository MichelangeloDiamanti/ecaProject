package GUI.Classes;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import GUI.GUI;

public class SwitchAnalysisTab
{
	public ChangeListener getChangeListener()
	{
		return new ChangeListener() 
		{
			public void stateChanged(ChangeEvent changeEvent) 
			{	
				JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			
				if(GUIutils.analysisPreviousTab.containsKey(tabIndex) && GUIutils.janalysisareas.containsKey(tabIndex))
				{
					// if compute was clicked 
					if(GUIutils.janalysisareas.get(tabIndex).getSelectedIndex() == GUIutils.janalysisareas.get(tabIndex).getTabCount() - 1)
					{
						int fc = JFileChooser.APPROVE_OPTION;
						
						if(!GUIutils.ECAfilesGUI.get(tabIndex).isSaved())
							fc = new SaveAction().save(tabIndex);
						
						if(fc == JFileChooser.APPROVE_OPTION)
						{
							GUIutils.janalysisareas.get(tabIndex).setSelectedIndex(GUIutils.analysisPreviousTab.get(tabIndex));
							GUIutils.BGJobTabKey = tabIndex;
							GUIutils.backgroundJob = new AnalysisWorker();
							GUIutils.backgroundJob.execute();
						}
					}
					else
						GUIutils.analysisPreviousTab.put(tabIndex, GUIutils.janalysisareas.get(tabIndex).getSelectedIndex());

				}
			}
		};
	}
}