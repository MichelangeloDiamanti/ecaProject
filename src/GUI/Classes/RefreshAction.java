package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.LinkedList;
import java.util.Queue;

import javax.swing.JSplitPane;

import GUI.GUI;

public class RefreshAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		refresh();
	}
	
	public void refresh()
	{
		FileTreeModel treeModel = new FileTreeModel(new ECAfileGUI(new File(System.getProperty("user.home"))));
		GUI.tree.setModel(treeModel);  
		GUI.tree.addMouseListener(new TreeClick().getMouseAdapter());
		
		if(GUIutils.ECAfilesGUI.size() > 0)
		{
			if(GUI.inputTabs.getSelectedIndex() != -1)
			{
				JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
				
				Queue<String> queuePath = new LinkedList<String>();
				String[] tokenizedPath = GUIutils.ECAfilesGUI.get(tabIndex).getCanonicalPath().split(File.separator);
			    for (int t = 0; t < tokenizedPath.length; t++)
			    	queuePath.add(tokenizedPath[t]);
			     
			    FileTreeModel.expandFilePath(queuePath, GUI.tree.getModel().getRoot(), 0);
			   
			}
        }
	}
}