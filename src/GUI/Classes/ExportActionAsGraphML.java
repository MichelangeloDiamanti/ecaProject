package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonCore.Edge;

public class ExportActionAsGraphML implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		try 
		{
			export();
		} 
		catch (AutomatonException ae) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(ae.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
		}
	}
	
	/**
	 * exports a graph into an existing file (selected with a file chooser)
	 * using the standard GraphML (XML for graphs) 
	 * @throws AutomatonException 
	 */
	public void export() throws AutomatonException
	{	
		Automaton currentAutomaton = GUIutils.automata.get(GUI.inputTabs.getSelectedComponent());
				
		if(currentAutomaton.getRoot() == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Choose a file to save the graph");
		fileChooser.setApproveButtonText("Save");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		int fc = fileChooser.showOpenDialog(null);
		if(fc == JFileChooser.APPROVE_OPTION)
		{
			File file = fileChooser.getSelectedFile();
						
			try 
			{
				String path = file.getAbsolutePath();
				if(!path.endsWith(".graphml"))
					path += ".graphml";
				
				PrintWriter writer = new PrintWriter(path, "UTF-8");
		
				writer.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
				writer.println("<graphml>");
				writer.println("	<key id=\"sk\" for=\"node\" attr.name=\"state\" attr.type=\"string\" />");
				writer.println("	<key id=\"ek\" for=\"edge\" attr.name=\"rule\" attr.type=\"string\"/>");
				writer.println("	<graph id=\"ecaGraph\" edgedefault=\"directed\">");
				
				for (BigInteger v : currentAutomaton.getVertices().keySet())
				{
					if(currentAutomaton.getVertices().get(v).isConsistent())
					{
						String state = currentAutomaton.getVertices().get(v).getState(currentAutomaton.getStatestable()).toString();
						writer.println("	<node id=\"" + currentAutomaton.getVertices().get(v).getState() + "\" class=\"" + currentAutomaton.getVertices().get(v).getStability() + "\" >");
						writer.println("		<data key=\"sk\">" + state + "</data>");
						writer.println("	</node>");
					}
					else
					{
						writer.println("	<node id=\"" + currentAutomaton.getVertices().get(v).getState() + "\">");
						writer.println("		<data key=\"sk\">INCONSISTENT</data>");
						writer.println("	</node>");
					}
				}
				
				int count = 0;
				for (String eStr : currentAutomaton.getEdges().keySet())
				{
					Edge e = currentAutomaton.getEdges().get(eStr);
					String id = "rule" + count;
					String f = e.getFrom().toString();
					String t = e.getTo().toString();
					String r = (e.getRule()==null) ? "" : e.getRule().toString();
					r=r.replace("&", "&amp;");
					r=r.replace("<", "&lt;");
					r=r.replace(">", "&gt;");
					
					count++;
					writer.println("	<edge id=\"" + id + "\" source=\"" + f + "\" target=\"" + t + "\">");
					writer.println("		<data key=\"ek\">" + r + "</data>");
					writer.println("	</edge>");
				}
				
				writer.println("</graph>");
				writer.println("</graphml>");
				writer.close();

			} catch (FileNotFoundException | UnsupportedEncodingException e1) { e1.printStackTrace(); }
		
		}
	}
}