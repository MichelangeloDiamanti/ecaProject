package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSplitPane;
import javax.swing.undo.CannotUndoException;
import javax.swing.undo.UndoManager;

import GUI.GUI;

public class UndoAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		if(GUI.inputTabs.getSelectedIndex() != -1)
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			UndoManager undoManager = GUIutils.undoManagers.get(tabIndex);
			try {
				undoManager.undo();
			} catch (CannotUndoException cannotUndoException) {}
		}
	}
}