package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;

import GUI.GUI;

public class SaveAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		save((JSplitPane) GUI.inputTabs.getSelectedComponent());
	}
	
	public int save(JSplitPane index)
	{
		int fc = JFileChooser.APPROVE_OPTION;
		
		if(GUIutils.ECAfilesGUI.size() > 0)
		{
			if(index != null)
			{
				if(GUIutils.ECAfilesGUI.get(index).isDummy())
					fc = new SaveAsAction().saveAs(index);
				else
				{
					ECAfileGUI file = GUIutils.ECAfilesGUI.get(index);
	
					PrintWriter writer;
					try 
					{
						writer = new PrintWriter(file.getCanonicalPath(), "UTF-8");
						String text = GUIutils.jtextpanes.get(index).getText();
						writer.println(text);
						writer.close();
						
						file.setSaved(true);
						GUI.frame.setTitle(GUIutils.getTitle());
					} 
					catch (FileNotFoundException | UnsupportedEncodingException e1) { e1.printStackTrace();	}
				}
			}
		}
		return fc;
	}
}