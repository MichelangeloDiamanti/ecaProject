package GUI.Classes;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashSet;

import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;

import org.graphstream.graph.Graph;
import org.graphstream.graph.implementations.MultiGraph;
import org.graphstream.ui.swingViewer.ViewPanel;
import org.graphstream.ui.view.Viewer;

import GUI.GUI;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonCore.Edge;
import ecaAnalysis.Classes.AutomatonCore.Vertex;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.Enumerations.Stability;

public class DisplayWorker extends SwingWorker<Integer, Integer>
{				
	
	private JPanel parent = new JPanel();
	private Automaton automaton = null;
	private ArrayList<Edge> path = null;
	private JSplitPane tabIndex = null;
	
	public DisplayWorker(JPanel p, Automaton a, JSplitPane ti)
	{
		this.parent = p;
		this.automaton = a;
		this.tabIndex = ti;
	}
	
	public DisplayWorker(JPanel p, ArrayList<Edge> pt, JSplitPane ti)
	{
		this.parent = p;
		this.path = pt;
		this.tabIndex = ti;
	}
	
	@Override
	protected Integer doInBackground() 
	{
		try
		{
			if(this.automaton != null && this.path == null)
			{
				this.parent.add(getGraphFromAutomaton());
			}
			else if(this.automaton == null && this.path != null)
			{
				this.parent.add(getGraphFromPath());
			}
			return 0;
		}
		catch (AutomatonException e) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
			GUIutils.setProgressLabel("Simulation cancelled");
			GUI.progressBar.setValue(0);
			return 1;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
			return 1;
		}
	}

	@Override
	protected void done()
	{		
		if(this.automaton != null && this.path == null)
		{
			GUIutils.joutputtabs.get(GUIutils.BGJobTabKey).setSelectedIndex(1); //1=graph
			SimulationEndAction.end();
		}
		
		GUI.frame.repaint();
	}
	
	
	private ViewPanel getGraphFromAutomaton() throws AutomatonException
	{
		if(this.automaton.getRoot() == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");
		
		String attr = "";
		if(GUIutils.ExtOrCom)
			attr = "node{ size-mode: fit; shape: rounded-box; fill-color: #CCC; stroke-mode: plain; padding: 5px; stroke-mode: plain; stroke-color: #555; stroke-width: 0px; text-color: #555; text-style: bold; text-size: 12; }";
		else
			attr = "node{ size-mode: fit; fill-color: #CCC; stroke-mode: plain; padding: 10px; stroke-mode: plain; stroke-color: #555; stroke-width: 0px; text-color: #555; text-style: bold; text-size: 12; }";
		
		attr = attr	+ "node.root{ text-color: #000; fill-color: #CCEECC; }"
					+ "node.stable{ fill-color: #CCEECC; }"
					+ "node.incorrect{ fill-color: #EECDCD; }"
					+ "node:clicked{ text-size: 16; }"
					+ "edge{ arrow-shape: arrow; size: 2px; }"
					+ "edge.natural{ fill-color: #555; }";
		
		
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		Graph graph = new MultiGraph("Automaton");
		
		for (BigInteger v : this.automaton.getVertices().keySet()) 
		{
			if(this.automaton.getVertices().get(v).isConsistent())
			{
				String state = (GUIutils.ExtOrCom) ? this.automaton.getVertices().get(v).getState(this.automaton.getStatestable()).toGraphString() : this.automaton.getVertices().get(v).getState().toString();
				graph.addNode(this.automaton.getVertices().get(v).getState().toString()).addAttribute("ui.label", state);

				if(this.automaton.getVertices().get(v).getStability() == Stability.STABLE) 
					graph.getNode(this.automaton.getVertices().get(v).getState().toString()).addAttribute("ui.class", "stable");
			}
			else
			{
				String state = (GUIutils.ExtOrCom) ? "INCORRECT" : this.automaton.getVertices().get(v).getState().toString();
				graph.addNode(this.automaton.getVertices().get(v).getState().toString()).addAttribute("ui.label", state);
				graph.getNode(this.automaton.getVertices().get(v).getState().toString()).addAttribute("ui.class", "incorrect");
			}				
		}
		
		graph.getNode(this.automaton.getRoot().getState().toString()).addAttribute("ui.class", "root");
		
		for (String eStr : this.automaton.getEdges().keySet())
		{
			Edge e = this.automaton.getEdges().get(eStr);
			String id = "";
			if(e.getRule() == null)
				id = e.getFrom() + "nature" + e.getTo();
			else
				id = e.getFrom() + e.getRule().getID() + e.getTo();
			
			String f = e.getFrom().toString();
			String t = e.getTo().toString();
			
			
			String rule = "";
			if(e.getRule() != null)
				rule = (GUIutils.ExtOrCom) ? e.getRule().toString() : e.getRule().getID();
				
			graph.addEdge(id, f, t, true).addAttribute("ui.label", rule);
			
			if(e.getRule() == null)
				graph.getEdge(id).addAttribute("ui.class", "natural");
			
		}
		
		graph.addAttribute("ui.stylesheet", attr);
		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");

	    Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
	    viewer.enableAutoLayout();
	    ViewPanel view = viewer.addDefaultView(false);   // false indicates "no JFrame".
	    
	    GUIutils.graphs.put(this.tabIndex, graph);

	    return view;	
	}
	
	
	
	private ViewPanel getGraphFromPath() throws AutomatonException
	{
		String attr = "";
		if(GUIutils.ExtOrCom)
			attr = "node{ size-mode: fit; shape: rounded-box; fill-color: #CCC; stroke-mode: plain; padding: 5px; stroke-mode: plain; stroke-color: #555; stroke-width: 0px; text-color: #555; text-style: bold; text-size: 12; }";
		else
			attr = "node{ size-mode: fit; fill-color: #CCC; stroke-mode: plain; padding: 10px; stroke-mode: plain; stroke-color: #555; stroke-width: 0px; text-color: #555; text-style: bold; text-size: 12; }";
		
		attr = attr	+ "node.root{ text-color: #000; fill-color: #CCEECC; }"
					+ "node.stable{ fill-color: #CCEECC; }"
					+ "node.incorrect{ fill-color: #EECDCD; }"
					+ "node:clicked{ text-size: 16; }"
					+ "edge{ arrow-shape: arrow; size: 2px; }"
					+ "edge.natural{ fill-color: #555; }";
		
		
		System.setProperty("org.graphstream.ui.renderer", "org.graphstream.ui.j2dviewer.J2DGraphRenderer");
		Graph graph = new MultiGraph("Automaton");
		
		HashSet<Vertex> pathVertices = new HashSet<Vertex>();
		for(Edge e : path)
		{
			pathVertices.add(e.getFrom());
			pathVertices.add(e.getTo());
		}
		
		for (Vertex v : pathVertices) 
		{
			if(v.isConsistent())
			{
				String state = (GUIutils.ExtOrCom) ? v.getState(GUIutils.automata.get(this.tabIndex).getStatestable()).toGraphString() : v.getState().toString();
				graph.addNode(v.getState().toString()).addAttribute("ui.label", state);

				if(v.getStability() == Stability.STABLE) 
					graph.getNode(v.getState().toString()).addAttribute("ui.class", "stable");
			}
			else
			{
				String state = (GUIutils.ExtOrCom) ? "INCORRECT" : v.getState().toString();
				graph.addNode(v.getState().toString()).addAttribute("ui.label", state);
				graph.getNode(v.getState().toString()).addAttribute("ui.class", "incorrect");
			}				
		}
		
		for (Edge e : path)
		{
			String id = "";
			if(e.getRule() == null)
				id = e.getFrom() + "nature" + e.getTo();
			else
				id = e.getFrom() + e.getRule().getID() + e.getTo();
			
			String f = e.getFrom().toString();
			String t = e.getTo().toString();
			
			
			String rule = "";
			if(e.getRule() != null)
				rule = (GUIutils.ExtOrCom) ? e.getRule().toString() : e.getRule().getID();
				
			graph.addEdge(id, f, t, true).addAttribute("ui.label", rule);
			
			if(e.getRule() == null)
				graph.getEdge(id).addAttribute("ui.class", "natural");
			
		}
		
		graph.addAttribute("ui.stylesheet", attr);
		graph.addAttribute("ui.quality");
		graph.addAttribute("ui.antialias");

	    Viewer viewer = new Viewer(graph, Viewer.ThreadingModel.GRAPH_IN_ANOTHER_THREAD);
	    viewer.enableAutoLayout();
	    ViewPanel view = viewer.addDefaultView(false);   // false indicates "no JFrame".
	    
	    
		return view;
		
	}

}