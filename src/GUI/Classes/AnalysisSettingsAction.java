package GUI.Classes;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;

import net.miginfocom.swing.MigLayout;

public class AnalysisSettingsAction implements ActionListener
{
	public static JFrame asFrame = null;
	private JTextField pathTextField;
	private JButton btnBrowse;
	private JButton okBtn;
	
	public void actionPerformed(ActionEvent e)
	{
		asFrame = new JFrame("Analysis Settings");
		
		asFrame.setMinimumSize(new Dimension(500, 400));
		asFrame.setLocationRelativeTo(null);
	
		asFrame.getContentPane().setLayout(new BorderLayout(0, 0));	
				
		asFrame.setBounds(100, 100, 450, 300);
		asFrame.getContentPane().setLayout(new MigLayout("", "[][grow][]", "[][][][][][][][][]"));
		
		JLabel lblPath = new JLabel("Path to T2");
		asFrame.getContentPane().add(lblPath, "cell 0 0,alignx trailing");
		
		pathTextField = new JTextField(GUIutils.prefs.get("T2_PATH", ""));
		asFrame.getContentPane().add(pathTextField, "flowx,cell 1 0,growx");
		pathTextField.setColumns(10);
		pathTextField.setPreferredSize(new Dimension(0, 26));
		
		btnBrowse = new JButton("Browse...");
		btnBrowse.addActionListener(new BrowseAction());
		asFrame.getContentPane().add(btnBrowse, "cell 2 0, grow, pushy, wrap push");
				
		
		okBtn = new JButton("OK");
		asFrame.getContentPane().add(okBtn, "cell 2 8, alignx right");
		okBtn.addActionListener(new OKAction());
		
		
		asFrame.setVisible(true);
		
	}
	
	public class BrowseAction implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			File file = null;
			
			JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Select the T2 exe file");
			fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileChooser.setFileFilter(new FileNameExtensionFilter("exe","exe"));
			int fc = fileChooser.showOpenDialog(null);
			
			if(fc == JFileChooser.APPROVE_OPTION)
			{
				file = fileChooser.getSelectedFile();
				
				if(file.getAbsolutePath().endsWith(".exe"))
					pathTextField.setText(file.getAbsolutePath());				
			}
			
			asFrame.repaint();
		}
	}
	
	
	public class OKAction implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			GUIutils.prefs.put("T2_PATH", pathTextField.getText());
			asFrame.dispose();
		}
	}
}