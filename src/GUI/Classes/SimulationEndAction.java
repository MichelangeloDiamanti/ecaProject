package GUI.Classes;


import GUI.GUI;

public class SimulationEndAction
{
	public static void end()
	{
		GUI.progressBar.setIndeterminate(false);
				
		int componentsCount = GUIutils.jinitialstateareas.get(GUIutils.BGJobTabKey).getComponentCount();
		if(componentsCount > 0)
			GUIutils.setOptionsEnabled("simulationInput", false);
		
		GUIutils.setOptionsEnabled("start", true);
		GUIutils.setOptionsEnabled("stop", false);
		GUIutils.setOptionsEnabled("analysis", true);	
		
		GUIutils.setOptionsEnabled("queries", false);
		if(GUIutils.automata.get(GUIutils.BGJobTabKey) != null)
			if(GUIutils.automata.get(GUIutils.BGJobTabKey).getRoot() != null)
				GUIutils.setOptionsEnabled("queries", true);
		

		GUIutils.backgroundJob = null; 
		GUIutils.BGJobTabKey = null;

	}
}