package GUI.Classes;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.SwingWorker;

import GUI.GUI;
import GUI.Classes.Enumerations.AnalyisiJobs;
import ecaAnalysis.Classes.Automaton;
import ecaAnalysis.Classes.AutomatonException;
import ecaAnalysis.Classes.ECAfile;
import ecaAnalysis.Classes.ECArule;

public class AnalysisWorker extends SwingWorker<Integer, Integer>
{	
	AnalyisiJobs jobToDo = null;

	public AnalysisWorker()
	{
		GUI.progressBar.setValue(0);
		jobToDo = AnalyisiJobs.CONSISTENCY;
		GUIutils.setOptionsEnabled("start", false);
		GUIutils.setOptionsEnabled("analysis", false);
		GUIutils.setOptionsEnabled("queries", false);
	}
	
	public AnalysisWorker(AnalyisiJobs job)
	{
		GUI.progressBar.setValue(0);
		jobToDo = job;
		GUIutils.setOptionsEnabled("start", false);
		GUIutils.setOptionsEnabled("analysis", false);
	}
	
	@Override
	protected Integer doInBackground() 
	{
		try 
		{
			ECAfile ecafile = new CheckErrorsAction().getCheckedEcafile();
						
			JSplitPane tabIndex = GUIutils.BGJobTabKey;
			String result = "";
				
			if(jobToDo != AnalyisiJobs.CONSISTENCY)
			{
				GUIutils.janalysisareas.get(tabIndex).setSelectedIndex(jobToDo.jobTabIndex());
				GUIutils.janalysistexts.get(tabIndex).get(jobToDo.jobTabIndex()).setText(result);
			}
				
			
			switch (jobToDo) 
			{
				// UNUSED RULES
				case UNUSABLE_RULES:
				case CONSISTENCY:
				{
					GUI.progressBar.setValue(0);
					GUIutils.setProgressLabel("Computing unusable rules");
					
					result = "";
					HashSet<ECArule> unusableRules = ecafile.findUnusableRules(true);
					if(unusableRules.isEmpty() == false)
					{
						for (ECArule ur : unusableRules)
							result += ur + "\n";
					}
					else
						result += "There are no UNUSED rules\n";
					
					GUI.progressBar.setValue(100);
					GUIutils.janalysistexts.get(tabIndex).get(0).setText(result);
					
					if(jobToDo != AnalyisiJobs.CONSISTENCY)
						break;
				}
				// INCORRECT RULES
				case INCORRECT_RULES:
				{							
					GUI.progressBar.setValue(0);
					GUIutils.setProgressLabel("Computing incorrect rules");
					
					result = "";
					HashSet<ECArule> incorrectRules = ecafile.findIncorrectRules(true);
					if(incorrectRules.isEmpty() == false)
					{
						for (ECArule ir : incorrectRules) 
							result += ir + "\n";
					}
					else
						result += "There are no INCORRECT rules\n";
					
					GUI.progressBar.setValue(100);
					GUIutils.janalysistexts.get(tabIndex).get(1).setText(result);
					
					if(jobToDo != AnalyisiJobs.CONSISTENCY)
						break;
				}
				// REDUNDANT RULES
				case REDUNDANT_RULES:
				{
					GUI.progressBar.setValue(0);
					GUIutils.setProgressLabel("Computing redundant rules");
					
					result = "";
					HashMap<ECArule, HashSet<ECArule>> redundantRules = ecafile.findRedundantRules();
					
					if(!redundantRules.keySet().isEmpty())
					{
						for (ECArule redr : redundantRules.keySet()) 
						{
							result += "The rule " + redr + " is redundant w.r.t:\n";
							for (ECArule wrt : redundantRules.get(redr)) 
							{
								result += wrt + "\n";
							}
							result += "\n";
						}
					}
					else
						result += "There are no REDUNDANT rules\n";
					
					GUI.progressBar.setValue(100);
					GUIutils.janalysistexts.get(tabIndex).get(2).setText(result);

					break;
				}
				// DETERMINISM
				case DETERMINISM:
				{
					GUI.progressBar.setValue(0);
					GUIutils.setProgressLabel("Checking determinism");
					
					result = "";
					boolean deterministic = ecafile.isDeterministic();
					if (deterministic)
						result = "The program is deterministic";
					else 
						result = "The program is NOT deterministic";
					
					GUI.progressBar.setValue(100);
					GUIutils.janalysistexts.get(tabIndex).get(3).setText(result);
										
					break;
				}
				// TERMINATION
				case TERMINATION_INPUT:
				{
					GUI.progressBar.setIndeterminate(true);
					GUIutils.setProgressLabel("Generating input components");
					
					Automaton tmpAutomaton = new Automaton(ecafile);
					JSplitPane parent = GUIutils.jtermactuatorsparent.get(tabIndex);
					JPanel child = GUIutils.createInputComponents(ecafile.getActuators(), tmpAutomaton);
					parent.setRightComponent(child);
					GUIutils.jtermactuatorsareas.put(tabIndex, child);
					JButton submit = new JButton("Submit");
				    submit.addActionListener(new SubmitTerminationAction());//TODO
		            child.add(submit, "cell 1 " + ecafile.getActuators().size());								
		      
		            // put the input components in a map in order to make the disable method easier
					HashSet<Component> terminationInput = (GUIutils.disabled.get("terminationInput") != null) ? GUIutils.disabled.get("terminationInput") : new HashSet<Component>();
					for (Component c : GUIutils.jtermactuatorsareas.get(GUIutils.BGJobTabKey).getComponents()) 
						terminationInput.add(c);

					terminationInput.add(GUIutils.jtermactuatorsareas.get(GUIutils.BGJobTabKey));

					GUIutils.disabled.put("terminationInput", terminationInput);
					
		            GUI.progressBar.setIndeterminate(false);
					GUIutils.setProgressLabel("Waiting for input");
					
					break;
				}
				case TERMINATION:
				{
					GUI.progressBar.setIndeterminate(true);
					GUIutils.setProgressLabel("Checking termination");
					
					result = "";
						
					String T2path = GUIutils.prefs.get("T2_PATH", "");
					if(!T2path.equals(""))
					{
						String TMPpath = ecafile.getT2file(GUIutils.ECAfilesGUI.get(tabIndex));
										
						Runtime r = Runtime.getRuntime();
						long startTime = System.nanoTime();
						Process p = r.exec("mono " + T2path + " --termination " + TMPpath + "--polyrank false");
						p.waitFor();	
						long endTime = System.nanoTime();
						BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
						
						String line = "";
						while ((line = b.readLine()) != null) 
							result += line + "\n";

						result = result + "\nelapsed time: " + (endTime-startTime)/1000000 + "ms";
						b.close();
					}

					GUI.progressBar.setIndeterminate(false);
					GUI.progressBar.setValue(0);
					GUIutils.janalysistexts.get(tabIndex).get(4).setText(result);							
		      
					break;
				}
			}
			return 0;
		} 
		catch (AutomatonException e) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(e.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors

			return 1;
		}
		catch (Exception e1)
		{
			e1.printStackTrace();
			return 1;
		}
	
	}

	@Override
	protected void done()
	{		
		GUIutils.setOptionsEnabled("start", true);
		GUIutils.setOptionsEnabled("analysis", true);

		GUIutils.setOptionsEnabled("queries", false);
		if(GUIutils.automata.get(GUIutils.BGJobTabKey) != null)
			if(GUIutils.automata.get(GUIutils.BGJobTabKey).getRoot() != null)
				GUIutils.setOptionsEnabled("queries", true);

		GUIutils.backgroundJob = null;
		GUIutils.BGJobTabKey = null;

		if(this.isCancelled())
		{
			GUI.progressBar.setIndeterminate(false);
			GUI.progressBar.setValue(0);
			GUIutils.setProgressLabel("Job cancelled");
		}
	}
	
}