package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JPanel;
import javax.swing.JSplitPane;

import GUI.GUI;
import GUI.Classes.Enumerations.QueriesJobs;

public class StartAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		GUIutils.BGJobTabKey = (JSplitPane) GUI.inputTabs.getSelectedComponent();
		
		GUIutils.setOptionsEnabled("start", false);
		GUIutils.setOptionsEnabled("stop", true);
		GUIutils.setOptionsEnabled("analysis", false);
		GUIutils.setOptionsEnabled("queries", false);
		
		JPanel findPathsPanel = ((JPanel) GUIutils.jqueriesresults.get(GUIutils.BGJobTabKey).get(QueriesJobs.FIND_PATHS.jobTabIndex()));
		findPathsPanel.removeAll();
		
		GUIutils.backgroundJob = new InputWorker(); // we create the new thread
		GUIutils.backgroundJob.execute();
	}
	
}
