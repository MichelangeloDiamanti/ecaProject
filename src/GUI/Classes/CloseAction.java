package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;
import javax.swing.JSplitPane;

import GUI.GUI;

public class CloseAction implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
		close(tabIndex);
	}
	
	public void close(JSplitPane tabIndex)
	{
		if(GUI.inputTabs != null)
		{
			if(GUI.inputTabs.getSelectedIndex() != -1)
			{		
				if(GUIutils.BGJobTabKey != null)
					if(GUIutils.BGJobTabKey.equals(tabIndex))
						new StopAction().stop();

				if(!GUIutils.ECAfilesGUI.get(GUI.inputTabs.getSelectedComponent()).isSaved())
				{
					int response = JOptionPane.showOptionDialog(null, "Do you want to save the file before closing?", "Confirm", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, new String[]{"Yes", "No", "Cancel"}, "default");
					if(response == JOptionPane.CANCEL_OPTION)
						return;
					else if(response == JOptionPane.YES_OPTION)
						new SaveAction().save(tabIndex);						
				}
							
				GUIutils.ECAfilesGUI.remove(tabIndex);
				GUIutils.jtextpanes.remove(tabIndex);
				GUIutils.jerrorsareas.remove(tabIndex);
				GUIutils.jgraphsareas.remove(tabIndex);
				GUIutils.graphs.remove(tabIndex);
				GUIutils.undoManagers.remove(tabIndex);
				GUIutils.jinitialstateareas.remove(tabIndex);
				GUIutils.jactuatorsareas.remove(tabIndex);
				GUIutils.jsensorsareas.remove(tabIndex);
				GUIutils.jqueriesareas.remove(tabIndex);
				GUIutils.janalysisareas.remove(tabIndex);
				GUIutils.fromList.remove(tabIndex);
				GUIutils.toList.remove(tabIndex);
				GUIutils.analysisPreviousTab.remove(tabIndex);
				GUIutils.joutputtabs.remove(tabIndex);
				GUIutils.janalysistexts.remove(tabIndex);
				GUIutils.jqueriesresults.remove(tabIndex);				
				GUIutils.automata.remove(tabIndex);
				GUIutils.sensorConfigFlag.remove(tabIndex);
				GUIutils.jtermactuatorsareas.remove(tabIndex);
				
				GUI.inputTabs.remove(tabIndex);
			}
        }
	}
	
}