package GUI.Classes;

import GUI.GUI;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Queue;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

/**
 * The methods in this class allow the JTree component to traverse the file
 * system tree and display the files and directories.
 *
 */
 public class FileTreeModel implements TreeModel 
 {

    // We specify the root directory when we create the model.
    protected ECAfileGUI root;
    private static final ECAfileGUI[] EMPTY_LIST = {};
    
    public FileTreeModel(ECAfileGUI root) 
    {
        this.root = root;
    }

    // The model knows how to return the root object of the tree
    public Object getRoot() 
    {
        return root;
    }

    private ECAfileGUI[] getFiles(ECAfileGUI parent) 
    {
        File[] files = parent.listFiles(new FileFilter() 
        {
            @Override
            public boolean accept(File file) 
            {
                if(!file.isHidden())
                {
                    // if the file is a not hidden directory
                    if(file.isDirectory())
                        return true;
                    // if the file has txt extension
                    else if (file.getName().endsWith(".txt"))
                        return true;
                }
                return false;
            }
        });
        

        ArrayList<File> leaves = new ArrayList<File>();
        ArrayList<File> dirs = new ArrayList<File>();
        
        for (int i = 0; i < files.length; i++) {
			if(files[i].isDirectory())
				dirs.add(files[i]);
			else 
				leaves.add(files[i]);
		}
        
        // order the list alphabetically, directories first
        Collections.sort(leaves, new FileNameComparator());
        Collections.sort(dirs, new FileNameComparator());
        
        ECAfileGUI treeFiles[] =  new ECAfileGUI[files.length];

        int i = 0;
        for (File f : dirs) {
			treeFiles[i] = new ECAfileGUI(f);
			i++;
		}
        for (File f : leaves) {
			treeFiles[i] = new ECAfileGUI(f);
			i++;
		}
        
        return treeFiles != null ? treeFiles : EMPTY_LIST;
    }

    @Override
    public int getChildCount(Object parent) 
    {
        return getFiles((ECAfileGUI) parent).length;
    }

    @Override
    public Object getChild(Object parent, int index) 
    {
        return getFiles((ECAfileGUI) parent)[index];
    }

    @Override
    public int getIndexOfChild(Object parent, Object child) 
    {
        ECAfileGUI[] files = getFiles((ECAfileGUI) parent);
        for (int idx = 0; idx < files.length; idx++) 
        {
            if (files[idx].equals(child))
                return idx;
        }
        return -1;
    }
    
    // Tell JTree whether an object in the tree is a leaf
    @Override
    public boolean isLeaf(Object node) 
    {        
        return ((ECAfileGUI)node).isFile(); 
    }
    
    


    // This method is invoked by the JTree only for editable trees.  
    // This TreeModel does not allow editing, so we do not implement 
    // this method.  The JTree editable property is false by default.
    public void valueForPathChanged(TreePath path, Object newvalue) 
    {
    }

    // Since this is not an editable tree model, we never fire any events,
    // so we don't actually have to keep track of interested listeners
    public void addTreeModelListener(TreeModelListener l) 
    {
    }

    public void removeTreeModelListener(TreeModelListener l) 
    {
    }
    
    static class FileNameComparator implements Comparator<File> {
        public int compare(File o1, File o2) {
        	String name1 = o1.getName();
        	String name2 = o2.getName();
        	return name1.compareToIgnoreCase(name2);
        }
    }
    
    public static void expandFilePath(Queue<String> queuePath, Object root, int offset)
	{	
		String parent = queuePath.poll();
		boolean rootHomeFlag = false;
		
		if(!queuePath.isEmpty())
		{
		for (int i = 0; i < GUI.tree.getModel().getChildCount(root); i++) 
		{
			Object child = GUI.tree.getModel().getChild(root, i);
			if(parent.equals(child.toString()))
			{ 
				int index = i + offset + 1;
				GUI.tree.expandRow(index);
				expandFilePath(queuePath, child, index);
				rootHomeFlag = true;
				break;
			}
	    }
		if(!rootHomeFlag)
			expandFilePath(queuePath, root, offset);
		}
	}
    
}