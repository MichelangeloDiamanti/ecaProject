package GUI.Classes;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JSplitPane;

import org.graphstream.graph.Graph;

import GUI.GUI;
import ecaAnalysis.Classes.AutomatonException;

public class ExportActionAsImage implements ActionListener
{
	public void actionPerformed(ActionEvent e)
	{
		try 
		{
			screenshot();
		} 
		catch (AutomatonException ae) 
		{
			JSplitPane tabIndex = (JSplitPane) GUI.inputTabs.getSelectedComponent();
			GUIutils.jerrorsareas.get(tabIndex).setText(ae.getMessage());
			GUIutils.joutputtabs.get(tabIndex).setSelectedIndex(4); //4=errors
		}
	}
	
	public void screenshot() throws AutomatonException
	{	
		Graph currentGraph = GUIutils.graphs.get(GUI.inputTabs.getSelectedComponent());
				
		if(currentGraph == null)
			throw new AutomatonException("Cannot apply this method to an empty automaton.\nThe simulation must be performed first");
		
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Choose a file to save the graph");
		fileChooser.setApproveButtonText("Save");
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		
		int fc = fileChooser.showOpenDialog(null);
		if(fc == JFileChooser.APPROVE_OPTION)
		{
			File file = fileChooser.getSelectedFile();
						
			String path = file.getAbsolutePath();
			String extension = "";
			if(path.contains("."))
				extension = path.substring(path.lastIndexOf("."), path.length());
			extension = extension.toUpperCase();
			
			if( !extension.equals(".JPG")  && 
				!extension.equals(".JPEG") && 
				!extension.equals(".PNG")  &&
				!extension.equals(".GIF")  &&
				!extension.equals(".BMP"))
			{
				path += ".png";
			}
						
			currentGraph.addAttribute("ui.screenshot", path);
					
		}
	}
}