package GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.HashSet;

import javax.swing.ButtonGroup;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTree;
import javax.swing.KeyStroke;

import GUI.Classes.AboutAction;
import GUI.Classes.AnalysisSettingsAction;
import GUI.Classes.CheckErrorsAction;
import GUI.Classes.CloseAction;
import GUI.Classes.CompactRadioButton;
import GUI.Classes.ECAfileGUI;
import GUI.Classes.ExitAction;
import GUI.Classes.ExportActionAsGraphML;
import GUI.Classes.ExportActionAsImage;
import GUI.Classes.ExtendedRadioButton;
import GUI.Classes.FileTreeModel;
import GUI.Classes.GUIutils;
import GUI.Classes.NewAction;
import GUI.Classes.OpenAction;
import GUI.Classes.ParallelRadioButton;
import GUI.Classes.RedoAction;
import GUI.Classes.RefreshAction;
import GUI.Classes.SaveAction;
import GUI.Classes.SaveAsAction;
import GUI.Classes.SequentialRadioButton;
import GUI.Classes.SimulationSettingsAction;
import GUI.Classes.StartAction;
import GUI.Classes.StopAction;
import GUI.Classes.TreeClick;
import GUI.Classes.UndoAction;



public class GUI 
{
	public static JFrame frame = null;
	public static JMenuBar menuBar = null;
	public static JTree tree = null;
	public static JTabbedPane inputTabs = null;
	public static JSplitPane splitPaneTree = null;
	public static JSplitPane progressSplitPane = null;
	public static JProgressBar progressBar = null;
	public static JLabel progressLabel = null;
	
		
	public static void main(String[] args) 
	{	
		new GUI();
	}
	
	public GUI() 
	{
		super();
		
		// FRAME #######################################################################################
		frame = new JFrame("vIRONy");
		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		
		frame.setMinimumSize(new Dimension(800, 600));
		frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
		
		frame.getContentPane().setLayout(new BorderLayout(0, 0));
		
		frame.addWindowListener(new WindowAdapter()
		{
           	@Override
           	public void windowClosing(WindowEvent e)
           	{
           		new ExitAction().confirm();
           	}
		});
		
		ImageIcon icon = new ImageIcon(this.getClass().getResource("/images/icon.png"));	
		frame.setIconImage(icon.getImage());
		
		// MENU BAR #######################################################################################
		JMenuBar menuBar = new JMenuBar();	
		frame.setJMenuBar(menuBar);

		HashSet<Component> tabDisabled = new HashSet<Component>();
		HashSet<Component> startDisabled = new HashSet<Component>();
		HashSet<Component> stopDisabled = new HashSet<Component>();
		HashSet<Component> graphDisabled = new HashSet<Component>();
		HashSet<Component> analysisDisabled = new HashSet<Component>();
		HashSet<Component> queriesDisabled = new HashSet<Component>();
		
		JMenu JMBfile = new JMenu("File");
		menuBar.add(JMBfile);
			
		JMenuItem JMBFnew = new  JMenuItem("New");
		JMBfile.add(JMBFnew);
		JMBFnew.addActionListener(new NewAction());
		JMBFnew.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
	
		JMenuItem JMBFopen = new  JMenuItem("Open");
		JMBfile.add(JMBFopen);
		JMBFopen.addActionListener(new OpenAction());
		JMBFopen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		
		JMBfile.addSeparator();
		
		JMenuItem JMBFsave = new  JMenuItem("Save");
		JMBfile.add(JMBFsave);
		JMBFsave.addActionListener(new SaveAction());
		JMBFsave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		
		JMenuItem JMBFsaveas = new  JMenuItem("Save As...");
		JMBfile.add(JMBFsaveas);
		JMBFsaveas.addActionListener(new SaveAsAction());
		JMBFsaveas.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		
		JMBfile.addSeparator();
		
		JMenuItem JMBFrefresh = new  JMenuItem("Refresh");
		JMBfile.add(JMBFrefresh);
		JMBFrefresh.addActionListener(new RefreshAction());
		JMBFrefresh.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F5,0));
		
		JMBfile.addSeparator();
		
		JMenuItem JMBFclose = new  JMenuItem("Close");
		JMBfile.add(JMBFclose);
		JMBFclose.addActionListener(new CloseAction());
		JMBFclose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		tabDisabled.add(JMBFclose);
				
		JMenuItem JMBFexit = new  JMenuItem("Exit");
		JMBfile.add(JMBFexit);
		JMBFexit.addActionListener(new ExitAction());
		JMBFexit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, ActionEvent.ALT_MASK));
		
		// EDIT MENU
		JMenu JMBedit = new JMenu("Edit");
		menuBar.add(JMBedit);	
		
		// UNDO
		JMenuItem JMBEundo = new  JMenuItem("Undo");
		JMBedit.add(JMBEundo);
		JMBEundo.addActionListener(new UndoAction());
		JMBEundo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK));
		tabDisabled.add(JMBEundo);
		
		// REDO
		JMenuItem JMBEredo = new  JMenuItem("Redo");
		JMBedit.add(JMBEredo);
		JMBEredo.addActionListener(new RedoAction());
		JMBEredo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		tabDisabled.add(JMBEredo);
				
		// SIMULATION
		JMenu JMBsimulation = new JMenu("Simulation");
		menuBar.add(JMBsimulation);
		ButtonGroup simulationGroup = new ButtonGroup();
		JRadioButtonMenuItem JMBSss = new  JRadioButtonMenuItem("Sequential Simulation");
		JRadioButtonMenuItem JMBSps = new  JRadioButtonMenuItem("Parallel Simulation");
		JMBSss.setSelected(true);
		simulationGroup.add(JMBSss);
		simulationGroup.add(JMBSps);
		JMBSss.addActionListener(new SequentialRadioButton());
		JMBSps.addActionListener(new ParallelRadioButton());
		JMBsimulation.add(JMBSss);
		JMBsimulation.add(JMBSps);		
		
		JMBsimulation.addSeparator();
		
		JMenuItem JMBSstart = new  JMenuItem("Start Simulation");
		JMBsimulation.add(JMBSstart);
		JMBSstart.addActionListener(new StartAction());
		JMBSstart.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		startDisabled.add(JMBSstart);
	
		JMenuItem JMBScheck = new  JMenuItem("Check Errors");
		JMBsimulation.add(JMBScheck);
		JMBScheck.addActionListener(new CheckErrorsAction());
		JMBScheck.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		startDisabled.add(JMBScheck);

		JMBsimulation.addSeparator();
		
		JMenuItem JMBSstop = new  JMenuItem("Stop Simulation");
		JMBsimulation.add(JMBSstop);
		JMBSstop.addActionListener(new StopAction());
		JMBSstop.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		stopDisabled.add(JMBSstop);

		JMenu JMBgraph = new JMenu("Graph");
		menuBar.add(JMBgraph);
		
		ButtonGroup graphGroup = new ButtonGroup();
		JRadioButtonMenuItem JMBGev = new  JRadioButtonMenuItem("Extended Visualization");
		JRadioButtonMenuItem JMBGcv = new  JRadioButtonMenuItem("Compact Visualization");
		JMBGev.setSelected(true);
		graphGroup.add(JMBGev);
		graphGroup.add(JMBGcv);
		JMBGev.addActionListener(new ExtendedRadioButton());
		JMBGcv.addActionListener(new CompactRadioButton());
		JMBgraph.add(JMBGev);
		JMBgraph.add(JMBGcv);
		
		JMBgraph.addSeparator();
		
		JMenuItem JMBGexportGML = new  JMenuItem("Export As GraphML");
		JMBgraph.add(JMBGexportGML);
		JMBGexportGML.addActionListener(new ExportActionAsGraphML());
		JMBGexportGML.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
		graphDisabled.add(JMBGexportGML);

		JMenuItem JMBGexportIMG = new  JMenuItem("Export As Image");
		JMBgraph.add(JMBGexportIMG);
		JMBGexportIMG.addActionListener(new ExportActionAsImage());
		JMBGexportIMG.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, ActionEvent.CTRL_MASK));
		graphDisabled.add(JMBGexportIMG);	
		
		JMenu JMBsettings = new JMenu("Settings");
		menuBar.add(JMBsettings);
		JMenuItem JMBSanalysis = new  JMenuItem("Analysis");
		JMBsettings.add(JMBSanalysis);
		JMBSanalysis.addActionListener(new AnalysisSettingsAction());
		JMenuItem JMBSsimulation = new  JMenuItem("Simulation");
		JMBsettings.add(JMBSsimulation);
		JMBSsimulation.addActionListener(new SimulationSettingsAction());
		
		JMenu JMBhelp = new JMenu("Help");
		menuBar.add(JMBhelp);
		JMenuItem JMBHabout = new  JMenuItem("About");
		JMBhelp.add(JMBHabout);
		JMBHabout.addActionListener(new AboutAction());
		

			
		// TREE #############################################################################
	
		JPanel wrapper = new JPanel(new BorderLayout());
		
		progressSplitPane = new JSplitPane();
		progressBar = new JProgressBar(); 		
		progressBar.setForeground(new Color(102, 204, 51));
		progressLabel = new JLabel("");
		progressSplitPane.setRightComponent(progressBar);		
		progressSplitPane.setLeftComponent(progressLabel);	
		progressSplitPane.setContinuousLayout(true);
				
		splitPaneTree = new JSplitPane();
		splitPaneTree.setDividerLocation(200);	
		splitPaneTree.setContinuousLayout(true);
		
		tree = new JTree();
		
		JScrollPane scrollTree = new JScrollPane(tree);
		scrollTree.setPreferredSize(new Dimension(200, 1));
		scrollTree.setMinimumSize(new Dimension(100, 1));
		
		// Create a TreeModel object to represent our tree of files
		FileTreeModel treeModel = new FileTreeModel(new ECAfileGUI(new File(System.getProperty("user.home"))));
		tree.setModel(treeModel);  	
		tree.addMouseListener(new TreeClick().getMouseAdapter());
		
		splitPaneTree.setLeftComponent(scrollTree);
		splitPaneTree.setRightComponent(new JPanel());
		
		wrapper.add(splitPaneTree, BorderLayout.CENTER);
		wrapper.add(progressSplitPane, BorderLayout.SOUTH);
		frame.getContentPane().add(wrapper, BorderLayout.CENTER);
		
		GUIutils.disabled.put("tab", tabDisabled);
		GUIutils.disabled.put("start", startDisabled);
		GUIutils.disabled.put("stop", stopDisabled);
		GUIutils.disabled.put("graph", graphDisabled);
		GUIutils.disabled.put("analysis", analysisDisabled);
		GUIutils.disabled.put("queries", queriesDisabled);
		
		GUIutils.setOptionsEnabled(false);
			
		frame.setVisible(true);
	}



	
}