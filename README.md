# IRONy

This software aims at providing an IDE for developers of IE programs (intelligent environments) written using the IRON syntax. It is capable of deeply analyzing the specified rules to find potential errors, redundancy and other problems that may affect the developing system. 
The features it offers are:

 - **Parsing**: it detects the syntax errors in the input files and returns detailed info about them in order to make a correction.
 - **Simulation**: to actually see the behavior of the system, the user can provide ad initial state and start a simulation. The result is a examinable automaton that allows to perform queries on it to get statistics and discover properties of the rules. 
 - **Semantic analysis**: if the user doesn't want to perform a simulation, he can get some system properties analysing the rules. This operation is accomplished using an SMT solver.

This software requires the following dependencies:
 - Java JVM 1.7 or greater
 - JavaCC, URL https://javacc.java.net/
 - Microsoft Z3, URL https://github.com/Z3Prover/z3
 - GraphStream, URL http://graphstream-project.org/download/

This software is in alpha version and currently under developement. In order to try it out one must follow these steps:
 - Obtain a working copy of [Microsoft Z3](https://github.com/Z3Prover/z3) with [java bindings](https://github.com/Z3Prover/z3#java)
 - Import the project in [Eclipse](https://eclipse.org/) or create a new Java project and copy the source code provided
 - Add the required libraries:
    - Properties > Java Build Path > Libraries
    - Add in there all the libraries contained in the folder "ExternalLibraries" provided. If they're already there but are marked as "missing" you must "Edit" them and select the correct path
    - For Microsoft Z3 library you must find in your filesystem a file called "com.microsoft.z3.jar" and add it in the libraries as well. It should be under the "build" folder obtained by compiling Z3
    - As soon as all the errors are cleared you can build the main GUI class and try out the software

There you can find a video that shows some of the features of the software\

[![software demo](https://img.youtube.com/vi/EieKAkVW7eo/0.jpg)](https://www.youtube.com/watch?v=EieKAkVW7eo)
